<div id="reservation-thingy" class="reservation-selector hidden-xs" style="left: -920px;" data-closed=1>
    <div class="main-title">
        <div class="titles">
            <h1>{{ trans('strings.reserve-table') }}</h1>
        </div>
    </div>
    <div style="left: 482px;position: relative;width: 500px;padding-left: 50px;padding-right: 50px;">
        <div id="table-picker">
            <input type="hidden" name="time" id="table-time">
            <div class="datepicker-days">
                <table class="table-condensed">
                    <thead>
                    <tr>
                        <th></th>
                        <th class="prev"><span class="glyphicon glyphicon-chevron-left" title="Previous Week" id="picker-prevPage"></span></th>
                        <th class="picker-switch" colspan="5" title="Select Month"><span id="table-picker-title-from"></span> - <span id="table-picker-title-to"></span></th>
                        <th class="next" ><span class="glyphicon glyphicon-chevron-right" title="Next Week" id="picker-nextPage"></span>
                        </th>
                    </tr>
                    <tr>
                        <th></th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(0)->dayOfWeek) }}</th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(1)->dayOfWeek) }}</th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(2)->dayOfWeek) }}</th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(3)->dayOfWeek) }}</th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(4)->dayOfWeek) }}</th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(5)->dayOfWeek) }}</th>
                        <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(6)->dayOfWeek) }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($table_picker_times as $time)
                        <tr class="table-picker-row" data-time = '{{ $time }}'>
                            <td>{{ $time }}</td>
                            <td class="day"></td>
                            <td class="day"></td>
                            <td class="day"></td>
                            <td class="day"></td>
                            <td class="day"></td>
                            <td class="day"></td>
                            <td class="day"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div style="text-align: center;margin-left: -30px;"><a href="{{ route('reservation.table') }}" style="font-family: 'EB Garamond', serif; color: #b99670; text-transform: uppercase; font-size: 18px;">Tovább</a></div>
    </div>
</div>
</div>
</div>

<!-- jQuery -->
<script src="{{ asset('assets/bower/jquery/dist/jquery.min.js') }}"></script>
<script>
    'use strict';

    var dt;

    var disabledTableDays = [ {!! $table_picker_disabled_days !!} ];
    var unavailableTableDays = [ {!! $table_picker_unavailable_days !!} ];

    var reservations = {!! json_encode($table_picker_reservations) !!}; // object holding every reservation
    var tableReservations = [];

    var limit = {{ $table_picker_limit }};
    var pageLimit = 4; // generated days (server side) / 7 (1 week)

    var page = 0; // page number for paginator

    var genTitle = function() {
        $("#table-picker-title-from").html(moment().startOf('day').add("days", (page*7)).format('YYYY-MM-DD'));
        $("#table-picker-title-to").html(moment().startOf('day').add("days", 6 + page*7).format('YYYY-MM-DD'));
    };

    var genTable = function() {
        // resetting
        $('#table-picker td:not(:first-child)').html('');
        $('#table-picker td:not(:first-child)').removeClass('full');
        $('#table-picker td:not(:first-child)').removeClass('selected');
        $("#table-time").val('');

        // refreshing
        $('.table-picker-row').each(function(rowkey, rowval) { // rows
            var row = $(this);
            $(this).children('td').each(function(key, val) { // cols
                if (key == 0) return; // don't do anything with first col - it's a label
                var time = $(rowval).data('time'); // first col label
                var hour = time.substr(0,2); // hour part for date manipulation

                var theDate = moment().startOf('day').add("days", (key-1) + (page*7) ).add("hours", hour);
                var datetimeStr = theDate.format('YYYY-MM-DD HH:mm:ss'); // generating the datetime string for the reservation
                $(this).attr('data-datetime', datetimeStr);
                $(this).on('click', function() {
                    var date = theDate.format('YYYY-MM-DD');
                    var time = theDate.format('HH:mm');
                    if (! $(this).hasClass('full')) window.location = '{{ route('reservation.table') }}?date='+date+'&time='+time;
                });

                if ($.inArray(theDate.format('YYYY-MM-DD'), disabledTableDays) > -1 || $.inArray(theDate.format('YYYY-MM-DD'), unavailableTableDays) > -1) {
                    $(this).addClass('full');
                    return;
                }
                if (reservations[time] == null || reservations[time] === undefined/* || reservations[time][datetimeStr] == null || reservations[time][datetimeStr] === undefined*/) return; // skip if there's no elements for this hour
                if (reservations[time][datetimeStr] == null || reservations[time][datetimeStr] === undefined) return; // count of reservation

                var count = reservations[time][datetimeStr]
                if (count >= limit)
                    $(this).addClass('full');
            })
        })
    };

    $("#picker-nextPage").on("click", function() {
        if (page == pageLimit-1) return false;
        page++;
        genTitle();
        genTable();
    });

    $("#picker-prevPage").on("click", function() {
        if (page == 0) return false;
        page--;
        genTitle();
        genTable();
    });

    $(function () {
        genTitle();
        genTable();

        var kutya = $("#reservation-thingy");
        $('#reservation-thingy > .main-title > .titles').on('click', function() {
            if (kutya.data('closed') == 1) {
                kutya.animate({
                    "left": '-482px'
                }, 300);
                kutya.data('closed', 0);
            } else {
                kutya.animate({
                    "left": '-920px'
                }, 300);
                kutya.data('closed', 1);
            }
        });
    });
</script>
