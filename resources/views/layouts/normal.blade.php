<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <link rel="icon" href="img\favicon.ico">

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/bower/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/base.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" media="screen">

    <!-- Bootstrap DateTimePicker CSS -->
    <link rel="stylesheet" href="{{ asset('assets/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <!-- Blueimp Gallery -->
    <link rel="stylesheet" href="{{ asset('assets/bower/blueimp-gallery/css/blueimp-gallery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower/blueimp-bootstrap-image-gallery/css/bootstrap-image-gallery.min.css') }}">

    <link href="{{ asset('assets/bower/bootstrap-star-rating/css/star-rating.min.css') }}" media="all" rel="stylesheet" type="text/css" />

    <link href='https://fonts.googleapis.com/css?family=EB+Garamond&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        // UA-71561761-1
        ga('create', 'can\'t touch this!', 'auto');
        ga('send', 'pageview');
    </script>
</head>


<body id="page-top" data-spy="scroll" data-target="#navbar-example2" data-offset="0" class="scrollspy-example" {!! (\Request::route()->getName() != 'homepage') ? 'style="background-color: #f5e3bd; background-image: url(\'/assets/img/normal-page-bg.png\')   "' : '' !!}>
<nav class="navbar navbar-static-top navigation-bar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header page-scroll">
            <button type="button" class="pull-left navbar-toggle" style="margin-left: 10px" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="btn-group visible-xs pull-right lang-selector-mobile"> <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">{!! strtoupper(LaravelLocalization::getCurrentLocale()) !!} <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><div class="lang {{ LaravelLocalization::getCurrentLocale() == 'hu' ? 'active' : 'inactive' }}"><a rel="alternate" hreflang="hu" href="{{LaravelLocalization::getLocalizedURL('hu') }}"> HU</a></div></li>
                    <li><div class="lang {{ LaravelLocalization::getCurrentLocale() == 'en' ? 'active' : 'inactive' }}"><a rel="alternate" hreflang="en" href="{{LaravelLocalization::getLocalizedURL('en') }}"> EN</a></div></li>
                </ul>
            </div>
        </div>
        <img src="/assets/img/logo-lines.png" usemap="#image-map" id="malom-logo" class="hidden-xs">
        <map name="image-map" id="image-map" class="hidden-xs">
            <area shape="poly" coords="91,78,120,81,128,56,152,83,177,57,185,81,213,77,186,15,118,14" alt="Home page" href="{{ (Request::route()->getName() == 'homepage') ? '#page-top' : '/#page-top' }}">
            </area>
        </map>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="nav navbar-nav left-side-nav">
                    <li class="hidden"><a class="page-scroll" href="#page-top"></a></li>
                    <li class="visible-xs"><a href="{{ (Request::route()->getName() == 'homepage') ? '#page-top' : '/#page-top' }}">{!! trans('strings.homepage') !!}</a></li>

                    <li><a class="page-scroll {{ (Request::route()->getName() == 'story') ? 'active-page' : '' }}" href="{{ route('story') }}">  {{ $blocks[0]->getTranslation()->name }}</a></li>
                    <li><a class="page-scroll {{ (Request::route()->getName() == 'restaurant') ? 'active-page' : '' }}" href="{{ route('restaurant') }}">{{ $blocks[1]->getTranslation()->name }}</a></li>
                    <li><a class="page-scroll {{ (Request::route()->getName() == 'menu') ? 'active-page' : '' }}" href="{{ route('menu') }}">{{ trans('strings.menu') }}</a></li>
                    <li><a class="page-scroll" href="{{ (Request::route()->getName() == 'homepage') ? '#contacts' : '/#contacts' }}">{{ $blocks[2]->getTranslation()->name }}</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2"></div>
            <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="nav navbar-nav">
                    <li><a class="page-scroll {{ (Request::route()->getName() == 'events') ? 'active-page' : '' }}" href="{{ route('events') }}">{{ trans('strings.events') }}</a></li>
                    <li><a class="page-scroll {{ (Request::route()->getName() == 'services') ? 'active-page' : '' }}" href="{{ route('services') }}">{{ trans('strings.services') }}</a></li>
                    <li><a class="page-scroll {{ (Request::route()->getName() == 'gallery') ? 'active-page' : '' }}" href="{{ route('gallery') }}">{{ trans('strings.gallery') }}</a></li>
                    <li><a class="page-scroll {{ (Request::route()->getName() == 'contact') ? 'active-page' : '' }}" href="{{ route('contact') }}">{{ trans('strings.contacts') }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

@include('language-selector')
@include('reservation')

<!-- CONTENT BEGIN -->

@section('page-content')
@show

<section id="closing" class="closing-section closing-section-normal">
    <div class="container-fluid">
        <div class="row row-centered">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 menu">
                <a href="{{ (Request::route()->getName() == 'homepage') ? '#page-top' : '/#page-top' }}">FŐOLDAL</a>
                <a href="{{ route('story') }}">Történetünk</a>
                <a href="{{ route('restaurant') }}">étterem</a>
                <a href="{{ (Request::route()->getName() == 'homepage') ? '#vendeghazak' : '/#contacts' }}">Vendégházak</a>
                <a href="{{ route('events') }}">események</a>
                <a href="{{ route('gallery') }}">galéria</a>
                <a href="{{ route('services') }}">{{ trans('strings.services') }}</a>
                <a href="{{ route('wellness') }}">{{ trans('strings.wellness') }}</a>
                <a href="{{ route('programroom') }}">{{ trans('strings.programroom') }}</a>
                <a href="{{ route('watermill') }}">{{ trans('strings.watermill') }}</a>
                <a href="{{ route('contact') }}">ELÉRHETŐSÉGEK</a>
                <a href="{{ (Request::route()->getName() == 'homepage') ? '#opinions' : '/#opinions' }}">VÉLEMÉNYEK</a>
                <a href="{{ route('reservation.table') }}">{{ trans('strings.reserve-table') }}</a>
                <a href="{{ route('reservation.room') }}">{{ trans('strings.reserve-room') }}</a>
            </div>
        </div>
        <div class="row row-centered">
            <div class="hidden-xs col-lg-12 col-md-12">
                <img style="padding-bottom: 20px; padding-top: 20px;" src="/assets/img/logo.png">
                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 24px; margin: 0 0 0 0px;">{{ $pageSettings['address_'.\LaravelLocalization::getCurrentLocale()] }}</p>
                <p style="text-transform: uppercase; color: #353434;font-family: 'Lovelo', sans-serif;font-size: 18px; margin: 0 0 0 0px;">{{ trans('strings.contacts') }}</p>
                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 18px; margin: 0 0 0 0px;">{{ $pageSettings['tel1_'.\LaravelLocalization::getCurrentLocale()] }}    {{ $pageSettings['tel2_'.\LaravelLocalization::getCurrentLocale()] }}</p>
                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 18px; margin: 0 0 0 0px;">{{ $pageSettings['email_'.\LaravelLocalization::getCurrentLocale()] }}</p>

                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 24px;">Facebook | Google+</p>
                <p style="text-transform: uppercase; color: #353434;font-family:'EB Garamond', serif;font-size: 15px; padding-bottom: 80px;">Design By: INFOGRAFIK - MINŐSÉGI KIVITELEZÉS</p>
            </div>
            <div class="visible-xs col-lg-12 col-md-12">
                <img style="padding-bottom: 20px; padding-top: 20px;" src="/assets/img/logo.png">
                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 20px; margin: 0 0 0 0px;">{{ $pageSettings['address_'.\LaravelLocalization::getCurrentLocale()] }}</p>
                <p style="text-transform: uppercase; color: #353434;font-family: 'Lovelo', sans-serif;font-size: 14px; margin: 0 0 0 0px;">{{ trans('strings.contacts') }}</p>
                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 14px; margin: 0 0 0 0px;">{{ $pageSettings['tel1_'.\LaravelLocalization::getCurrentLocale()] }}    {{ $pageSettings['tel2_'.\LaravelLocalization::getCurrentLocale()] }}</p>
                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 14px; margin: 0 0 0 0px;">{{ $pageSettings['email_'.\LaravelLocalization::getCurrentLocale()] }}</p>

                <p style="text-transform: uppercase; color: #fff;font-family: 'Lovelo', sans-serif;font-size: 20px;">Facebook | Google+</p>
                <p style="text-transform: uppercase; color: #353434;font-family:'EB Garamond', serif;font-size: 15px; padding-bottom: 80px;">Design By: INFOGRAFIK - MINŐSÉGI KIVITELEZÉS</p>
            </div>

        </div>
        <div class="row row-centered" style="margin-right: -15px; margin-left: -15px;">
            <div class="col-lg-12 col-md-12 copyright">

                <img height='32' class="hidden-xs" src='/assets/img/payment.png' style="margin-top: 8px;">
                <center><img height='32' class="img-responsive visible-xs" src='/assets/img/payment.png' style="margin-top: 8px; max-height: 32px"></center>

                <div id="btn-top" class="btn-top-copyright">
                    <p style>lap<br>tetejére</p>
                </div>

                <p style="text-transform: uppercase; color: #fff;font-family:'EB Garamond', serif;font-size: 15px; margin-top: -80px;">Copyright © 2015 MALOMUDVAR</p>

            </div>
        </div>
    </div>
</section>

<!-- CONTENT END -->

<!-- jQuery -->
<script src="{{ asset('assets/bower/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('assets/bower/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/scrolling-nav.js') }}"></script>

<!-- Moment JS JavaScript -->
<script type="text/javascript" src="{{ asset('assets/bower/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bower/moment/min/moment-with-locales.min.js') }}"></script>

<!-- Bootstrap DateTimePicker JavaScript -->
<script type="text/javascript" src="{{ asset('assets/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('assets/bower/blueimp-gallery/js/jquery.blueimp-gallery.min.js') }}"></script>
<script src="{{ asset('assets/bower/blueimp-bootstrap-image-gallery/js/bootstrap-image-gallery.min.js') }}"></script>

@section('page-assets')
@show

</body>
</html>
<script>
    $('body').scrollspy({ target: '#navbar-example' });

    $("#btn-top").click(function(){
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });

    // banner
    $(function() {
        $.get('/load-banner', function(data) {
            $('body').append(data);
        });
    });

    $('#image-map area').each(function () {
    // Assigning an action to the mouseover event
    $(this).mouseover(function (e) {
        document.getElementById("malom-logo").src="/assets/img/logo-lines-hover.png";
    });

    // Assigning an action to the mouseout event
    $(this).mouseout(function (e) {
        document.getElementById("malom-logo").src="/assets/img/logo-lines.png";
    });
</script>
