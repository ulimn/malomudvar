@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => ['admin.homeblocks.update', $block->id], 'method' => 'post')) !!}

    <h4>{{ trans('strings.Hungarian') }}</h4>
    <div class="form-group">
        <label for="name_hu">{{ trans('forms.homeblocks.name') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="name_hu" value="{{ $block->getTranslation('hu')->name }}" required>
    </div>
    <div class="form-group">
        <label for="description_hu">{{ trans('forms.homeblocks.description') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea class="form-control" name="description_hu" required>{{ $block->getTranslation('hu')->description }}</textarea>
    </div>
    <div class="form-group">
        <label for="content_hu">{{ trans('forms.homeblocks.content') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea class="form-control" name="content_hu" required>{{ $block->getTranslation('hu')->content }}</textarea>
    </div>

    <h4>{{ trans('strings.English') }}</h4>
    <div class="form-group">
        <label for="name_en">{{ trans('forms.homeblocks.name') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" name="name_en" value="{{ $block->getTranslation('en')->name }}" required>
    </div>
    <div class="form-group">
        <label for="description_en">{{ trans('forms.homeblocks.description') }} - {{ trans('strings.English') }}</label>
        <textarea class="form-control" name="description_en" required>{{ $block->getTranslation('en')->description }}</textarea>
    </div>
    <div class="form-group">
        <label for="content_en">{{ trans('forms.homeblocks.content') }} - {{ trans('strings.English') }}</label>
        <textarea class="form-control" name="content_en" required>{{ $block->getTranslation('en')->content }}</textarea>
    </div>

    <h4>{{ trans('strings.German') }}</h4>
    <div class="form-group">
        <label for="name_de">{{ trans('forms.homeblocks.name') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" name="name_de" value="{{ $block->getTranslation('de')->name }}" required>
    </div>
    <div class="form-group">
        <label for="description_de">{{ trans('forms.homeblocks.description') }} - {{ trans('strings.German') }}</label>
        <textarea class="form-control" name="description_de" required>{{ $block->getTranslation('de')->description }}</textarea>
    </div>
    <div class="form-group">
        <label for="content_de">{{ trans('forms.homeblocks.content') }} - {{ trans('strings.German') }}</label>
        <textarea class="form-control" name="content_de" required>{{ $block->getTranslation('de')->content }}</textarea>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>

{!! Form::close() !!}
@endsection
