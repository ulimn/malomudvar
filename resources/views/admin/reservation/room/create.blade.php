@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => ['admin.reservations.room.store'], 'method' => 'post')) !!}

    <div class="form-group">
        <label for="apartment">{{ trans('forms.reservation.room.which-apartment') }}</label>
        <select class="form-control" name="apartment" id="apartment-picker" required>
            @foreach($apartments as $apartment)
                <option value="{{ $apartment->id }}" {{ $apartment->id == $currentApartment ? "selected=selected" : "" }}>{{ $apartment->getTranslation()->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="name">{{ trans('forms.reservation.room.name') }}</label>
        <input type="text" class="form-control" name="name" value="" required>
    </div>
    <div class="form-group">
        <label for="address">{{ trans('forms.reservation.room.address') }}</label>
        <input type="text" class="form-control" name="address" value="">
    </div>
    <div class="form-group">
        <label for="phone">{{ trans('forms.reservation.room.tel') }}</label>
        <input type="text" class="form-control" name="phone" value="" required>
    </div>
    <div class="form-group">
        <label for="email">{{ trans('forms.reservation.room.email') }}</label>
        <input type="email" class="form-control" name="email" value="" required>
    </div>
    <div class="form-group">
        <label for="coupon">{{ trans('forms.reservation.room.coupon') }}</label>
        <input type="text" class="form-control" name="coupon" value="">
    </div>
    <div class="form-group">
        <div class='col-md-6'>
            <label for="time">{{ trans('forms.reservation.room.time-begin') }}*</label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker6'>
                    <input type='text' name="time_from" class="form-control" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class='col-md-6'>
            <label for="time">{{ trans('forms.reservation.room.time-end') }}*</label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker7'>
                    <input type='text' name="time_to" class="form-control" required />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class='col-md-6'>
            <label for="adults">{{ trans('forms.reservation.room.adult-count') }}*</label>
            <input type="text" class="form-control" name="adults" value="" required>
        </div>
        <div class='col-md-6'>
            <label for="children">{{ trans('forms.reservation.room.child-count') }}*</label>
            <input type="text" class="form-control" name="children" value="" required>
    </div>
    </div>
    <div class="form-group">
        <label for="comment">{{ trans('forms.reservation.room.comment') }}</label>
        <textarea class="form-control" name="comment" rows="5"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
    <a href="{{ route('admin.reservations.room.index') }}" class="btn btn-default">{{ trans('forms.cancel') }}</a>

{!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker6').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                disabledDates: [
                    @foreach($disabledDates as $date)
                    '{{ $date }}',
                    @endforeach
                ],
                minDate: moment(),
                maxDate: moment().add('days', 182),
                format: "YYYY-MM-DD HH:mm",
                sideBySide: true
            });
            $('#datetimepicker7').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                disabledDates: [
                    @foreach($disabledDates as $date)
                    '{{ $date }}',
                    @endforeach
                ],
                minDate: moment(),
                maxDate: moment().add('days', 182),
                format: "YYYY-MM-DD HH:mm",
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
            $("#apartment-picker").on("change", function() {
                window.location = "/admin/reservations/room/create/" + $(this).val();
            });
        });
    </script>
@endsection
