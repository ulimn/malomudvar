@extends('admin.layouts.main')

@section('page-content')
    <div id="delModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ trans('forms.dialog.warning') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ trans('forms.dialog.delete') }};</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.cancel') }}</button>
                    <a href="{{ route('admin.reservations.room.delete', [$reservation->id]) }}" class="btn btn-danger">{{ trans('forms.delete') }}</a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

{!! Form::open(array('route' => ['admin.reservations.room.update', $reservation->id], 'method' => 'post')) !!}

    <div class="form-group">
        <label for="apartment">{{ trans('forms.reservation.room.which-apartment') }}</label>
        <select class="form-control" name="apartment" required>
            @foreach($apartments as $apartment)
                <option {{ $apartment->id == $reservation->apartment_id ? 'selected' : '' }} value="{{ $apartment->id }}">{{ $apartment->getTranslation()->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="name">{{ trans('forms.reservation.room.name') }}</label>
        <input type="text" class="form-control" name="name" value="{{ $reservation->name }}" required>
    </div>
    <div class="form-group">
        <label for="address">{{ trans('forms.reservation.room.address') }}</label>
        <input type="text" class="form-control" name="address" value="{{ $reservation->address }}">
    </div>
    <div class="form-group">
        <label for="phone">{{ trans('forms.reservation.room.tel') }}</label>
        <input type="text" class="form-control" name="phone" value="{{ $reservation->phone }}" required>
    </div>
    <div class="form-group">
        <label for="email">{{ trans('forms.reservation.room.email') }}</label>
        <input type="email" class="form-control" name="email" value="{{ $reservation->email }}" required>
    </div>
    <div class="form-group">
        <label for="coupon">{{ trans('forms.reservation.room.coupon') }}</label>
        <input type="text" class="form-control" name="coupon" value="{{ $reservation->coupon }}">
    </div>
    <div class="form-group">
        <div class='col-md-6'>
            <label for="time">{{ trans('forms.reservation.room.time-begin') }}*</label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker6'>
                    <input type='text' name="time_from" class="form-control" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class='col-md-6'>
            <label for="time">{{ trans('forms.reservation.room.time-end') }}*</label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker7'>
                    <input type='text' name="time_to" class="form-control" required />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class='col-md-6'>
            <label for="adults">{{ trans('forms.reservation.room.adult-count') }}*</label>
            <input type="text" class="form-control" name="adults" value="{{ $reservation->adult_count }}" required>
        </div>
        <div class='col-md-6'>
            <label for="children">{{ trans('forms.reservation.room.child-count') }}*</label>
            <input type="text" class="form-control" name="children" value="{{ $reservation->child_count }}" required>
    </div>
    </div>
    <div class="form-group">
        <label for="comment">{{ trans('forms.reservation.room.comment') }}</label>
        <textarea class="form-control" name="comment" rows="5">{{ $reservation->comment }}</textarea>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
    <a href="{{ route('admin.reservations.room.index') }}" class="btn btn-default">{{ trans('forms.cancel') }}</a>
    <button type="button" data-toggle="modal" data-target="#delModal" class="btn btn-danger pull-right">{{ trans('forms.delete') }}</button>

{!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker6').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                disabledDates: [
                    @foreach($disabledDates as $date)
                    '{{ $date }}',
                    @endforeach
                ],
                defaultDate: moment(new Date('{{ $reservation->time_from }}')),
                format: "YYYY-MM-DD",
                sideBySide: true
            });
            $('#datetimepicker7').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                disabledDates: [
                    @foreach($disabledDates as $date)
                    '{{ $date }}',
                    @endforeach
                ],
                minDate: moment(),
                format: "YYYY-MM-DD",
                defaultDate: moment(new Date('{{ $reservation->time_to }}')),
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@endsection
