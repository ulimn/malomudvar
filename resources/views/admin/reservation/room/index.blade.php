@extends('admin.layouts.main')

@section('page-content')

<a href="{{ route('admin.reservations.room.create') }}"><span class="btn btn-primary">{{ trans('admin.new-reservation') }}</span></a>

<table id="admin-events-table" class="display table table-bordered table-hover table-striped" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Edit</th>
        <th>Name</th>
        <th>Apartment</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Time/From</th>
        <th>Time/To</th>
        <th>Adults</th>
        <th>Children</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Edit</th>
        <th>Name</th>
        <th>Apartment</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Time/From</th>
        <th>Time/To</th>
        <th>Adults</th>
        <th>Children</th>
    </tr>
    </tfoot>
    <tbody>
        @foreach($reservations as $res)
            <tr>
                <td><a href="{{ route('admin.reservations.room.edit', [$res->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>{{ $res->name }}</td>
                <td>{{ $res->apartment_id }}</td>
                <td>{{ $res->phone }}</td>
                <td>{{ $res->email }}</td>
                <td>{{ $res->getTimeFrom() }}</td>
                <td>{{ $res->getTimeTo() }}</td>
                <td>{{ $res->adult_count }}</td>
                <td>{{ $res->child_count }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('page-assets')
    <script>
        'use strict';

        $(function() {
            $('#admin-events-table').DataTable();
        })
    </script>
@endsection
