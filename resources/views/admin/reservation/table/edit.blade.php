@extends('admin.layouts.main')

@section('page-content')
    <div id="delModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ trans('forms.dialog.warning') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ trans('forms.dialog.delete') }};</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.cancel') }}</button>
                    <a href="{{ route('admin.reservations.table.delete', [$reservation->id]) }}" class="btn btn-danger">{{ trans('forms.delete') }}</a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

{!! Form::open(array('route' => ['admin.reservations.table.update', $reservation->id], 'method' => 'post')) !!}

    <div class="form-group">
        <label for="name">{{ trans('forms.reservation.table.name') }}</label>
        <input type="text" class="form-control" name="name" value="{{ $reservation->name }}" required>
    </div>
    <div class="form-group">
        <label for="address">{{ trans('forms.reservation.table.address') }}</label>
        <input type="text" class="form-control" name="address" value="{{ $reservation->address }}">
    </div>
    <div class="form-group">
        <label for="phone">{{ trans('forms.reservation.table.tel') }}</label>
        <input type="text" class="form-control" name="phone" value="{{ $reservation->phone }}" required>
    </div>
    <div class="form-group">
        <label for="email">{{ trans('forms.reservation.table.email') }}</label>
        <input type="email" class="form-control" name="email" value="{{ $reservation->email }}" required>
    </div>
    <div class="form-group">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                <label for="date">{{ trans('forms.reservation.table.date') }}*</label>
                <div class='input-group date' id='datetimepicker7'>
                    <input type='text' name="date" class="form-control" id="table-date-input" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                <label for="time">{{ trans('forms.reservation.table.time') }}*</label>
                <select name="time" id="table-time-select" class="form-control" required= >
                    <option value=""></option>
                    @foreach($times as $time)
                        <option value="{{ $time }}" {{ false !== strpos($reservation_time, $time) ? 'selected=selected' : '' }}>{{ $time }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='col-lg-4 col-md-4'>
            <div class="form-group">
                <label for="guests">{{ trans('forms.reservation.table.guest-count') }}*</label>
                <input type="number" class="form-control" name="guests" value="{{ $reservation->guest_count }}" min="0" max id="guest-count" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="comment">{{ trans('forms.reservation.table.comment') }}</label>
        <textarea class="form-control" name="comment" rows="5">{{ $reservation->comment }}</textarea>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
        <a href="{{ route('admin.reservations.table.index') }}" class="btn btn-default">{{ trans('forms.cancel') }}</a>
        <button type="button" data-toggle="modal" data-target="#delModal" class="btn btn-danger pull-right">{{ trans('forms.delete') }}</button>
    </div>

{!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        ;'use strict';

        var disabledTimes = [
            @forelse($disabledTimes as $time)
                '{!! $time->time !!}',
            @empty
            @endforelse
        ];

        var firstLoad = true;

        var calculateDisabled = function() {
            if (firstLoad === true) $("#table-time-select").val('0');
            $("#table-time-select option").each(function () {
                var td = $(this);
                var dateTime = $("#table-date-input").val() + ' ' + $(this).val() + ":00";
                if (disabledTimes.indexOf(dateTime) > -1
                    && dateTime != '{!! $reservation->time !!}')
                    $(td).prop("disabled", true);
                else $(td).prop("disabled", false);
            });
        }

        $(function () {
            var dtp = $('#datetimepicker7').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                defaultDate: moment(new Date('{{ $reservation_date }}')),
                format: "YYYY-MM-DD",
                minDate: moment().startOf("day"),

            });

            //calculateDisabled(false);

            dtp.on("dp.change", function() {
                if (firstLoad === true) $("#table-time-select").val('0'); // on first load, the time is needed
                $("#table-time-select option").each(function () {
                    var td = $(this);
                    var dateTime = $("#table-date-input").val() + ' ' + $(this).val() + ":00";
                    if (disabledTimes.indexOf(dateTime) > -1 && dateTime != '{!! $reservation->time !!}')
                        $(td).prop("disabled", true);
                    else $(td).prop("disabled", false);
                });

                firstLoad = false; // first load done.
            });

            var getRemaining = function() {
                $.ajax({
                    type: 'POST',

                    headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
                    url: '{!! route('reservation.table.ajax-get-count') !!}',
                    dataType: 'text',
                    data: {
                        'datetime': $("#table-date-input").val() + ' '+ $("#table-time-select").val() + ":00"
                    },
                    success: function (data) {
                        $("#guest-count").attr("max", data);
                    }
                });
            };
            getRemaining();
            
            $("#table-time-select").on("change", getRemaining);
        });
    </script>
@endsection
