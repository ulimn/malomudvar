@extends('admin.layouts.main')

@section('page-content')

    <style>
        #admin-table-addforced {
            width: 100%;
        }
    </style>

    <div class="row">
        <div class="col-md-3">
            <h3>{{ trans('admin.days') }}</h3>
            @for($i = 0; $i < 7; $i++)
            <label class="checkbox"><input type="checkbox"
                                           data-dayofweek="{{ $i }}"
                                           name="unavailable[]"
                        {!! $unavailables[$i] == 0 ? 'checked="checked"' : '' !!}> {{ trans('strings.days-long.'.$i) }}
            </label>
            @endfor
        </div>
        <div class="col-md-9">
            <h3>{{ trans('admin.forced') }}</h3>
            <div class="pull-left">
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'></div>
                    <button class="btn btn-primary" id="admin-table-addforced">{{ trans('strings.add') }}</button>
                </div>
            </div>
            <div id="admin-datelist-container" class="pull-left">
                <ul class="admin-list" id="admin-list">
                </ul>
            </div>
        </div>

        @endsection

        @section('page-assets')
            <script>
                'use strict';

                $("[name='unavailable[]']").bootstrapSwitch({'size': 'mini'});

                $(function () {

                    // reload list of forced days
                    var loadForceddays = function() {
                        console.log("reload");
                        $("#admin-list").empty();
                        $.ajax({
                            url: '{!! route('admin.reservations.table.ajax-get-forcedday') !!}',
                            method: 'GET',
                            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                            dataType: 'json',
                            success: function (data) {
                                var listStr = "";
                                $.each(data.forceddays, function(key, row) {
                                    listStr += '<li>'+row+' - <button class="admin-list-button btn btn-default btn-xs" data-date="'+row+'" role="button">{{ trans('strings.delete') }}</button></li>';
                                });
                                $("#admin-list").append(listStr); // <li>1990-03-28 - <a href="#" role="button">{{ trans('strings.delete') }}</a></li>
                            }
                        });
                    }; loadForceddays();

                    // set day as (un)available
                    $('#datetimepicker1').datetimepicker({'inline': true, format: 'YYYY-MM-DD'});
                    $("[name='unavailable[]']").on('switchChange.bootstrapSwitch', function (event, state) {
                        var dayOfWeek = $(this).data('dayofweek');
                        $.ajax({
                            url: '{!! route('admin.reservations.table.ajax-set-unavailableday') !!}',
                            method: 'POST',
                            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                            dataType: 'jsonp',
                            data: {
                                dayOfWeek: dayOfWeek,
                                value: state
                            },
                            success: function (data) {
                                console.log("kutya success");
                            }
                        });
                    });

                    // add new forced day and refresh list
                    $("#admin-table-addforced").on("click", function() {
                        if ($("#admin-table-addforced").hasClass("disabled")) return;

                        $("#admin-table-addforced").addClass("disabled");

                        var date = $('#datetimepicker1').data("DateTimePicker").date();
                        $.ajax({
                            url: '{!! route('admin.reservations.table.ajax-set-forcedday') !!}',
                            method: 'POST',
                            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                            dataType: 'json',
                            data: {
                                date: date.format("YYYY-MM-DD")
                            },
                            success: function () {
                                loadForceddays();
                                $("#admin-table-addforced").removeClass("disabled");
                            }
                        });
                    });

                    $("body").on("click", ".admin-list-button", function() {
                        var date = $(this).data("date");
                        $.ajax({
                            url: '{!! route('admin.reservations.table.ajax-remove-forcedday') !!}',
                            method: 'POST',
                            headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                            dataType: 'json',
                            data: {
                                date: date
                            },
                            success: function () {
                                loadForceddays();
                            }
                        });
                    })
                });
            </script>
@endsection
