@extends('admin.layouts.main')

@section('page-content')

<style>
    #table-picker table td.day {
        background-color: #b99670;
        border-radius: 100px;
        height: 35px!important;
        width: 35px !important;
        color: #ffffff;
        font-family: arial, sans-serif;
        font-size: 20px;
        text-align: center;
    }

    #table-picker table td.day.selected {
        background-color: green !important;
    }

    #table-picker table td.day.full {
        background-color: #ea725e;
        border-radius: 100px;
        height: 35px!important;
        width: 35px !important;
        color: #ffffff;
        font-family: arial, sans-serif;
        font-size: 20px;
    }

    #table-picker th.picker-switch {
        text-align: center;
    }
</style>

<a href="{{ route('admin.reservations.table.create') }}"><span class="btn btn-primary pull-right">{{ trans('admin.new-reservation') }}</span></a>

<div class="form-group col-md-6">
    <div id="table-picker">
        <input type="hidden" name="time" id="table-time">
        <div class="datepicker-days">
            <table class="table-condensed">
                <thead>
                <tr>
                    <th></th>
                    <th class="prev"><span class="glyphicon glyphicon-chevron-left" title="Previous Week" id="picker-prevPage"></span></th>
                    <th class="picker-switch" colspan="5" title="Select Month"><span id="table-picker-title-from"></span> - <span id="table-picker-title-to"></span></th>
                    <th class="next" ><span class="glyphicon glyphicon-chevron-right" title="Next Week" id="picker-nextPage"></span>
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(0)->dayOfWeek) }}</th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(1)->dayOfWeek) }}</th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(2)->dayOfWeek) }}</th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(3)->dayOfWeek) }}</th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(4)->dayOfWeek) }}</th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(5)->dayOfWeek) }}</th>
                    <th class="dow">{{ trans('strings.days-short.'.\Carbon\Carbon::today()->addDays(6)->dayOfWeek) }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($times as $time)
                    <tr class="table-picker-row" data-time = '{{ $time }}'>
                        <td>{{ $time }}</td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<table id="admin-events-table" class="display table table-bordered table-hover table-striped" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Edit</th>
        <th>Name</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Time</th>
        <th>Guests</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Edit</th>
        <th>Name</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Time</th>
        <th>Guests</th>
    </tr>
    </tfoot>
    <tbody>

    </tbody>
</table>
@endsection

@section('page-assets')
    <script>
        'use strict';

        var dt;

        var disabledTableDays = [ {!! $disabledTableDays !!} ];

        var reservations = {!! json_encode($reservations) !!}; // object holding every reservation
        var tableReservations = [];

        var limit = 3;//{{ $limit }};
        var pageLimit = 300; // generated days (server side) / 7 (1 week)

        var page = 0; // page number for paginator

        var genTitle = function() {
            $("#table-picker-title-from").html(moment().startOf('day').add("days", (page*7)).format('YYYY-MM-DD'));
            $("#table-picker-title-to").html(moment().startOf('day').add("days", 6 + page*7).format('YYYY-MM-DD'));
        }

        var genTable = function() {
            // resetting
            $('#table-picker td:not(:first-child)').html('');
            $('#table-picker td:not(:first-child)').removeClass('full');
            $('#table-picker td:not(:first-child)').removeClass('selected');
            $("#table-time").val('');

            // refreshing
            $('.table-picker-row').each(function(rowkey, rowval) { // rows
                var row = $(this);
                $(this).children('td').each(function(key, val) { // cols
                    if (key == 0) return; // don't do anything with first col - it's a label
                    var time = $(rowval).data('time'); // first col label
                    var hour = time.substr(0,2); // hour part for date manipulation

                    var theDate = moment().startOf('day').add("days", (key-1) + (page*7) ).add("hours", hour);
                    var datetimeStr = theDate.format('YYYY-MM-DD HH:mm:ss');
                    $(this).attr('data-datetime', datetimeStr);

                    if ($.inArray(theDate.format('YYYY-MM-DD'), disabledTableDays) > -1) {
                        $(this).addClass('full');
                    }
                    if (reservations[time] == null || reservations[time] === undefined/* || reservations[time][datetimeStr] == null || reservations[time][datetimeStr] === undefined*/) return; // skip if there's no elements for this hour
                    if (reservations[time][datetimeStr] == null || reservations[time][datetimeStr] === undefined) return; // count of reservation

                    var count = reservations[time][datetimeStr]
                    $(this).html(count);
                    if (count >= limit)
                        $(this).addClass('full');
                })
            })
        }

        $("#picker-nextPage").on("click", function() {
            if (page == pageLimit-1) return false;
            page++;
            genTitle();
            genTable();
        });

        $("#picker-prevPage").on("click", function() {
            if (page == 0) return false;
            page--;
            genTitle();
            genTable();
        });

        var loadReservations = function(datetime) {
            $.ajax({
                type: 'POST',

                headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
                url: '{!! route('admin.reservations.table.ajax-get') !!}',
                dataType: 'json',
                data: {
                    'datetime': datetime
                },
                success: function (data) {
                    $(data).each(function(key, val) {
                        tableReservations.push([
                            '<a href="/admin/reservations/table/'+val.id+'/edit" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a>',
                            val.name,
                            val.address,
                            val.phone,
                            val.email,
                            val.time,
                            val.guest_count
                        ])
                    });

                    dt.clear(); // redundant clear to be sure
                    dt.rows.add(tableReservations);
                    dt.draw();

                    tableReservations = [];
                }
            });
        }

        $(function () {
            dt = $('#admin-events-table').DataTable();

            genTitle();
            genTable();

            $("#table-picker .day").on('click', function() {
                dt.clear();

                $("#table-picker .selected").removeClass('selected');
                $(this).addClass('selected');
                $("#table-time").val($(this).data('datetime'));

                loadReservations($(this).data('datetime'));
            });
        });
    </script>
@endsection
