@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => ['admin.reservations.table.store'], 'method' => 'post')) !!}

    <div class="form-group">
        <label for="name">{{ trans('forms.reservation.table.name') }}</label>
        <input type="text" class="form-control" name="name" value="" required>
    </div>
    <div class="form-group">
        <label for="address">{{ trans('forms.reservation.table.address') }}</label>
        <input type="text" class="form-control" name="address" value="">
    </div>
    <div class="form-group">
        <label for="phone">{{ trans('forms.reservation.table.tel') }}</label>
        <input type="text" class="form-control" name="phone" value="" required>
    </div>
    <div class="form-group">
        <label for="email">{{ trans('forms.reservation.table.email') }}</label>
        <input type="email" class="form-control" name="email" value="" required>
    </div>
    <div class="form-group">
        <label for="allday">{{ trans('forms.reservation.table.all-day') }}</label>
        <input type="checkbox" id="table-allday-toggle" class="form-control" name="allday" value="1">
    </div>
    <div class="form-group">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                <label for="date">{{ trans('forms.reservation.table.date') }}*</label>
                <div class='input-group date' id='datetimepicker7'>
                    <input type='text' name="date" class="form-control table-time-control" id="table-date-input" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                <label for="time">{{ trans('forms.reservation.table.time') }}*</label>
                <select name="time" id="table-time-select" class="form-control table-time-control" required= >
                    <option value=""></option>
                    @foreach($times as $time)
                        <option value="{{ $time }}">{{ $time }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='col-lg-4 col-md-4'>
            <div class="form-group">
                <label for="guests">{{ trans('forms.reservation.table.guest-count') }}*</label>
                <input type="number" class="form-control" name="guests" min="0" max id="guest-count" required>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="comment">{{ trans('forms.reservation.table.comment') }}</label>
        <textarea class="form-control" name="comment" rows="5"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
    <a href="{{ route('admin.reservations.table.index') }}" class="btn btn-default">{{ trans('forms.cancel') }}</a>

{!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        ;'use strict';

        var disabledTimes = [
            @forelse($disabledTimes as $time)
                '{!! $time->time !!}',
            @empty
            @endforelse
        ];

        var calculateDisabled = function() {
            $("#table-time-select").val('0');
            $("#table-time-select option").each(function () {
                var td = $(this);
                var dateTime = $("#table-date-input").val() + ' ' + $(this).val() + ":00";
                if (disabledTimes.indexOf(dateTime) > -1)
                    $(td).prop("disabled", true);
                else $(td).prop("disabled", false);
            });
        }

        $(function () {
            var dtp = $('#datetimepicker7').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                format: "YYYY-MM-DD",
                minDate: moment().startOf("day")
            });

            //calculateDisabled(false);

            dtp.on("dp.change", function() {
                $("#table-time-select").val('0'); // on first load, the time is needed
                $("#table-time-select option").each(function () {
                    var td = $(this);
                    console.log(td.val());
                    var dateTime = $("#table-date-input").val() + ' ' + $(this).val() + ":00";
                    if (disabledTimes.indexOf(dateTime) > -1)
                        $(td).prop("disabled", true);
                    else $(td).prop("disabled", false);
                });
            });

            $("#table-time-select").on("change", function() {
                var that = $(this);
                $.ajax({
                    type: 'POST',

                    headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
                    url: '{!! route('reservation.table.ajax-get-count') !!}',
                    dataType: 'text',
                    data: {
                        'datetime': $("#table-date-input").val() + ' '+ $(that).val()
                    },
                    success: function (data) {
                        $("#guest-count").attr("max", data);
                    }
                });
            });

            /*
            $("#table-allday-toggle").on("change", function() {
                if ($("#table-allday-toggle").is(":checked"))
                    $(".table-time-control").attr('disabled', 'disabled');
                else
                    $(".table-time-control").removeAttr('disabled');
            });
            */
        });
    </script>
@endsection
