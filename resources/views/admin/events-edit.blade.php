@extends('admin.layouts.main')

@section('page-content')

<div id="delModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('forms.dialog.warning') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ trans('forms.dialog.delete') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.cancel') }}</button>
                <a href="{{ route('admin.events.delete', [$event->id]) }}" class="btn btn-danger">{{ trans('forms.delete') }}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{!! Form::open(array('route' => ['admin.events.update', $event->id], 'method' => 'post', 'files' => true)) !!}

    <div class="form-group">
        <div class="col-md-6">
            <label for="days">{{ trans('forms.events.when') }}</label>
            <div class='input-group date' id='datetime'>
                <input type='text' class="form-control" name="datetime"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <div class="col-md-6">
            <label for="days">{{ trans('forms.events.days') }}</label>
            <input type="number" name="days" value="{{ $event->days }}" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="title_hu">{{ trans('forms.events.title') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="title_hu" value="{{ $event->getTranslation('hu')->title }}" required>
    </div>
    <div class="form-group">
        <label for="text_hu">{{ trans('forms.events.text') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea rows="5"class="form-control" name="text_hu" required>{{ $event->getTranslation('hu')->text }}</textarea>
    </div>
    <div class="form-group">
        <label for="title_en">{{ trans('forms.events.title') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" name="title_en" value="{{ $event->getTranslation('en')->title }}" required>
    </div>
    <div class="form-group">
        <label for="text_en">{{ trans('forms.events.text') }} - {{ trans('strings.English') }}</label>
        <textarea rows="5"class="form-control" name="text_en" required>{{ $event->getTranslation('en')->text }}</textarea>
    </div>
    <div class="form-group">
        <label for="title_de">{{ trans('forms.events.title') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" value="{{ $event->getTranslation('de')->title }}" name="title_de" required>
    </div>
    <div class="form-group">
        <label for="text_de">{{ trans('forms.events.text') }} - {{ trans('strings.German') }}</label>
        <textarea rows="5"class="form-control" name="text_de"  required>{{ $event->getTranslation('de')->text }}</textarea>
    </div>
    <div class="form-group">
        @if($hasImage)
        <div class="row-fluid">
            <h4>{{ trans('forms.image') }}</h4>
            <img style="max-width: 300px; margin-bottom: 10px;" src="/uploads/events/{{ $event->id }}.jpg">
        </div>
        @endif
        <div class="row-fluid">
            <label for="text_de">{{ trans('forms.change-image') }}</label>
            <input id="file-input" name="file" type="file">
        </div>
    </div>


    <div class="row-fluid" style="margin-bottom: 20px;">
        <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
        <a class="btn btn-default" href="{{ route('admin.events') }}">{{ trans('forms.cancel') }}</a>
        <button type="button" data-toggle="modal" data-target="#delModal" class="btn btn-danger pull-right">{{ trans('forms.delete') }}</button>
    </div>


{!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        $(function () {
            $('#datetime').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                defaultDate: '{{ $event->datetime }}',
                format: 'YYYY-MM-DD HH:mm',
                sideBySide: true
            });

            $("#file-input").fileinput({
                allowedFileTypes: ['image'],
                showUpload: false,
                showPreview: false,
                language: 'hu',
            })
        });
    </script>
@endsection
