@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => 'admin.contacts.update', 'method' => 'post')) !!}

    <h4>{{ trans('strings.Hungarian') }}</h4>
    <div class="form-group">
        <label for="address_hu">{{ trans('forms.contact.address') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="address_hu" value="{{ $pageSettings['address_hu'] }}" required>
    </div>
    <div class="form-group">
        <label for="tel1_hu">{{ trans('forms.contact.tel1') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="tel1_hu" value="{{ $pageSettings['tel1_hu'] }}" required>
    </div>
    <div class="form-group">
        <label for="tel2_hu">{{ trans('forms.contact.tel2') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="tel2_hu" value="{{ $pageSettings['tel2_hu'] }}" required>
    </div>
    <div class="form-group">
        <label for="email_hu">{{ trans('forms.contact.email') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="email_hu" value="{{ $pageSettings['email_hu'] }}" required>
    </div>


    <h4>{{ trans('strings.English') }}</h4>
    <div class="form-group">
        <label for="address_en">{{ trans('forms.contact.address') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="address_en" value="{{ $pageSettings['address_en'] }}" required>
    </div>
    <div class="form-group">
        <label for="tel1_en">{{ trans('forms.contact.tel1') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="tel1_en" value="{{ $pageSettings['tel1_en'] }}" required>
    </div>
    <div class="form-group">
        <label for="tel2_en">{{ trans('forms.contact.tel2') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="tel2_en" value="{{ $pageSettings['tel2_en'] }}" required>
    </div>
    <div class="form-group">
        <label for="email_en">{{ trans('forms.contact.email') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="email_en" value="{{ $pageSettings['email_en'] }}" required>
    </div>


    <h4>{{ trans('strings.German') }}</h4>
    <div class="form-group">
        <label for="address_de">{{ trans('forms.contact.address') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="address_de" value="{{ $pageSettings['address_de'] }}" required>
    </div>
    <div class="form-group">
        <label for="tel1_de">{{ trans('forms.contact.tel1') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="tel1_de" value="{{ $pageSettings['tel1_de'] }}" required>
    </div>
    <div class="form-group">
        <label for="tel2_de">{{ trans('forms.contact.tel2') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="tel2_de" value="{{ $pageSettings['tel2_de'] }}" required>
    </div>
    <div class="form-group">
        <label for="email_de">{{ trans('forms.contact.email') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="email_de" value="{{ $pageSettings['email_de'] }}" required>
    </div>


    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>

{!! Form::close() !!}
@endsection
