@extends('admin.layouts.main')

@section('page-content')

@if(!empty($videos))
<ul>
	@foreach($videos as $video)
		<li>{{ $video->title }} - <a href="{!! route('admin.videos.delete', [$video->id]) !!}">{{ trans('forms.delete') }}</a></li>
	@endforeach
</ul>
@endif

{!! Form::open(array('route' => 'admin.videos.store', 'method' => 'post')) !!}
<div class="well">
	<h4>{{ trans('forms.videos.new') }}</h4>
	<div class="form-group">
		<label for="title">{{ trans('forms.videos.title') }}</label>
		<input type="text" name="title" class="form-control">
	</div>
	<div class="form-group">
		<label for="embedcode">{{ trans('forms.videos.link') }}</label>
		<textarea name="embedcode" class="form-control" rows="7"></textarea>
	</div>
	<button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
</div>
{!! Form::close() !!}

@endsection