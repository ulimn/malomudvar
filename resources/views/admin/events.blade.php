@extends('admin.layouts.main')

@section('page-content')

<a href="{{ route('admin.events.create') }}"><span class="btn btn-primary">{{ trans('admin.events-new') }}</span></a>

<table id="admin-events-table" class="display table table-bordered table-hover table-striped" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Edit</th>
        <th>Date/Time</th>
        <th>Day</th>
        <th>Title</th>
        <th>Text</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Edit</th>
        <th>Date/Time</th>
        <th>Day</th>
        <th>Title</th>
        <th>Text</th>
    </tr>
    </tfoot>
    <tbody>
        @foreach($events as $event)
            <tr data-event-id="{{ $event->id }}">
                <td>{!! link_to_route('admin.events.edit', '', array('id' => $event->id), array('class' => 'glyphicon glyphicon-pencil btn btn-default')) !!}</td>
                <td>{{ $event->datetime }}</td>
                <td>{{ $event->days }}</td>
                <td>{{ $event->getTranslation()->title }}</td>
                <td>{{ $event->getTranslation()->text }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('page-assets')
    <script>
        'use strict';

        $(function() {
            $('#admin-events-table').DataTable();
        })
    </script>
@endsection
