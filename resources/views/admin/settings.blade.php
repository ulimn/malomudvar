@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => 'admin.settings.store', 'method' => 'post')) !!}

    <div class="form-group">
        <label for="sitename_hu">{{ trans('forms.settings.sitename') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" id="contact-email" name="sitename_hu" value="{{ $pageSettings['sitename_hu'] }}" required>
    </div>
    <div class="form-group">
        <label for="slogan_hu">{{ trans('forms.settings.slogan') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" id="contact-email" name="slogan_hu" value="{{ $pageSettings['slogan_hu'] }}" required>
    </div>
    <div class="form-group">
        <label for="sitename_en">{{ trans('forms.settings.sitename') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" id="contact-email" name="sitename_en" value="{{ $pageSettings['sitename_en'] }}" required>
    </div>
    <div class="form-group">
        <label for="slogan_en">{{ trans('forms.settings.slogan') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" id="contact-email" name="slogan_en" value="{{ $pageSettings['slogan_en'] }}" required>
    </div>
    <div class="form-group">
        <label for="sitename_de">{{ trans('forms.settings.sitename') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" id="contact-email" name="sitename_de" value="{{ $pageSettings['sitename_de'] }}" required>
    </div>
    <div class="form-group">
        <label for="slogan_de">{{ trans('forms.settings.slogan') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" id="contact-email" name="slogan_de" value="{{ $pageSettings['slogan_de'] }}" required>
    </div>
    <div class="form-group">
        <label for="contact_route_hu">{{ trans('forms.settings.contact-route') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control"  name="contact_route_hu" value="{{ $pageSettings['contact_route_hu'] }}">
    </div>
    <div class="form-group">
        <label for="contact_route_en">{{ trans('forms.settings.contact-route') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control"  name="contact_route_en" value="{{ $pageSettings['contact_route_en'] }}">
    </div>
    <div class="form-group">
        <label for="contact_route_de">{{ trans('forms.settings.contact-route') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control"  name="contact_route_de" value="{{ $pageSettings['contact_route_de'] }}">
    </div>
    <div class="form-group">
        <label for="company_info_hu">{{ trans('forms.settings.company_info') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea class="form-control" rows="5" name="company_info_hu">{{ $pageSettings['company_info_hu'] }}</textarea>
    </div>
    <div class="form-group">
        <label for="company_info_en">{{ trans('forms.settings.company_info') }} - {{ trans('strings.English') }}</label>
        <textarea class="form-control" rows="5" name="company_info_en">{{ $pageSettings['company_info_en'] }}</textarea>
    </div>
    <div class="form-group">
        <label for="company_info_de">{{ trans('forms.settings.company_info') }} - {{ trans('strings.German') }}</label>
        <textarea class="form-control" rows="5"  name="company_info_de">{{ $pageSettings['company_info_de'] }}</textarea>
    </div>


    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>

{!! Form::close() !!}
@endsection
