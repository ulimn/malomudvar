@extends('admin.layouts.main')

@section('page-content')

    {!! Form::open(array('route' => ['admin.banner.save'], 'method' => 'post', 'files' => true)) !!}
        <div class="row-fluid">
            <label for="text_de">{{ trans('forms.change-image') }}</label>
            <input id="file-input" name="file" type="file">
        </div>
        <div class="row-fluid">
        (Üres kép esetén törlődik a jelenlegi)
        </div>

        <input type="submit" class="btn btn-primary" value="{{ trans('forms.save') }}">
    {!! Form::close() !!}
@endsection

@section('page-assets')
    <script>
        'use strict';

        $(function() {
            $("#file-input").fileinput({
                allowedFileTypes: ['image'],
                showUpload: false,
                showPreview: false,
                language: 'hu',
            })
        })
    </script>
@endsection
