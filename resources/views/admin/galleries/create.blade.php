@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => 'admin.galleries.store', 'method' => 'post')) !!}

    <div class="form-group">
        <label for="name_hu">{{ trans('forms.galleries.name') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" id="contact-email" name="name_hu" value="" required>
    </div>

    <div class="form-group">
        <label for="name_en">{{ trans('forms.galleries.name') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" id="contact-email" name="name_en" value="" required>
    </div>

    <div class="form-group">
        <label for="name_de">{{ trans('forms.galleries.name') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" id="contact-email" name="name_de" value="" required>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>

{!! Form::close() !!}
@endsection
