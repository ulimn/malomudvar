@extends('admin.layouts.main')

@section('page-content')

<div id="delModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('forms.dialog.warning') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ trans('forms.dialog.delete') }};</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.cancel') }}</button>
                <a href="{{ route('admin.galleries.delete', [$gallery->id]) }}" class="btn btn-danger">{{ trans('forms.delete') }}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{!! Form::open(array('route' => ['admin.galleries.update', $gallery->id], 'method' => 'post')) !!}

    <div class="form-group">
        <label for="name_hu">{{ trans('forms.galleries.name') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" id="contact-email" name="name_hu" value="{{ $gallery->getTranslation('hu')->name }}" required>
    </div>

    <div class="form-group">
        <label for="name_en">{{ trans('forms.galleries.name') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" id="contact-email" name="name_en" value="{{ $gallery->getTranslation('en')->name }}" required>
    </div>

    <div class="form-group">
        <label for="name_de">{{ trans('forms.galleries.name') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" id="contact-email" name="name_de" value="{{ $gallery->getTranslation('de')->name }}" required>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
    <a class="btn btn-default" href="{{ route('admin.galleries') }}">{{ trans('forms.cancel') }}</a>
    <button type="button" data-toggle="modal" data-target="#delModal" class="btn btn-danger pull-right">{{ trans('forms.delete') }}</button>

{!! Form::close() !!}
@endsection