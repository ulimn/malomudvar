@extends('admin.layouts.main')

@section('page-content')

<table id="admin-galleries-table" class="display table table-bordered table-hover table-striped" cellspacing="0"
       width="100%">
    <thead>
        <tr>
            <th>Open</th>
            <th>Edit</th>
            <th>Name</th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <th>Open</th>
            <th>Edit</th>
            <th>Name</th>
        </tr>
    </tfoot>

    <tbody>
    @foreach ($galleries as $gallery)
        <tr>
            <td>{!! link_to_route('admin.galleries.show', '', array('id' => $gallery->id), array('class' => 'glyphicon glyphicon-eye-open btn btn-default')) !!}</td>
            <td>{!! link_to_route('admin.galleries.edit', '', array('id' => $gallery->id), array('class' => 'glyphicon glyphicon-pencil btn btn-default')) !!}</td>
            <td>{{ $gallery->getTranslation()->name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('page-assets')
    @parent

    <script>
        $(function () {
            $("#admin-galleries-table").DataTable({
                "order": [[1, "asc"]]
            });
        });
    </script>
@endsection