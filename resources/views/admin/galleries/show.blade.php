@extends('admin.layouts.main')

@section('page-content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <img id="album-image" class="img-responsive"  src="{{ $gallery->getAlbumImageUrl() }}">
                </div>
                <button style="margin: 10px 0 30px" id="set-album-image" type="button" class="btn btn-default pull-left"><span class="glyphicon glyphicon-picture"></span> {{ trans('forms.galleries.choose-featured-image') }}</button>
                <button style="margin: 10px 0 30px 5px" id="gallery-upload-new-button" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> {{ trans('forms.galleries.new-image') }}</button>
            </div>
            <div class="col-md-6">
                <p>{{ trans('forms.galleries.name') }}: {{ $gallery->getTranslation()->name }}</p>
                <p>{{ trans('forms.galleries.image-count') }}: {{ $gallery->imageCount() }}</p>
                <a href="{{ route('admin.galleries.edit', [$gallery->id]) }}">{{ trans('forms.galleries.edit') }}</a>
            </div>
        </div>
        <div class="row">
            <form id="admin-gallery-images-form" class="hidden" action="{{ route('admin.image.store', $gallery->id) }}" method="POST" enctype="multipart/form-data">
                {!! Form::hidden('gallery_id', $gallery->id) !!}
                {!! Form::token() !!}
                <div class="form-group">
                    <label for="###">Új kép feltöltése</label>
                </div>
                <div class="form-group input-right">
                    <input id="input-1a" name="file[]" type="file" class="file" multiple="true" data-show-preview="false" data-language="hu">
                </div>
            </form>
        </div>
        <div class="row" id="images-container">
            @forelse ($gallery->images()->get() as $image)
                <div class="col-lg-2 col-md-5 col-sm-6 col-xs-12 thumbnail-wrapper">
                    <div class="thumbnail">
                        <img class="img-responsive"  src="/uploads/images/thumbnails/{{  $image->filename }}"></div>
                    <p style="word-break: break-all;">{{ $image->filename }}</p>
                    <form method="POST" action="{{ route('admin.galleries.albumimage', ['galleryId' => $gallery->id, 'imageId' => $image->id]) }}" class="set-album-image-button invisible">
                        {!! Form::token() !!}
                        <button type="submit" class="btn btn-default " style="margin-top: -10px; margin-bottom: 10px;">Választ</button>
                    </form>
                    {!! Form::open(array('route' => array('admin.image.destroy', $image->id), 'method' => 'delete', 'class' => 'remove-image-button invisible')) !!}
                    <button type="submit" class="btn btn-danger pull-right " style="margin-top: -10px; margin-bottom: 10px;"><span class="glyphicon glyphicon-remove"></span></button>
                    {!! Form::close() !!}
                </div>
            @empty
                {{ trans('forms.galleries.no-image') }}
            @endforelse
        </div>
    </div>

@endsection

@section('page-assets')
    <style>
        .thumbnail div>img, .thumbnail>img {
            object-fit: contain !important;
            object-position: center !important;
            height: 150px !important;
            width: 200px !important;
        }
        .thumbnail {
            margin-bottom: 5px;
        }
        .thumbnail-wrapper {
            padding-top: 20px;
        }
        .selected-wrapper {
            background: rgba(0 ,30, 200, 0.1);
        }
        form {
            display: inline;
        }
        #album-image {
            max-height: 500px;
        }
    </style>

    <script>
        $("#gallery-upload-new-button").on('click', function () {
            $('#admin-gallery-images-form').toggleClass('hidden');
        })

        $(".thumbnail").parent().hover(function() {
            $(this).addClass('selected-wrapper');
            $(this).children(".remove-image-button").removeClass('invisible');
        }, function() {
            $(this).removeClass('selected-wrapper');
            $(this).children(".remove-image-button").addClass('invisible');
        })

        $('#set-album-image').on('click', function() {
            $(".set-album-image-button").toggleClass('invisible');
            if (!$("#images-container > div").children(".set-album-image-button").hasClass('invisible')) {
                $('html,body').animate({scrollTop: $("#images-container").offset().top}, 'slow');
            }
        })
    </script>
@endsection
