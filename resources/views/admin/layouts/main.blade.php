<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Tell robots to go away -->
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <title>Malom Udvar</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/bower/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Bootstrap DateTimePicker CSS -->
    <link rel="stylesheet" href="{{ asset('assets/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <!-- Bootstrap FileInput CSS -->
    <link href="{{ asset('assets/bower/bootstrap-fileinput/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />

    <!-- jQuery DataTables CSS -->
    <script src="{{ asset('assets/bower/datatables/media/css/jquery.dataTables.min.css') }}"></script>


    <!-- MetisMenu CSS -->
    <link href="{{ asset('assets/bower/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ asset('assets/bower/startbootstrap-sb-admin-2/dist/css/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/bower/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('assets/bower/morrisjs/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('assets/bower/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- BS Swithc -->
    <link href="{{ asset('assets/css/bootstrap-switch.min.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <p class="navbar-brand" href=>{{ config('project.site-title') }} Admin</p>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-flag fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a rel="alternate" hreflang="hu" href="{{LaravelLocalization::getLocalizedURL('hu') }}"><i class="fa fa-flag fa-fw"></i> HU</a></li>
                        <li><a rel="alternate" hreflang="en" href="{{LaravelLocalization::getLocalizedURL('en') }}"><i class="fa fa-flag fa-fw"></i> EN</a></li>
                        <li><a rel="alternate" hreflang="de" href="{{LaravelLocalization::getLocalizedURL('en') }}"><i class="fa fa-flag fa-fw"></i> DE</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <!-- menu -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> {{ trans('admin.dashboard') }}</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> {{ trans('admin.homeblocks') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @foreach($homeblocks as $homeblock)
                                <li>
                                    <a href="{{ route('admin.homeblocks.edit', [$homeblock->id]) }}">{{ $homeblock->getTranslation()->name }}</a>
                                </li>
                                @endforeach
                                <li>
                                    <a href="{{ route('admin.contacts') }}">{{ trans('admin.contact-data') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> {{ trans('admin.pages') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('admin.story.edit') }}">{{ trans('admin.story') }}</a>
                                    <a href="{{ route('admin.restaurant.edit') }}">{{ trans('admin.restaurant') }}</a>
                                    <a href="{{ route('admin.services.edit') }}">{{ trans('admin.services') }}</a>
                                    <a href="{{ route('admin.wellness.edit') }}">{{ trans('admin.wellness') }}</a>
                                    <a href="{{ route('admin.programroom.edit') }}">{{ trans('admin.programroom') }}</a>
                                    <a href="{{ route('admin.watermill.edit') }}">{{ trans('admin.watermill') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> {{ trans('admin.foods') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('admin.food') }}">{{ trans('strings.list') }}</a>
                                    <a href="{{ route('admin.food.cccreate') }}">{{ trans('strings.new') }}</a>
                                    <a href="{{ route('admin.food.categoryimages') }}">{{ trans('admin.food-category-images') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{!! route('admin.reviews') !!}"><i class="fa fa-star-o fa-fw"></i> {{ trans('admin.reviews.title-plural') }}</a>
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-home fa-fw"></i> {{ trans('admin.apartments') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @foreach($apartments as $apartment)
                                <li>
                                    <a href="{{ route('admin.apartments.edit', [$apartment->id]) }}">{{ $apartment->getTranslation()->name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> {{ trans('admin.reservations-room') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('admin.reservations.room.index') }}">{{ trans('strings.list') }}</a>
                                    <a href="{{ route('admin.reservations.room.create') }}">{{ trans('strings.new') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> {{ trans('admin.reservations-table') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('admin.reservations.table.index') }}">{{ trans('strings.list') }}</a>
                                    <a href="{{ route('admin.reservations.table.create') }}">{{ trans('strings.new') }}</a>
                                    <a href="{{ route('admin.reservations.table.settings') }}">{{ trans('strings.settings') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{!! route('admin.events') !!}"><i class="fa fa-calendar fa-fw"></i> {{ trans('admin.events') }}</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-picture-o fa-fw"></i> {{ trans('admin.galleries') }}<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('admin.galleries') }}">{{ trans('strings.list') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.galleries.create') }}">{{ trans('strings.new') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.videos') }}">{{ trans('admin.videos') }}</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{!! route('admin.settings') !!}"><i class="fa fa-wrench fa-fw"></i> {{ trans('admin.settings') }}</a>
                        </li>
                        <li>
                            <a href="{!! route('admin.banner') !!}"><i class="fa fa-lightbulb-o fa-fw"></i> {{ trans('admin.banner') }}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ $pageTitle or 'Admin' }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            @section('page-content')
            @show
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('assets/bower/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/bower/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- jQuery DataTables-->
    <script src="{{ asset('assets/bower/datatables/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- Bootstrap FileInput JavaScript -->
    <script src="{{ asset('assets/bower/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{ asset('assets/bower/bootstrap-fileinput/js/fileinput_locale_hu.js') }}"></script>
    <script src="{{ asset('assets/bower/bootstrap-fileinput/js/fileinput_locale_de.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/bower/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('assets/bower/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/bower/morrisjs/morris.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/bower/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js') }}"></script>

    <!-- Moment JS JavaScript -->
    <script type="text/javascript" src="{{ asset('assets/bower/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower/moment/min/moment-with-locales.min.js') }}"></script>

    <!-- Bootstrap DateTimePicker JavaScript -->
    <script type="text/javascript" src="{{ asset('assets/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- BS Swithc -->
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>

    @section('page-assets')
    @show

</body>

</html>
