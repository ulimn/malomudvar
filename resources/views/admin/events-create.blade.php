@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => 'admin.events.store', 'method' => 'post', 'files' => true)) !!}

    <div class="form-group">
        <div class="col-md-6">
            <label for="days">{{ trans('forms.events.when') }}</label>
            <div class='input-group date' id='datetime'>
                <input type='text' class="form-control" name="datetime"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <div class="col-md-6">
            <label for="days">{{ trans('forms.events.days') }}</label>
            <input type="number" name="days" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="title_hu">{{ trans('forms.events.title') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="title_hu" required>
    </div>
    <div class="form-group">
        <label for="text_hu">{{ trans('forms.events.text') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea class="form-control" name="text_hu" required></textarea>
    </div>
    <div class="form-group">
        <label for="title_en">{{ trans('forms.events.title') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" name="title_en" required>
    </div>
    <div class="form-group">
        <label for="text_en">{{ trans('forms.events.text') }} - {{ trans('strings.English') }}</label>
        <textarea class="form-control" name="text_en" required></textarea>
    </div>
    <div class="form-group">
        <label for="title_de">{{ trans('forms.events.title') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" name="title_de" required>
    </div>
    <div class="form-group">
        <label for="text_de">{{ trans('forms.events.text') }} - {{ trans('strings.German') }}</label>
        <textarea class="form-control" name="text_de" required></textarea>
    </div>

    <div class="form-group">
        <label for="">{{ trans('forms.image') }}</label>
        <input id="file-input" name="file" type="file">
    </div>

    <div class="row-fluid" style="margin-bottom: 20px;">
        <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
        <a class="btn btn-default" href="{{ route('admin.events') }}">{{ trans('forms.cancel') }}</a>
    </div>

{!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        $(function () {
            $('#datetime').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                sideBySide: true,
                format: 'YYYY-MM-DD HH:mm',
            });
        });

        $("#file-input").fileinput({
            allowedFileTypes: ['image'],
            showUpload: false,
            showPreview: false,
            language: 'hu',
        })
    </script>
@endsection
