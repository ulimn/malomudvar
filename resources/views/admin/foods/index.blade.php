@extends('admin.layouts.main')

@section('page-content')

    <table id="admin-foods-table" class="display table table-bordered table-hover table-striped" cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <th>Edit</th>
            <th>Name</th>
            <th>Category</th>
        </tr>
        </thead>

        <tfoot>
        <tr>
            <th>Edit</th>
            <th>Name</th>
            <th>Category</th>
        </tr>
        </tfoot>

        <tbody>
        @foreach ($foods as $food)
            <tr>
                <td>{!! link_to_route('admin.food.edit', '', array('id' => $food->id), array('class' => 'glyphicon glyphicon-pencil btn btn-default')) !!}</td>
                <td>{{ $food->getTranslation()->name }}</td>
                <td>{{ $food->foodcategory()->first()->getTranslation()->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page-assets')
    @parent

    <script>
        $(function () {
            $("#admin-foods-table").DataTable({
                "order": [[2, "asc"]]
            });
        });
    </script>
@endsection