@extends('admin.layouts.main')

@section('page-content')

    @foreach($foodcategories as $key => $foodcategory)
    <h4>{{ $foodcategory->getTranslation()->name }}</h4>

    <div class="row">
        <div class="col-xs-1 col-md-4">
            <img src="/uploads/foodcategories/{{ $key+1 }}/a.jpg">
        </div>
        <div class="col-xs-1 col-md-4">
            <img src="/uploads/foodcategories/{{ $key+1 }}/b.jpg">
        </div>
        <div class="col-xs-1 col-md-4">
            <img src="/uploads/foodcategories/{{ $key+1 }}/c.jpg">
        </div>
    </div>

    <div class="row" id="file-form-{{ $key+1 }}-container">
        <div style="margin: 20px" class="well">
            {!! Form::open(['route' => ['admin.food.categoryimages.update', 'id' => $foodcategory->id], 'files' => true, 'id' => 'upload-{{ $i }}']) !!}
                <input type="hidden" name="category" value="{{ $key+1 }}">
                <div class="form-group">
                    <label for="file">{{ trans('forms.select-image') }}</label>
                    <select class="form-control" name="side">
                        <option value='a'>{{ trans('strings.left') }}</option>
                        <option value='b'>{{ trans('strings.middle') }}</option>
                        <option value='c'>{{ trans('strings.right') }}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="file">{{ trans('forms.browse-image') }}</label>
                    <input class="file-input" id="input-{{ $key+1 }}" name="file" type="file">
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    @endforeach
@endsection

@section('page-assets')
    <script>
        'use strict';

        $(function() {
            $(".file-input").fileinput({
                allowedFileTypes: ['image'],
                showPreview: false,
                language: 'hu',
            })
        });
    </script>
@endsection
