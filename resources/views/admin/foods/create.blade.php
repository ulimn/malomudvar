@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => 'admin.food.store', 'method' => 'post')) !!}

    <div class="form-group">
        <label for="name_hu">{{ trans('forms.foods.name') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="name_hu" required>
    </div>

    <div class="form-group">
        <label for="name_en">{{ trans('forms.foods.name') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" name="name_en" required>
    </div>

    <div class="form-group">
        <label for="name_de">{{ trans('forms.foods.name') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" name="name_de" required>
    </div>

    <div class="form-group">
        <label for="foodcategory">{{ trans('forms.foods.category') }}</label>
        <select class="form-control" name="foodcategory">
            @foreach($foodcategories as $cat)
                <option value="{{ $cat->id }}">{{ $cat->getTranslation()->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="no_egg">{{ trans('forms.foods.no-egg') }}</label>
        <input type="checkbox" value="1" class="form-control bsSwitch" name="no_egg">
    </div>
    <div class="form-group">
        <label for="no_sugar">{{ trans('forms.foods.no-sugar') }}</label>
        <input type="checkbox" value="1" class="form-control bsSwitch" name="no_sugar">
    </div>
    <div class="form-group">
        <label for="no_milk">{{ trans('forms.foods.no-milk') }}</label>
        <input type="checkbox" value="1" class="form-control bsSwitch" name="no_milk">
    </div>
    <div class="form-group">
        <label for="no_wheat">{{ trans('forms.foods.no-wheat') }}</label>
        <input type="checkbox" value="1" class="form-control bsSwitch" name="no_wheat" >
    </div>

    <div class="form-group">
        <label for="price">{{ trans('forms.foods.price') }}</label>
        <input type="number" class="form-control" name="price" required>
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>
    <a class="btn btn-default" href="{{ route('admin.food') }}">{{ trans('forms.cancel') }}</a>

{!! Form::close() !!}
@endsection

@section('page-assets')
<script>
    $(function() {
        $(".bsSwitch").bootstrapSwitch({
            'onText':  '{{ trans('strings.yes') }}',
            'offText':  '{{ trans('strings.no') }}'
        });
    });
</script>
@endsection