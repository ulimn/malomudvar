@extends('admin.layouts.main')

@section('page-content')

{!! Form::open(array('route' => ['admin.apartments.update', $apartment->id], 'method' => 'post', 'files' => true)) !!}

    <h4>{{ trans('strings.Hungarian') }}</h4>
    <div class="form-group">
        <label for="name_hu">{{ trans('forms.apartment.name') }} - {{ trans('strings.Hungarian') }}</label>
        <input type="text" class="form-control" name="name_hu" value="{{ $apartment->getTranslation('hu')->name }}" required>
    </div>
    <div class="form-group">
        <label for="text1_hu">{{ trans('forms.apartment.text1') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea rows="10" class="form-control" name="text1_hu" required>{{ $apartment->getTranslation('hu')->text1 }}</textarea>
    </div>
    <div class="form-group">
        <label for="text2_hu">{{ trans('forms.apartment.text2') }} - {{ trans('strings.Hungarian') }}</label>
        <textarea rows="10" class="form-control" name="text2_hu" required>{{ $apartment->getTranslation('hu')->text2 }}</textarea>
    </div>

    <h4>{{ trans('strings.English') }}</h4>
    <div class="form-group">
        <label for="name_en">{{ trans('forms.apartment.name') }} - {{ trans('strings.English') }}</label>
        <input type="text" class="form-control" name="name_en" value="{{ $apartment->getTranslation('en')->name }}" required>
    </div>
    <div class="form-group">
        <label for="text1_en">{{ trans('forms.apartment.text1') }} - {{ trans('strings.English') }}</label>
        <textarea rows="10" class="form-control" name="text1_en" required>{{ $apartment->getTranslation('en')->text1 }}</textarea>
    </div>
    <div class="form-group">
        <label for="text2_en">{{ trans('forms.apartment.text2') }} - {{ trans('strings.English') }}</label>
        <textarea rows="10" class="form-control" name="text2_en" required>{{ $apartment->getTranslation('en')->text2 }}</textarea>
    </div>

    <h4>{{ trans('strings.German') }}</h4>
    <div class="form-group">
        <label for="name_de">{{ trans('forms.apartment.name') }} - {{ trans('strings.German') }}</label>
        <input type="text" class="form-control" name="name_de" value="{{ $apartment->getTranslation('de')->name }}" required>
    </div>
    <div class="form-group">
        <label for="text1_de">{{ trans('forms.apartment.text1') }} - {{ trans('strings.German') }}</label>
        <textarea rows="10" class="form-control" name="text1_de" required>{{ $apartment->getTranslation('de')->text1 }}</textarea>
    </div>
    <div class="form-group">
        <label for="text2_de">{{ trans('forms.apartment.text2') }} - {{ trans('strings.German') }}</label>
        <textarea rows="10" class="form-control" name="text2_de" required>{{ $apartment->getTranslation('de')->text2 }}</textarea>
    </div>

    <div class="form-group">
        <label for="file-a">{{ trans('forms.apartment.file-left') }}</label>
        <input id="file-a" name="file-a" type="file">
    </div>
    <div class="form-group">
        <label for="file-b">{{ trans('forms.apartment.file-right') }}</label>
        <input id="file-b" name="file-b" type="file">
    </div>

    <button type="submit" class="btn btn-primary">{{ trans('forms.save') }}</button>

{!! Form::close() !!}
@endsection

@section('page-assets')
<script>
    $(function() {
        $("#file-a").fileinput({'showUpload':false, 'previewFileType':'image', 'allowedFileExtensions':['jpg']});
        $("#file-b").fileinput({'showUpload':false, 'previewFileType':'image', 'allowedFileExtensions':['jpg']});
    });
</script>
@endsection
