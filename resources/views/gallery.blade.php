@extends('layouts.normal')

@section('page-content')
    
    <div class="container normal-page">
        <div class="row-centered" style="margin-top: 80px">
            <h1>3d séta</h1>

            <div class="thumbnail">
                <a href="/virtualtour/kesz.html" target="_blank"><img class="img-responsive" src="/assets/img/gallery/galery-3d.jpg"></a>
            </div>
        <div class="row row-centered" style="margin-top: 80px">
            <h1>kép galéria</h1>
        </div>
        <div class="row row-centered">
            @foreach($galleries as $gallery)
            <div class="col-lg-6 col-md-6">
                <div class="thumbnail">
                    <h2>{{ $gallery->getTranslation()->name }}</h2>
                    <a href="{{ route('gallery.show', ['id' => $gallery->id]) }}"><img style="cursor: pointer" class="img-responsive gallery-opener" src="{{ $gallery->getAlbumImageUrl(true) }}"></a>
                </div>
            </div>
            @endforeach
        </div>

        <div class="row row-centered" style="margin-top: 80px">
            <h1>Videók</h1>

            <div class="thumbnail">
                <?php $i = 0; // counter for the videos ?>
                @forelse($videos as $video)
                    {!! $i % 2 === 0 ? '<div class="row"?>' : ''!!}
                        <div class="col-md-6" style="margin-bottom: 10px">
                            <div class="embed-responsive embed-responsive-16by9">{!! $video->embedcode !!}</div>
                        </div>
                    {!! $i % 2 !== 0 ? '</div>' : ''!!}
                    <?php $i++; ?>
                @empty('')
                @endforelse
            </div>
        </div>
    </div>
    </div>
@endsection