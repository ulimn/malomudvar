@extends('layouts.normal')

@section('page-content')
    <div class="container normal-page">
        <div class="row-centered" style="margin-top: 80px">
            <h1>{{ trans('forms.reservation.table.title') }}</h1>
        </div>
        {!! Form::open(array('route' => 'reservation.table.store', 'method' => 'post')) !!}
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="name">{{ trans('forms.reservation.table.name') }}*</label>
                    <input type="text" class="form-control" name="name" value="{{ Request::cookie('MALOMUDVAR_USER_NAME') }}" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="address">{{ trans('forms.reservation.table.address') }}*</label>
                    <input type="text" class="form-control" name="address" value="{{ Request::cookie('MALOMUDVAR_USER_ADDRESS') }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="phone">{{ trans('forms.reservation.table.tel') }}*</label>
                    <input type="text" class="form-control" name="phone" value="{{ Request::cookie('MALOMUDVAR_USER_PHONE') }}" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="email">{{ trans('forms.reservation.table.email') }}*</label>
                    <input type="email" class="form-control" name="email" value="{{ Request::cookie('MALOMUDVAR_USER_EMAIL') }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="form-group">
                    <label for="date">{{ trans('forms.reservation.table.date') }}*</label>
                    <div class='input-group date' id='datetimepicker7'>
                        <input type='text' name="date" class="form-control" id="table-date-input" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="form-group">
                    <label for="time">{{ trans('forms.reservation.table.time') }}*</label>
                    <select name="time" id="table-time-select" class="form-control" required= >
                        <option value=""></option>
                        @foreach($times as $time)
                        <option value="{{ $time }}" {{ isset($_GET['time']) && $time == $_GET['time'] ? 'selected="selected"' : '' }}>{{ $time }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="form-group">
                    <label for="guests">{{ trans('forms.reservation.table.guest-count') }}*</label>
                    <input type="number" class="form-control" name="guests" id="guest-count" required min="0" max="">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="comment">{{ trans('forms.reservation.table.comment') }}</label>
            <textarea class="form-control" name="comment" rows="5"></textarea>
        </div>

        <div class="row-centered">
            <button type="submit" class="btn btn-primary">{{ trans('forms.reservation.table.reserve') }}</button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        'use strict';

        var disabledTimes = [
            @forelse($disabledTimes as $time)
                '{!! $time->time !!}',
            @empty
            @endforelse
        ];
        var unavailableDays = [
            @forelse($unavailableDays as $day)
                '{!! $day !!}',
            @empty
            @endforelse
        ];

        $(function () {
            var dtp = $('#datetimepicker7').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                format: "YYYY-MM-DD",
                minDate: moment().startOf("day"),
                maxDate: moment().startOf("day").add('days', 28),
                disabledDates: unavailableDays,
                {!! isset($_GET['date']) ? "defaultDate: '". $_GET['date'] ."'" : '' !!}
            });

            dtp.on("dp.change", function() {
                $("#table-time-select").val('0');
                $("#table-time-select option").each(function() {
                    var td = $(this);
                    if (disabledTimes.indexOf($("#table-date-input").val()+' '+$(this).val()+":00") > -1) $(td).prop( "disabled", true )
                    else $(td).prop( "disabled", false );
                })
            });

            $("#table-time-select").on("change", function() {
                var that = $(this);
                $.ajax({
                    type: 'POST',

                    headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
                    url: '{!! route('reservation.table.ajax-get-count') !!}',
                    dataType: 'text',
                    data: {
                        'datetime': $("#table-date-input").val() + ' '+ $(that).val()
                    },
                    success: function (data) {
                        $("#guest-count").attr("max", data);
                    }
                });
            });
        });
    </script>
@endsection
