@extends('layouts.normal')

@section('page-content')
    <div class="container normal-page">
        <div class="row-centered" style="margin-top: 80px">
            <h1>{{ trans('forms.reservation.room.title') }}</h1>
        </div>
        <div class="row-fluid">
            <h2>{{ trans('forms.reservation.room.subtitle') }}</h2>

            <h2>{{ trans('forms.reservation.room.subtitle2') }}</h2>
        </div>
        {!! Form::open(array('route' => 'reservation.room.store', 'method' => 'post')) !!}
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="apartment">{{ trans('forms.reservation.room.which-apartment') }}</label>
                    <select class="form-control" name="apartment" id="apartment-selector" required>
                        @foreach($apartments as $apartment)
                            <option {{ ($selectedApartment == $apartment->id) ? 'selected="selected"' : '' }} value="{{ $apartment->id }}">{{ $apartment->getTranslation()->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="name">{{ trans('forms.reservation.room.name') }}*</label>
                    <input type="text" class="form-control" name="name" value="{{ Request::cookie('MALOMUDVAR_USER_NAME') }}" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="address">{{ trans('forms.reservation.room.address') }}</label>
                    <input type="text" class="form-control" name="address" value="{{ Request::cookie('MALOMUDVAR_USER_ADDRESS') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="phone">{{ trans('forms.reservation.room.tel') }}*</label>
                    <input type="text" class="form-control" name="phone" value="{{ Request::cookie('MALOMUDVAR_USER_PHONE') }}" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="email">{{ trans('forms.reservation.room.email') }}*</label>
                    <input type="email" class="form-control" name="email" value="{{ Request::cookie('MALOMUDVAR_USER_EMAIL') }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- innen -->
            <div class='col-md-3'>
                <label for="time">{{ trans('forms.reservation.room.time-begin') }}*</label>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker6'>
                        <input type='text' name="time_from" class="form-control" required />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <label for="time">{{ trans('forms.reservation.room.time-end') }}*</label>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker7'>
                        <input type='text' name="time_to" class="form-control" required />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <!-- eddig -->
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="coupon">{{ trans('forms.reservation.room.coupon') }}</label>
                    <input type="text" class="form-control" name="coupon">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="adults">{{ trans('forms.reservation.room.adult-count') }}*</label>
                    <input type="text" class="form-control" name="adults" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="children">{{ trans('forms.reservation.room.child-count') }}*</label>
                    <input type="text" class="form-control" name="children" required>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="comment">{{ trans('forms.reservation.room.comment') }}</label>
            <textarea class="form-control" name="comment" rows="5"></textarea>
        </div>
        <div class="row-centered">
        <button type="submit" class="btn btn-primary">{{ trans('forms.reservation.room.reserve') }}</button>
    </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('page-assets')
    <script type="text/javascript">
        'use strict';

        $(function () {
            window.disabledDates = new Array();
            window.currentApartment = $('#apartment-selector').val();

            @foreach($disabledDates as $key => $datesForApartment)
                window.disabledDates[{{ $key }}] = [
                    @foreach($datesForApartment as $date)
                        '{{ $date }}',
                    @endforeach
                ];
            @endforeach

            $('#datetimepicker6').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                disabledDates: window.disabledDates[window.currentApartment],
                format: "YYYY-MM-DD",
                minDate: moment(),
                maxDate: moment().add('days', 182),
                sideBySide: true,
                useCurrent: false
            });
            $('#datetimepicker7').datetimepicker({
                locale: '{{ LaravelLocalization::getCurrentLocale() }}',
                disabledDates: window.disabledDates[window.currentApartment],
                minDate: moment(),
                maxDate: moment().add('days', 182),
                format: "YYYY-MM-DD",
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });

            $("#apartment-selector").on('change', function() {
                $('#datetimepicker6').data("DateTimePicker").clear().disabledDates(window.disabledDates[$('#apartment-selector').val()]);
                $('#datetimepicker7').data("DateTimePicker").disabledDates(window.disabledDates[$('#apartment-selector').val()]);
            });
        });
    </script>
@endsection
