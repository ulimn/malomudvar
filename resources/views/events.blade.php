@extends('layouts.normal')

@section('page-content')
<div class="container normal-page">
    <div class="row-centered" style="margin-top: 80px">
        <h1>Események</h1>
    </div>
    @forelse($events as $event)
        <?php $datetime = new DateTime($event->datetime); ?>
        <div class="row events-row hidden-xs">
            <div class="col-md-4">
                <div class="col-md-8">
                    <div class="thumbnail">
                        @if(file_exists('uploads/events/'.$event->id.'.jpg'))
                            <img src="/uploads/events/{{ $event->id }}.jpg">
                        @else
                            <img src="/assets/img/noimage.png">
                        @endif
                    </div>
                </div>
                <div class="col-md-4 event-date">
                    <div class="year">{{ $datetime->format('Y') }}</div>
                    <div class="day">{{ $datetime->format('d') }}</div>
                    <div class="month">{{ $datetime->format('F') }}</div>
                    <div class="hour">{{ $datetime->format('H:i') }}</div>
                    <div class="days">{{ $event->days }} {{ trans('strings.days') }}</div>
                </div>
            </div>
            <div class="col-md-8 event-text">{{  $event->getTranslation()->title }}<br>{{  $event->getTranslation()->text }}</div>
        </div>
        <div class="row events-row visible-xs">
                <div class="col-xs-12">
                    <div class="thumbnail">
                        @if(file_exists('uploads/events/'.$event->id.'.jpg'))
                            <img src="/uploads/events/{{ $event->id }}.jpg">
                        @else
                            <img src="/assets/img/noimage.png">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 event-date" style="margin-top: -35px; margin-left: 5px;">
                    <span class="year">{{ $datetime->format('Y') }}</span>
                    <span class="day" style="font-size: 40px; margin-top: initial; margin-bottom: initial;">{{ $datetime->format('d') }}</span>
                    <span class="month">{{ $datetime->format('F') }}</span>
                    <span class="hour">{{ $datetime->format('H:i') }}</span>
                    <span class="days">{{ $event->days }} {{ trans('strings.days') }}</span>
                </div>
            <div class="col-xs-12 event-text">{{  $event->getTranslation()->title }}<br>{{  $event->getTranslation()->text }}</div>
        </div>
    @empty
        <p>{{ trans('strings.noevents') }}</p>
    @endforelse

    <!-- past events -->

        <div class="row-centered" style="margin-top: 50px"><h2 style="text-transform: uppercase">{{ trans('strings.archive') }}</h2></div>
        <hr style="margin-top: -30px;" class="events-divider">
    @forelse($pastEvents as $event)
        <?php $datetime = new DateTime($event->datetime); ?>
        <div class="row events-row events-past hidden-xs">
            <div class="col-md-4">
                <div class="col-md-8">
                    <div class="thumbnail">
                        @if(file_exists('uploads/events/'.$event->id.'.jpg'))
                            <img src="/uploads/events/{{ $event->id }}.jpg">
                        @else
                            <img src="/assets/img/noimage.png">
                        @endif
                    </div>
                </div>
                <div class="col-md-4 event-date">
                    <div class="year">{{ $datetime->format('Y') }}</div>
                    <div class="day">{{ $datetime->format('d') }}</div>
                    <div class="month">{{ $datetime->format('F') }}</div>
                    <div class="hour">{{ $datetime->format('H:i') }}</div>
                    <div class="days">{{ $event->days }} {{ trans('strings.days') }}</div>
                </div>
            </div>
            <div class="col-md-8 event-text">{{  $event->getTranslation()->title }}<br>{{  $event->getTranslation()->text }}</div>
        </div>
        <div class="row events-row events-past visible-xs">
            <div class="col-xs-12">
                <div class="thumbnail">
                    @if(file_exists('uploads/events/'.$event->id.'.jpg'))
                        <img src="/uploads/events/{{ $event->id }}.jpg">
                    @else
                        <img src="/assets/img/noimage.png">
                    @endif
                </div>
            </div>
            <div class="col-xs-12 event-date" style="margin-top: -35px; margin-left: 5px;">
                <span class="year">{{ $datetime->format('Y') }}</span>
                <span class="day" style="font-size: 40px; margin-top: initial; margin-bottom: initial;">{{ $datetime->format('d') }}</span>
                <span class="month">{{ $datetime->format('F') }}</span>
                <span class="hour">{{ $datetime->format('H:i') }}</span>
                <span class="days">{{ $event->days }} {{ trans('strings.days') }}</span>
            </div>
            <div class="col-xs-12 event-text">{{  $event->getTranslation()->title }}<br>{{  $event->getTranslation()->text }}</div>
         </div>
    @empty
        <p>{{ trans('strings.noevents') }}</p>
    @endforelse
</div>
@endsection
