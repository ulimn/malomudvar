@extends('layouts.onepage')

@section('page-content')
    <div>
        <section id="home" class="home-section">
            <div class="container-fluid">

                <div class="row-fluid row-centered">
                    <h2>· {{ trans('strings.inn') }} ·</h2>
                </div>
                <div class="row-fluid row-centered">
                    <h1>{{ $pageSettings['sitename_'.\LaravelLocalization::getCurrentLocale()] }}</h1>
                </div>
                <div class="row-fluid row-centered">
                    <h2>{{ $pageSettings['slogan_'.\LaravelLocalization::getCurrentLocale()] }}</h2>
                </div>
                <div class="row-fluid row-centered">
                    <a href="/virtualtour/kesz.html" target="_blank" class="btn btn-primary">{{ trans('strings.3d-tour') }}</a>
                </div>

            </div>
        </section>

        <section id="history" class="history-section">
            <div class="container-fluid first">

                <div class="row-fluid row-centered">
                    <img src="/assets/img/rhomboid-sign.png">
                </div>
                <div class="row-fluid row-centered">
                    <h1>{{ $blocks[0]->getTranslation()->name }}</h1>
                </div>
                <div class="row-fluid row-centered">
                    <h2>{{ $blocks[0]->getTranslation()->description }}</h2>
                </div>
            </div>

            <div class="container-fluid second">

            </div>

            <div class="container-fluid third" id="history-onepage-text">
                <div class="onepage-text">
                    <img src="/assets/img/rhomboid-sign.png">
                    <p>{{ $blocks[0]->getTranslation()->content }}</p>
                    <a class="btn btn-sm-primary btn-sm" href="{{ route('story') }}">{{ trans('strings.continue') }}</a>
                </div>
            </div>
        </section>

        <section id="restaurant" class="restaurant-section">
            <div class="container-fluid first">

                <div class="row-fluid row-centered">
                    <img src="/assets/img/rhomboid-sign.png">
                </div>
                <div class="row-fluid row-centered">
                    <h1>{{ $blocks[1]->getTranslation()->name }}</h1>
                </div>
                <div class="row-fluid row-centered">
                    <h2>{{ $blocks[1]->getTranslation()->description }}</h2>
                </div>
            </div>

            <div class="container-fluid second" id="restaurant-onepage-text">
                <div class="onepage-text">
                    <img src="/assets/img/rhomboid-sign.png">
                    <p>{{ $blocks[1]->getTranslation()->content }}</p>
                    <a class="btn btn-sm-primary btn-sm" href="{{ route('restaurant') }}">{{ trans('strings.continue') }}</a>
                </div>
            </div>

        </section>

        <section id="vendeghazak" class="vendeghazak-section">
            <div class="container-fluid first">

                <div class="row-fluid row-centered">
                    <img src="/assets/img/rhomboid-sign.png">
                </div>
                <div class="row-fluid row-centered">
                    <h1>{{ $blocks[2]->getTranslation()->name }}</h1>
                </div>
                <div class="row-fluid row-centered">
                    <h2>{{ $blocks[2]->getTranslation()->description }}</h2>
                </div>

                <div class="onepage-text-vertical hidden-xs">
                    <p>{{ $blocks[2]->getTranslation()->content }}</p>
                    <a class="btn btn-sm-primary btn-sm" href="">{{ trans('strings.continue') }}</a>
                </div>
                <div class="onepage-text visible-xs" style="margin-top:initial;">
                    <p>{{ $blocks[2]->getTranslation()->content }}</p>
                    <a class="btn btn-sm-primary btn-sm" href="">{{ trans('strings.continue') }}</a>
                </div>
            </div>

        </section>

        <section id="contacts" class="contacts-section">
            <div class="container-fluid first">
                <div class="hidden-xs row-fluid row-centered">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-sm-4 col-xs-12 triangle triangle-down">
                            <h3>{{ $apartments[0]->getTranslation()->name }}</h3>
                            <a href="{{ route('apartments', [$apartments[0]->id]) }}">megnézem</a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 triangle triangle-up">
                            <a href="{{ route('apartments', [$apartments[1]->id]) }}">megnézem</a>
                            <h3>{{ $apartments[1]->getTranslation()->name }}</h3>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 triangle triangle-down">
                            <h3>{{ $apartments[2]->getTranslation()->name }}</h3>
                            <a href="{{ route('apartments', [$apartments[2]->id]) }}">megnézem</a>
                        </div>
                    </div>
                </div>
                <div class="visible-xs row-fluid row-centered">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <a style="text-decoration: none; color: initial" href="{{ route('apartments', [$apartments[0]->id]) }}">
                            <div class="onepage-text-vertical-apartment">
                                <div class="onepage-text-vertical-apartment-inner">
                                    <h3>{{ $apartments[0]->getTranslation()->name }}</h3>
                                    <a href="{{ route('apartments', [$apartments[0]->id]) }}">megnézem</a>
                                </div>
                            </div>
                        </a>
                        <a style="text-decoration: none; color: initial" href="{{ route('apartments', [$apartments[1]->id]) }}">
                            <div class="onepage-text-vertical-apartment">
                                <div class="onepage-text-vertical-apartment-inner">
                                    <h3>{{ $apartments[1]->getTranslation()->name }}</h3>
                                    <a href="{{ route('apartments', [$apartments[1]->id]) }}">megnézem</a>
                                </div>
                            </div>
                        </a>
                        <a style="text-decoration: none; color: initial" href="{{ route('apartments', [$apartments[2]->id]) }}">
                            <div class="onepage-text-vertical-apartment">
                                <div class="onepage-text-vertical-apartment-inner">
                                    <h3>{{ $apartments[2]->getTranslation()->name }}</h3>
                                    <a href="{{ route('apartments', [$apartments[2]->id]) }}">megnézem</a>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <!--
                <div class="row-fluid row-centered">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12 triangle triangle-down">
                            <h3>valamilyen vendégház</h3>
                            <a>megnézem</a>
                        </div>
                    </div>
                </div>
                -->
            </div>
        </section>

        <section id="opinions" class="opinions-section">
            <div class="container-fluid first">
                <div class="container">
                    <div class="row">
                        @forelse($reviews as $review)
                        <div data-review=1 class="col-md-6">
                            <div class="col-md-8">
                                <div class="review-name">{{ $review->name }}</div>
                                <div class="review-place">{{ $review->place }}</div></div>
                            <div class="col-md-4">
                                <div class="review-star"><input id="input-3" class="rating form-control hide" data-show-clear="false" data-disabled="true" data-star-caption="false" data-size="sm" data-show-caption="false" data-step="1" name="rating" value="{{ $review->rating }}"></div>
                            </div>
                            <div class="col-md-12 review-text-wrapper">
                                <div class="review-text-arrow"></div>
                                <div class="review-text">{{ $review->review }}</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="col-md-12">
                                </div>
                            </div>
                        </div>
                        @empty
                            <div class="row-centered">
                                <p>{{ trans('strings.no-reviews') }}</p>
                            </div>
                        @endforelse
                    </div>
                    <div class="row-centered">
                        <a href="{{ route('review.create') }}" class="btn btn-primary">{{ trans('strings.rate-button') }}</a>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <footer class="footer">
        <div class="container">
            <div class="bs-example" data-example-id="embedded-scrollspy">
                <nav id="navbar-example2" class="navbar navigation-bar navbar-default navbar-static">
                    <div class="container-fluid">
                        <div id="navbar-example" class="collapse navbar-collapse bs-example-js-navbar-scrollspy">
                            <ul class="nav navbar-nav">
                                <li class="active"><a class="page-scroll" href="#history-onepage-text">{{ $blocks[0]->getTranslation()->name }} <br><span class="glyphicon"><div class="circle"></div></span></a></li>
                                <li class=""><a class="page-scroll" href="#restaurant-onepage-text">{{ $blocks[1]->getTranslation()->name }} <br><span class="glyphicon"><div class="circle"></div></span></a></li>
                                <li class=""><a class="page-scroll" href="#contacts">{{ $blocks[2]->getTranslation()->name }} <br><span class="glyphicon"><div class="circle"></div></span></a></li>
                                <li class=""><a class="page-scroll" href="#opinions">{{ trans('strings.reviews') }} <br><span class="glyphicon"><div class="circle"></div></span></a></li>
                                <li class=""><a class="page-scroll" href="#closing">{{ trans('strings.contacts') }} <br><span class="glyphicon"><div class="circle"></div></span></a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </footer>
@endsection

@section('page-assets')
    <script src="{{ asset('assets/bower/bootstrap-star-rating/js/star-rating.min.js') }}" type="text/javascript"></script>
@endsection
