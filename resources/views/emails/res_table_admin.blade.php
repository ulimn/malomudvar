<div class="email-wrapper">
	<p>Asztal foglalás történt az alábbi adatokkal:</p>

	<ul>
		<li>Név: {{ $name }}</li>
		<li>Email: {{ $email }}</li>
		<li>Telefon: {{ $phone }}</li>
		<li>Időpont: {{ $reservationDate }}</li>
		<li>Személyek száma: {{ $personCount }}</li>
	</ul>
</div>