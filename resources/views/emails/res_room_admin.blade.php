<div class="email-wrapper">
	<p>Szállás foglalás történt az alábbi adatokkal:</p>

	<ul>
		<li>Név: {{ $name }}</li>
		<li>Email: {{ $email }}</li>
		<li>Telefon: {{ $phone }}</li>
		<li>Időpont: {{ $time_from }} - {{ $time_to }}</li>
		<li>Felnőttek száma: {{ $adultCount }}</li>
		<li>Gyerekek száma: {{ $childCount }}</li>
	</ul>
</div>