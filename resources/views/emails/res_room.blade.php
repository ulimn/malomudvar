<div class="email-wrapper">
	<h2>Tisztelt Hölgyem/Uram!</h2>

	<p>Köszönettel vettük szállás foglalását az alábbi adatokkal, amelyet ezúton megerősítünk.</p>

	<ul>
		<li>Foglalás időpontja: {{ $time_from }} - {{ $time_to }} </li>
		<li>Felnőttek száma: {{ $adultCount }}</li>
		<li>Gyerekek száma: {{ $childCount }}</li>
	</ul>

	<p>Barátsággal:<br>Bánfi Anna,<br>Ügyvezető, Tulajdonos</p>
</div>