<div class="email-wrapper">
	<h2>Tisztelt Hölgyem/Uram!</h2>

	<p>Köszönettel vettük asztal foglalását az alábbi adatokkal, amelyet ezúton megerősítünk.</p>

	<ul>
		<li>Foglalás időpontja: {{ $reservationDate }}</li>
		<li>Személyek száma: {{ $personCount }}</li>
	</ul>

	<p>Barátsággal: <br>Bánfi Anna,<br>Ügyvezető, Tulajdonos</p>
</div>