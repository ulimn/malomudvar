@extends('admin.layouts.main'   )

@section('page-content')
    <div>
        <ul>
            <li>{{ trans('strings.id'). ": " . $review->id }}</li>
            <li>{{ trans('admin.reviews.title-single'). ": " . $review->rating }}</li>
            <li>{{ trans('strings.language'). ": " . trans('strings.'.LaravelLocalization::getSupportedLocales()['hu']['name']) }}</li>
            <li>{{ trans('admin.reviews.name'). ": " . $review->name }}</li>
            <li>{{ trans('admin.reviews.place'). ": " . $review->place }}</li>
            <li>{{ trans('admin.reviews.text'). ": " . $review->review}}</li>
            @if($review->approved)
                <a class="btn btn-default" href="{{ route('admin.review.disapprove', [$review->id]) }}">{{ trans('admin.reviews.disapprove') }}</a>
            @else
                <a class="btn btn-default" href="{{ route('admin.review.approve', [$review->id]) }}">{{ trans('admin.reviews.approve') }}</a>
            @endif
            {!! Form::open(['route' => ['admin.review.delete', 'id' => $review->id], 'method' => 'delete']) !!}
            <button class="btn btn-danger" type="submit">{{ trans('admin.reviews.delete') }}</button>
            {!! Form::close() !!}
        </ul>
    </div>
@endsection