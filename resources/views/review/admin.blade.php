@extends('admin.layouts.main')

@section('page-content')

<table id="admin-reviews-table" class="display table table-bordered table-hover table-striped" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Approved</th>
        <th>Lang</th>
        <th>Rating</th>
        <th>Name</th>
        <th>Place</th>
        <th>Review</th>
        <th>Created</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Approved</th>
        <th>Lang</th>
        <th>Rating</th>
        <th>Name</th>
        <th>Place</th>
        <th>Review</th>
        <th>Created</th>
    </tr>
    </tfoot>
    <tbody>
        @foreach($reviews as $review)
            <tr style="cursor: pointer" data-review-id="{{ $review->id }}">
                <td>{{ ($review->approved) ? trans('strings.yes') : trans('strings.no') }}</td>
                <td>{{ $review->language }}</td>
                <td>{{ $review->rating }}</td>
                <td>{{ $review->name }}</td>
                <td>{{ $review->place }}</td>
                <td>{{ $review->review }}</td>
                <td>{{ $review->created_at }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('page-assets')
    <script>
        'use strict';

        $(function() {
            $('#admin-reviews-table').DataTable();
            $('#admin-reviews-table tbody').on('click', 'tr', function () {
                var id = $(this).data('reviewId');
                window.location = '/admin/review/'+id;
            } );
        })
    </script>
@endsection
