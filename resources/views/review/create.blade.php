@extends('layouts.normal')

@section('page-content')
    <div class="container normal-page">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row-centered" style="margin-top: 80px">
            <h1>Vélemény írása</h1>
        </div>
        {!! Form::open(array('route' => 'review.store', 'method' => 'post')) !!}
        <div style="text-align: center">
            <div class="form-group">
                <label for="name">{{ trans('forms.review.rating') }}</label>
                <input id="input-3" class="rating form-control hide" data-show-clear="false" data-star-caption="false" data-show-caption="false" data-step="1" data-size="sm" name="rating" value="{{ old('rating') }}" required>
            </div>
        </div>
        <div class="row-centered">
            <div class="col-md-4 col-md-offset-4">

            <div class="form-group">

                <label for="name">{{ trans('forms.review.name') }}</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" required>
            </div>
            </div>
        </div>
        <div class="row-centered">
            <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                <label for="place">{{ trans('forms.review.place') }}</label>
                <input class="form-control" type="text" name="place" value="{{ old('place') }}" required>
            </div>
        </div>
        </div>
        <div class="row-centered">
            <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label for="text">{{ trans('forms.review.review') }}</label>
                <textarea class="form-control" rows="5" type="text" name="review" cols="80" required>{{ old('review') }}</textarea>
            </div>
            </div>
        </div>
        <div class="row-centered">
            <div class="col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{{ trans('forms.send') }}</button>
            </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection


@section('page-assets')
    <script src="{{ asset('assets/bower/bootstrap-star-rating/js/star-rating.min.js') }}" type="text/javascript"></script>
@endsection