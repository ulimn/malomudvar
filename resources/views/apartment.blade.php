@extends('layouts.normal')

@section('page-content')
<div class="container normal-page">
    <div class="row-centered" style="margin-top: 80px">
        <h1>{{ $apartment->getTranslation()->name }}</h1>
    </div>
    <div class="row row-centered" style="margin-top: 30px">
        <center>
            <div class='input-group date' id='apartment-picker'></div>
        </center>
    </div>
    <div class="row row-centered">
        <a class="btn btn-primary" href="{{ route('reservation.room', [$apartment->id]) }}">{{ trans('strings.reserve-button') }}</a>
    </div>

    <div class="row" style="margin-top: 80px">
        <p>{{ $apartment->getTranslation()->text1 }}</p>
    </div>
    <div class="row">
        <div class="col-md-6">
            <img class="img-responsive center-block apartment-img thumbnail" src="/assets/img/apartments/{{ $apartment->id }}/a.jpg">
        </div>
        <div class="col-md-6">
            <img class="img-responsive center-block apartment-img thumbnail" src="/assets/img/apartments/{{ $apartment->id }}/b.jpg">
        </div>
    </div>
    <div class="row">
        <p>{{ $apartment->getTranslation()->text2 }}</p>
    </div>
</div>


@endsection

@section('page-assets')
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        var dp = $('#apartment-picker').datetimepicker({
            inline: true,
            minDate: moment().startOf('day'),
            maxDate: moment().add('days', 182),
            disabledDates: [
                @foreach($disabledDatesForApartment as $date)
                '{{ $date }}',
                @endforeach
            ],
            useCurrent: false,
        });

        var dpFilterDays = function() {
            $(".day").each(function() {
                var date = $(this).data('day');
                date = moment(date);
                if (date < moment() && !$(this).hasClass('today')) {
                    $(this).addClass("past");
                }
            });
        };
        dpFilterDays();

        // selecting another day
        dp.on("dp.change", function(date, oldDate) {
            console.log("asd");
            dpFilterDays;
            $(".day").each(function() {
                var date = $(this).data('day');
                date = moment(date);
                if (date < moment() && !$(this).hasClass('today')) {
                    $(this).addClass("past");
                }
            });
            $(".day").on("click", handle__dpDayClick);
            return;
        });

        // disable click event on active day
        var handle__dpDayClick = function(that) {

            if ($(that.target).hasClass('active')) {
                return false;
            }
            return;
        }

        // dp update, for times like next/prev button
        dp.on("dp.update", dpFilterDays);
    });
</script>
@endsection