@extends('layouts.normal')

@section('page-content')
<div class="container normal-page etlap">
    <div class="row-centered" style="margin-top: 80px">
        <h1>{{ $data->getTranslation()->name }}</h1>
    </div>
    <div class="row">
        <p>{!! nl2br($data->getTranslation()->text1) !!}</p>
    </div>
    <div class="row" style="margin-top: 80px">
        <div class="col-md-6">
            <img class="img-responsive center-block apartment-img thumbnail" src="/assets/img/restaurant/a.jpg">
        </div>
        <div class="col-md-6">
            <img class="img-responsive center-block apartment-img thumbnail" src="/assets/img/restaurant/b.jpg">
        </div>
    </div>
    <div class="row" style="margin-top: 80px">
        <p>{!! nl2br($data->getTranslation()->text2) !!}</p>
    </div>
</div>
@endsection
