@extends('layouts.normal')

@section('page-content')
<div class="container normal-page etlap">
    <div class="row-centered" style="margin-top: 80px">
        <h1>{{ trans('strings.menu') }}</h1>
    </div>
    <div class="row-centered hidden-xs">
        <div class="col-md-3">
            <img src="/assets/img/etlap/no-egg.png">
            <p>nincs benne tojás</p>
        </div>
        <div class="col-md-3">
            <img src="/assets/img/etlap/no-sugar.png">
            <p>nincs benne cukor</p>
        </div>
        <div class="col-md-3">
            <img src="/assets/img/etlap/no-milk.png">
            <p>nincs benne tej</p>
        </div>
        <div class="col-md-3">
            <img src="/assets/img/etlap/no-wheat.png">
            <p>nincs benne buza</p>
        </div>
    </div>
    <div class="row-centered visible-xs">
        <div class="col-md-3">
            <img style="width: 40px; height: 40px" src="/assets/img/etlap/no-egg.png">
            <p>nincs benne tojás</p>
        </div>
        <div class="col-md-3">
            <img style="width: 40px; height: 40px" src="/assets/img/etlap/no-sugar.png">
            <p>nincs benne cukor</p>
        </div>
        <div class="col-md-3">
            <img style="width: 40px; height: 40px" src="/assets/img/etlap/no-milk.png">
            <p>nincs benne tej</p>
        </div>
        <div class="col-md-3">
            <img style="width: 40px; height: 40px" src="/assets/img/etlap/no-wheat.png">
            <p>nincs benne buza</p>
        </div>
    </div>

    @foreach($foodcategories as $key => $foodcategory)
    <!-- cat 1 -->
    <div class="row-centered" style="margin-top: 120px">
        <h2>{{ $foodcategory->getTranslation()->name}}</h2>
    </div>
    <div class="row-centered">
        <div class="col-md-4">
        <div class="thumbnail">
            <img src="/uploads/foodcategories/{{$key+1}}/a.jpg">
        </div>
        </div>
        <div class="col-md-4">
        <div class="thumbnail">
            <img src="/uploads/foodcategories/{{$key+1}}/b.jpg">
        </div>
        </div>
        <div class="col-md-4">
        <div class="thumbnail">
            <img src="/uploads/foodcategories/{{$key+1}}/c.jpg">
        </div>
        </div>
    </div>
    <div class="row hidden-xs">
        <table class="table table-striped">
            <tbody>
            @foreach($foodcategory->foods()->get() as $food)
                <tr>
                    <td class="etlap-ico">{!! $food->no_egg == 1 ? '<img src="/assets/img/etlap/no-egg.png">' : '' !!}</td>
                    <td class="etlap-ico">{!! $food->no_sugar == 1 ? '<img src="/assets/img/etlap/no-sugar.png">' : '' !!}</td>
                    <td class="etlap-ico">{!! $food->no_milk == 1 ? '<img src="/assets/img/etlap/no-milk.png">' : '' !!}</td>
                    <td class="etlap-ico">{!! $food->no_wheat == 1 ? '<img src="/assets/img/etlap/no-wheat.png">' : '' !!}</td>
                    <td>{{ $food->getTranslation()->name }}</td>
                    <td style="text-align: right;">{{ $food->price }} Ft</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row visible-xs">
        <table class="table table-striped">
            <tbody>
            @foreach($foodcategory->foods()->get() as $food)
                <tr>
                    <td colspan="5" style="padding-top: 15px; padding-bottom:0px; font-size: 16px;">{{ $food->getTranslation()->name }}</td>
                </tr>
                <tr>
                    <td class="etlap-ico">{!! $food->no_egg == 1 ? '<img style="width:26px; height:26px" src="/assets/img/etlap/no-egg.png">' : '' !!}</td>
                    <td class="etlap-ico">{!! $food->no_sugar == 1 ? '<img style="width:26px; height:26px" src="/assets/img/etlap/no-sugar.png">' : '' !!}</td>
                    <td class="etlap-ico">{!! $food->no_milk == 1 ? '<img style="width:26px; height:26px" src="/assets/img/etlap/no-milk.png">' : '' !!}</td>
                    <td class="etlap-ico">{!! $food->no_wheat == 1 ? '<img style="width:26px; height:26px" src="/assets/img/etlap/no-wheat.png">' : '' !!}</td>
                    <td style="text-align: right;">{{ $food->price }} Ft</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endforeach
</div>
@endsection
