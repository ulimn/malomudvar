@extends('layouts.normal')

@section('page-content')

    {!! Form::open(array('route' => 'contact', 'method' => 'post')) !!}
    <div class="container normal-page">
        <div class="row-centered" style="margin-top: 80px">
            <h1>{{ trans('strings.contacts') }}</h1>

            <h3>{{ trans('strings.contact.general') }}</h3>

            <p>{{ $pageSettings['contact_route_'.\LaravelLocalization::getCurrentLocale()] }}</p>

            <p>{{ trans('strings.contact.address')}}: {{ $pageSettings['address_'.\LaravelLocalization::getCurrentLocale()] }}</p>

            <p>{{ trans('strings.contact.phone')}}: {{ $pageSettings['tel1_'.\LaravelLocalization::getCurrentLocale()] }}</p>

            <h3>{{ trans('strings.contact.company') }}</h3>

            <p>{!! nl2br($pageSettings['company_info_'.\LaravelLocalization::getCurrentLocale()]) !!}</p>

            <h3>{{ trans('strings.contact.contact-us') }}</h3>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="name">{{ trans('forms.contactpage.name') }}</label>
                    <input type="text" class="form-control" id="contact-email" name="name" required>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="email">{{ trans('forms.contactpage.email') }}</label>
                    <input type="email" class="form-control" id="contact-name" name="email" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <label for="message">{{ trans('forms.contactpage.message') }}</label>
                    <textarea class="form-control" id="contact-message" name="message" rows="5" required></textarea>
                </div>
            </div>
        </div>
        <div class="row-centered">
            <button type="submit" class="btn btn-primary">{{ trans('forms.contactpage.submit') }}</button>
        </div>

        {!! Form::close() !!}
        <div class="row-centered">
            <h3>Térkép</h3>

            <iframe class="hidden-xs thumbnail" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2676.5921497523536!2d19.500188315856438!3d47.86686157795555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47403a1712c8663f%3A0x15579a797fcca765!2zVMOhbmNzaWNzIMO6dCA5LCBCw6lyLCAzMDQ1!5e0!3m2!1sen!2shu!4v1458548067083" width="1100" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            <iframe class="visible-xs thumbnail" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2676.5921497523536!2d19.500188315856438!3d47.86686157795555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47403a1712c8663f%3A0x15579a797fcca765!2zVMOhbmNzaWNzIMO6dCA5LCBCw6lyLCAzMDQ1!5e0!3m2!1sen!2shu!4v1458548067083" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>

@endsection
