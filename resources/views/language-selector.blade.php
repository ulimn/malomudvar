<div class="lang-selector hidden-xs">
    <div class="lang {{ LaravelLocalization::getCurrentLocale() == 'hu' ? 'active' : 'inactive' }}"><a rel="alternate" hreflang="hu" href="{{LaravelLocalization::getLocalizedURL('hu') }}"> HU</a></div>
    <div class="lang {{ LaravelLocalization::getCurrentLocale() == 'en' ? 'active' : 'inactive' }}"><a rel="alternate" hreflang="en" href="{{LaravelLocalization::getLocalizedURL('en') }}"> EN</a></div>
    <!-- temporarily disabled TODO: change en locale to de here -->
    <div class="hidden lang {{ LaravelLocalization::getCurrentLocale() == 'de' ? 'active' : 'inactive' }}"><a rel="alternate" hreflang="de" href="{{LaravelLocalization::getLocalizedURL('en') }}"> DE</a></div>
</div>
