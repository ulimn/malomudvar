<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Form Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for Forms and the likes.
    |
    */

    'send' => 'Elküldöm',
    'save' => 'Ment',
    'cancel' => 'Mégse',
    'delete' => 'Töröl',

    'image' => 'Kép',
    'select-image' => 'Kép választása',
    'browse-image' => 'Kép tallózása',
    'change-image' => 'Kép változtatása',

    // galleries
    'errors' => [
        'upload' => 'A feltöltés nem sikerült.',
        'upload-not-fully' => 'A feltöltés nem sikerült maradéktalanul.',
        'thumbnail' => 'A feltöltés nem sikerült. (Thumbnail generálási hiba)',
    ],

    // galleries
    'galleries' => [
        'name' => 'Galéria megnevezése',
        'no-image' => 'Még nincs kép rendelve a galériához.',
        'choose-featured-image' => 'Albumkép választása',
        'new-image' => 'Új kép hozzáadása',
        'image-count' => 'Képek száma',
        'edit' => 'Adatok szerkesztése',
    ],

    // dialogs
    'dialog' => [
        'warning' => 'Figyelem',
        'delete' => 'Biztosan törölni szeretné az elemet?',
    ],

    // foods
    'foods' => [
        'name' => 'Neve',
        'no-egg' => 'Tojás nélkül',
        'no-sugar' => 'Cukor nélkül',
        'no-milk' => 'Tej nélkül',
        'no-wheat' => 'Búza nélkül',
        'category' => 'Kategória',
        'price' => 'Ár',
    ],

    // review page lines
    'review' => [
        'rating' => 'Ön milyen értékelést ad nekünk?',
        'name' => 'Az Ön neve vagy Cégnév',
        'place' => 'Az Ön tartózkodási helye (Budapest)',
        'review' => 'Vélemény írása',
    ],

    // contact page lines
    'contactpage' => [
        'name' => 'Név',
        'email' => 'E-mail cím',
        'message' => 'Levél írása',
        'submit' => 'Elküldés',
    ],

    // reservation page lines
    'reservation' => [
        'table' => [
            'title' => 'Asztal foglalás',
            'name' => 'Név',
            'address' => 'Cím (Város, Utca, Házszám)',
            'tel' => 'Telefonszám (melyen foglalásának napján el tudjuk érni)',
            'email' => 'E-mail cím',
            'date' => 'Dátum',
            'time' => 'Időpont',
            'guest-count' => 'Vendégek száma',
            'comment' => 'Megjegyzés',
            'reserve' => 'Foglalok',
            'all-day' => 'Egész napos esemény'
        ],
        'room' => [
            'title' => 'Szállás foglalás',
            'subtitle' => 'Amennyiben Önnek kényelmesebb, foglalhat szobát telefonon is. Hívjon minket a +36 00 000 0000-as telefonszámon. ',
            'subtitle2' => 'Kérjük pontosan töltse ki az űrlapot, ellenőrizze le a megadott adatokat, majd kattintson a elküldöm gombra! A foglalását követően egy automatikus visszaigazolást küldünk a megadott e-mail címére!.',
            'which-apartment' => 'Válasszon vendégházat',
            'name' => 'Név',
            'address' => 'Cím (Város, Utca, Házszám)',
            'tel' => 'Telefonszám (melyen foglalásának napján el tudjuk érni)',
            'email' => 'E-mail cím',
            'time-begin' => 'Időpont (kezdete)',
            'time-end' => 'Időpont (vége)',
            'coupon' => 'Kuponkód',
            'adult-count' => 'Felnőttek száma',
            'child-count' => 'Gyerekek száma',
            'comment' => 'Megjegyzés',
            'reserve' => 'Foglalok',
        ]
    ],

    // admin/settings page lines
    'settings' => [
        'sitename' => 'A site neve',
        'slogan' => 'Szlogen',
        'contact-route' => 'Út hozzánk',
        'company_info' => 'Cég információ',
    ],

    // admin/settings page lines
    'pages' => [
        'name' => 'Oldal megnevezése',
        'text1' => 'Első Szöveg',
        'text2' => 'Első Szöveg',
        'longtext' => 'Hosszú tartalom',
    ],

    // admin/events/create page lines
    'events' => [
        'title' => 'Esemény címe',
        'text' => 'Szövege',
        'days' => 'Napok',
        'when' => 'Mikor',
    ],

    // admin/events/create page lines
    'homeblocks' => [
        'name' => 'Blokk neve',
        'description' => 'Leírása',
        'content' => 'Tartalma',
    ],

    // admin/contacts form lines
    'contact' => [
        'address' => 'Cím',
        'tel1' => 'Telefon 1',
        'tel2' => 'Telefon 2',
        'email' => 'Email cím',
    ],

    // admin/apartment form lines
    'apartment' => [
        'name' => 'Ház neve',
        'text1' => 'Felső szöveg',
        'text2' => 'Alsó szöveg',
        'file-left' => 'Bal kép',
        'file-right' => 'Jobb kép',
    ],

    // admin/apartment form lines
    'services' => [
        'name' => 'Oldal címe',
        'text1' => 'Szöveg',
        'file-left' => 'Bal kép',
        'file-right' => 'Jobb kép'
    ],

    // admin/apartment form lines
    'wellness' => [
        'name' => 'Oldal címe',
        'text1' => '1. Szöveg',
        'text1' => '2. Szöveg',
        'file-left' => 'Bal kép',
        'file-right' => 'Jobb kép'
    ],

    // admin/apartment form lines
    'programroom' => [
        'name' => 'Oldal címe',
        'text1' => '1. Szöveg',
        'text2' => '2. Szöveg',
        'file-left' => 'Bal kép',
        'file-right' => 'Jobb kép'
    ],

    // admin/apartment form lines
    'watermill' => [
        'name' => 'Oldal címe',
        'text1' => 'Szöveg',
        'text2' => 'Videó link (embed)',
        'file-left' => 'Bal kép',
        'file-right' => 'Jobb kép'
    ],

    // video
    'videos' => [
        'new' => 'Új hozzáadása',
        'title' => 'Cím',
        'link' => 'Hivatkozás'
    ],
];
