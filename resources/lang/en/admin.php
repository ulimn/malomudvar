<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for the admin area.
    |
    */

    'dashboard' => 'Vezérlőpult',
    'pages' => 'Oldalak',
    'homeblocks' => 'Kezdőlap blokkok',

    'restaurant' => 'Étterem',
    'foods' => 'Étlap',
    'foods-new' => 'Új étel hozzáadása',
    'food-category-images' => 'Kategóriák képei',
    'food' => [
        ''
    ],

    'banner' => 'Banner',
    'galleries' => 'Galériák',
    'gallery-new' => 'Új Galéria',
    'gallery-edit' => 'Galéria módosítása',
    'gallery-admin' => 'Galéria adminisztrálása',
    'story' => 'Történetünk',
    'services' => 'Szolgáltatásaink',
    'wellness' => 'Wellness',
    'programroom' => 'Rendezvényterem',
    'watermill' => 'Vízimalom',
    'reservation-edit' => 'Foglalás szerkesztése',
    'reservations-room' => 'Szállás foglalások',
    'reservations-table' => 'Asztal foglalások',
    'new-reservation' => 'Új foglalás',
    'apartment' => 'Vendégház',
    'apartments' => 'Vendégházak',
    'settings' => 'Beállítások',
    'days' => 'Days',
    'forced' => 'Forced',
    'contact-data' => 'Elérhetőségek',
    'events' => 'Események',
    'events-new' => 'Új esemény',
    'events-edit' => 'Esemény szerkesztése',
    'homeblock-edit' => 'Főoldali blokk szerkesztése',
    'indexblock' => 'Kezdőlap Blokkok',
    'reviews' => [
        'title-plural' => 'Értékelések',
        'title-single' => 'Értékelés',
        'approve' => 'Visszaigazol',
        'disapprove' => 'Elutasít',
        'delete' => 'Töröl',
        'name' => 'Név',
        'place' => 'Hely',
        'text' => 'Értékelés szövege',
    ],
    'videos' => 'Videók',

];
