<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for general use in various places.
    |
    */

    'homepage' => 'Home',
    'yes' => 'Yes',
    'no' => 'No',
    'left' => 'Left',
    'middle' => 'Middle',
    'right' => 'Right',
    'continue' => 'Next',
    'id' => 'ID',
    'new' => 'New',
    'add' => 'Add',
    'delete' => 'Delete',
    'list' => 'List',
    'settings' => 'Settings',
    'inn' => 'Inn',
    'menu' => 'Menu',
    '3d-tour' => '3d tour',
    'events' => 'Events',
    'rate-button' => 'Ratings',
    'reviews' => 'Reviews',
    'no-reviews' => 'There are no ratings yet.',
    'gallery' => 'Gallery',
    'reserve' => 'Reservation',
    'reserve-room' => 'Room reservation',
    'reserve-table' => 'Table reservation',
    'reserve-button' => 'Reserve',
    'contacts' => 'Contacts',
    'services' => 'Services',
    'wellness' => 'Wellness',
    'programroom' => 'Program Room',
    'watermill' => 'Watermill',
    'contact' => [
        'general' => 'General contact',
        'company' => 'Company data',
        'address' => 'Address',
        'phone' => 'Phone',
        'contact-us' => 'Contact us',
    ],
    'days-long' => [ // vasárnappal kezdődik! starting with Sunday!
        '0' => 'Sunday',
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
    ],
    'days-short' => [ // vasárnappal kezdődik! starting with Sunday!
        '0' => 'Su',
        '1' => 'Mo',
        '2' => 'Te',
        '3' => 'We',
        '4' => 'Th',
        '5' => 'Fr',
        '6' => 'Sa',
    ],
    'email' => [
        'confirmation' => 'Confirmation',
    ],
    'archive' => 'Archive',
    'noevents' => 'There are no events yet.',
    'days' => 'days',
    'language' => 'Languages',
    'cookie-message' => 'We use cookies to give you the best experience on our site. By continuing to use our website, without changing your browser preferences, you agree to our use of cookies.',
    'cookie-button' => 'OK',
    'Hungarian' => 'Hungarian',
    'English' => 'English',
    'German' => 'German',
    'back' => 'Back',
    'messages' => [
        'successful-reservation' => 'Reservation successfull!'
    ]
];
