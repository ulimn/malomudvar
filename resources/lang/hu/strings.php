<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for general use in various places.
    |
    */

    'homepage' => 'Kezdőlap',
    'yes' => 'Igen',
    'no' => 'Nem',
    'left' => 'Bal',
    'middle' => 'Középső',
    'right' => 'Jobb',
    'continue' => 'Tovább',
    'id' => 'Azonosító',
    'new' => 'Új',
    'add' => 'Hozzáad',
    'delete' => 'Töröl',
    'list' => 'Lista',
    'settings' => 'Beállítások',
    'inn' => 'Fogadó',
    'menu' => 'Étlap',
    '3d-tour' => '3d bejárás',
    'events' => 'Események',
    'rate-button' => 'Értékelés',
    'reviews' => 'Vélemények',
    'no-reviews' => 'Még nincsenek értékelések.',
    'gallery' => 'Galéria',
    'reserve' => 'Foglalás',
    'reserve-room' => 'Szállás foglalás',
    'reserve-table' => 'Asztal foglalás',
    'reserve-button' => 'Lefoglalom',
    'contacts' => 'Elérhetőségek',
    'services' => 'Szolgáltatásaink',
    'wellness' => 'Wellness',
    'programroom' => 'Programterem',
    'watermill' => 'Vízimalom',
    'contact' => [
        'general' => 'Álltalános elérhetőségeink',
        'company' => 'Céges adatok',
        'address' => 'Cím',
        'phone' => 'Telefonszám',
        'contact-us' => 'Írjon nekünk',
    ],
    'days-short' => [ // vasárnappal kezdődik! starting with Sunday!
        '0' => 'V',
        '1' => 'H',
        '2' => 'K',
        '3' => 'Sze',
        '4' => 'Cs',
        '5' => 'P',
        '6' => 'Szo',
    ],
    'days-long' => [ // vasárnappal kezdődik! starting with Sunday!
        '0' => 'Vasárnap',
        '1' => 'Hétfő',
        '2' => 'Kedd',
        '3' => 'Szerda',
        '4' => 'Csütörtök',
        '5' => 'Péntek',
        '6' => 'Szombat',
    ],
    'email' => [
        'confirmation' => 'Visszaigazolás',
    ],
    'archive' => 'Archívum',
    'noevents' => 'Jelenleg még nincsenek események rögzítve.',
    'days' => 'napos',
    'language' => 'Nyelv',
    'cookie-message' => 'A weboldal cookie-kat használ a jobb felhasználói élmény érdekében. A "Rendben" gombra kattintva hozzájárul a cookie-k használatához.',
    'cookie-button' => 'Rendben',
    'Hungarian' => 'Magyar',
    'English' => 'Angol',
    'German' => 'Német',
    'back' => 'Vissza',
    'messages' => [
        'successful-reservation' => 'Sikeres foglalás!'
    ]
];
