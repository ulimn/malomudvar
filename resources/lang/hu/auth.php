<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ezek a belépési adatok nem találhatóak a rendszerünkben.',
    'throttle' => 'Túl sok belépési kísérlet. Kérem, próbálja :seconds múlva.',

];
