<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for general use in various places.
    |
    */

    'cookie-message' => 'Wir verwenden Cookies, um Ihnen die beste Browsererfahrung auf unserer Webseite zu bieten.',
    'cookie-button' => 'Verstanden',

];
