#!/usr/bin/env bash

RED='\033[0;31m'
CYAN='\033[0;36m'
GREEN='\033[1;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No color

check_env() {
	printf "\n${GREEN}Checking .env file...${NC}\n"
	if [ ! -f .env ]; then
	    printf "\n${RED}.env file not found, your application may not be deployed!${NC} "
	    read -r -p "${1:-Continue without .env file? [y/N]} " response
		case $response in
		    [yY][eE][sS]|[yY])
				printf "\n${GREEN}Continuing...${NC}\n"
		        ;;
		    *)
		        printf "\nTo deploy your application, run deploy.sh!"
		        printf "\n${RED}Stopping script.${NC}\n"
		        exit 0
		        ;;
		esac
    else
    	printf "\n${GREEN}.env exists, continuing...${NC}\n"
    fi
}

deploy() {
	printf "\n${GREEN}Running Composer...${NC}\n"
	composer update
	composer dump-autoload

	printf "\n${GREEN}Setting file permissions...${NC}\n"
	chmod -R 664 storage/
	chmod -R +X storage/
	chmod -R 664 bootstrap/cache/
	chmod -R +X bootstrap/cache/

	printf "\n${GREEN}Clearing Laravel cache etc...${NC}\n"
	php artisan cache:clear
	php artisan clear-compiled

	printf "\n${GREEN}Running migrations WITHOUT seeds...${NC}\n"
	php artisan migrate

	printf "\n${GREEN}Running bower...${NC}\n"
	bower install

	printf "\n${GREEN}Update Script End.${NC}\n"
}

printf "\n${CYAN}Laravel Update Script for lazy people :D${NC}\n"

check_env

printf "\n${ORANGE}To set everything correctly, you must be the owner of the files in this folder!${NC}\n"

read -r -p "${1:-Continue? [y/N]} " response
case $response in
    [yY][eE][sS]|[yY])
        deploy
        ;;
    *)
        printf "\n\n${RED}Stopping script.${NC}\n"
        exit 0
        ;;
esac
