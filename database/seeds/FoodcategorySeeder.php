<?php

use App\Models\Foodcategory;
use App\Models\FoodcategoryTranslation;
use Illuminate\Database\Seeder;

class FoodcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Appetizer cat
        $foodcategory = Foodcategory::create();

        // Hungarian
        $fcatTr = new FoodcategoryTranslation();
        $fcatTr->language = 'hu';
        $fcatTr->name = 'Hideg és meleg előételek';
        $foodcategory->translation()->save($fcatTr);

        // German
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'de';
        $fcatTr->name = 'Unsere Hideg és meleg előételek';
        $foodcategory->translation()->save($fcatTr);

        // English
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'en';
        $fcatTr->name = 'Our Hideg és meleg előételek';
        $foodcategory->translation()->save($fcatTr);



      // Create Soups cat
        $foodcategory = Foodcategory::create();

        // Hungarian
        $fcatTr = new FoodcategoryTranslation();
        $fcatTr->language = 'hu';
        $fcatTr->name = 'Levesek';
        $foodcategory->translation()->save($fcatTr);

        // German
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'de';
        $fcatTr->name = 'Suppe';
        $foodcategory->translation()->save($fcatTr);

        // English
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'en';
        $fcatTr->name = 'Soups';
        $foodcategory->translation()->save($fcatTr);


        // Create Maindish cat
        $foodcategory = Foodcategory::create();

        // Hungarian
        $fcatTr = new FoodcategoryTranslation();
        $fcatTr->language = 'hu';
        $fcatTr->name = 'Főételek';
        $foodcategory->translation()->save($fcatTr);

        // German
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'de';
        $fcatTr->name = 'Hauptspeise';
        $foodcategory->translation()->save($fcatTr);

        // English
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'en';
        $fcatTr->name = 'Main dishes';
        $foodcategory->translation()->save($fcatTr);



        // Create Dessert cat
        $foodcategory = Foodcategory::create();

        // Hungarian
        $fcatTr = new FoodcategoryTranslation();
        $fcatTr->language = 'hu';
        $fcatTr->name = 'Desszertek';
        $foodcategory->translation()->save($fcatTr);

        // German
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'de';
        $fcatTr->name = 'Desserte';
        $foodcategory->translation()->save($fcatTr);

        // English
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'en';
        $fcatTr->name = 'Desserts';
        $foodcategory->translation()->save($fcatTr);


        // Create Drinks cat
        $foodcategory = Foodcategory::create();

        // Hungarian
        $fcatTr = new FoodcategoryTranslation();
        $fcatTr->language = 'hu';
        $fcatTr->name = 'Itallap';
        $foodcategory->translation()->save($fcatTr);

        // German
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'de';
        $fcatTr->name = 'de Itallap';
        $foodcategory->translation()->save($fcatTr);

        // English
        $fcatTr = new App\Models\FoodcategoryTranslation;
        $fcatTr->language = 'en';
        $fcatTr->name = 'en Itallap';
        $foodcategory->translation()->save($fcatTr);
    }
}
