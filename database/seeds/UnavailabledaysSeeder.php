<?php

use Illuminate\Database\Seeder;

class UnavailabledaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unavailabledays')->insert([
            ['daynum' => 0, 'unavailable' => 0],
            ['daynum' => 1, 'unavailable' => 0],
            ['daynum' => 2, 'unavailable' => 0],
            ['daynum' => 3, 'unavailable' => 0],
            ['daynum' => 4, 'unavailable' => 0],
            ['daynum' => 5, 'unavailable' => 0],
            ['daynum' => 6, 'unavailable' => 0]
        ]);
    }
}
