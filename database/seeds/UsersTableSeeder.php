<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	private $table = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->insert([
        	'name' => 'Admin',
            'email' => 'admin@admin.hu',
            'password' => bcrypt('admin'),
        ]);
    }
}
