<?php

use App\Models\Food;
use App\Models\FoodTranslation;
use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Food = Food::create([
            'price' => 1500,
            'no_egg' => 1,
            'no_sugar' => 0,
            'no_milk' => 1,
            'no_wheat' => 1,
            'foodcategory_id' => 1,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'Leves 1';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'Leves 1';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'Leves 1';
        $Food->translation()->save($foodTr);







        $Food = Food::create([
            'price' => 50,
            'no_egg' => 1,
            'no_sugar' => 1,
            'no_milk' => 1,
            'no_wheat' => 1,
            'foodcategory_id' => 1,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'Leves 2';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'Leves 2';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'Leves 2';
        $Food->translation()->save($foodTr);








        $Food = Food::create([
            'price' => 15050,
            'no_egg' => 0,
            'no_sugar' => 0,
            'no_milk' => 0,
            'no_wheat' => 0,
            'foodcategory_id' => 1,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'Leves 3';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'Leves 3';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'Leves 3';
        $Food->translation()->save($foodTr);











        $Food = Food::create([
            'price' => 2050,
            'no_egg' => 1,
            'no_sugar' => 0,
            'no_milk' => 0,
            'no_wheat' => 1,
            'foodcategory_id' => 2,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'fő 1';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'fő 1';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'fő 1';
        $Food->translation()->save($foodTr);







        $Food = Food::create([
            'price' => 2550,
            'no_egg' => 1,
            'no_sugar' => 1,
            'no_milk' => 1,
            'no_wheat' => 1,
            'foodcategory_id' => 2,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'fő 2';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'fő 2';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'fő 2';
        $Food->translation()->save($foodTr);












        $Food = Food::create([
            'price' => 550,
            'no_egg' => 0,
            'no_sugar' => 0,
            'no_milk' => 0,
            'no_wheat' => 0,
            'foodcategory_id' => 3,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'inni 1';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'inni 1';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'inni 1';
        $Food->translation()->save($foodTr);







        $Food = Food::create([
            'price' => 1550,
            'no_egg' => 0,
            'no_sugar' => 0,
            'no_milk' => 0,
            'no_wheat' => 0,
            'foodcategory_id' => 3,
        ]);

        // Hungarian
        $foodTr = new FoodTranslation();
        $foodTr->language = 'hu';
        $foodTr->name = 'inni 2';
        $Food->translation()->save($foodTr);

        // German
        $foodTr = new FoodTranslation;
        $foodTr->language = 'de';
        $foodTr->name = 'inni 2';
        $Food->translation()->save($foodTr);

        // English
        $foodTr = new FoodTranslation;
        $foodTr->language = 'en';
        $foodTr->name = 'inni 2';
        $Food->translation()->save($foodTr);
    }
}
