<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // $this->call(UserTableSeeder::class);

        // HomeBlock Seeder
        $this->call(UsersTableSeeder::class);
        $this->call(HomeBlockSeeder::class);
        $this->call(ApartmentSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(PageSeederTwo::class);
        $this->call(FoodcategorySeeder::class);
        $this->call(UnavailabledaysSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
