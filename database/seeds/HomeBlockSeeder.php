<?php

use Illuminate\Database\Seeder;
use App\Models\HomeBlock;
use App\Models\HomeBlockTranslation;

class HomeBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Tortenetunk block
        $block = HomeBlock::create([
            'slug' => 'tortenetunk'
        ]);

        // Hungarian
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'hu';
        $blockTr->name = 'Történetünk';
        $blockTr->description = 'különleges csapat';
        $blockTr->content = 'Lorem ipsum Hungarian';
        $block->translation()->save($blockTr);

        // German
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'de';
        $blockTr->name = 'Unsere Geschichte';
        $blockTr->description = 'Über Tschapat';
        $blockTr->content = 'Lorem ipsum Deutch';
        $block->translation()->save($blockTr);

        // English
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'en';
        $blockTr->name = 'Our Story';
        $blockTr->description = 'Exceptional Team';
        $blockTr->content = 'Lorem ipsum English';
        $block->translation()->save($blockTr);


        // Create Etterem block
        $block = HomeBlock::create([
            'slug' => 'etterem'
        ]);

        // Hungarian
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'hu';
        $blockTr->name = 'Étterem';
        $blockTr->description = 'mennyei ízek';
        $blockTr->content = 'Lorem étterem ipsum Hungarian';
        $block->translation()->save($blockTr);

        // German
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'de';
        $blockTr->name = 'Restaurant';
        $blockTr->description = 'DE Etterem desc';
        $blockTr->content = 'Lorem Etterem ipsum Deutch';
        $block->translation()->save($blockTr);

        // English
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'en';
        $blockTr->name = 'Restaurant';
        $blockTr->description = 'EN Restaurant Desc.';
        $blockTr->content = 'Lorem etterem ipsum English';
        $block->translation()->save($blockTr);


        // Create Vendeghazak block
        $block = HomeBlock::create([
            'slug' => 'vendeghazak'
        ]);

        // Hungarian
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'hu';
        $blockTr->name = 'Vendégházak';
        $blockTr->description = 'otthonos kényelem';
        $blockTr->content = 'Lorem étterem ipsum Hungarian';
        $block->translation()->save($blockTr);

        // German
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'de';
        $blockTr->name = 'Apartments';
        $blockTr->description = 'DE otthonos kényelem';
        $blockTr->content = 'Lorem Etterem ipsum Deutch';
        $block->translation()->save($blockTr);

        // English
        $blockTr = new App\Models\HomeBlockTranslation;
        $blockTr->language = 'en';
        $blockTr->name = 'Apartments';
        $blockTr->description = 'EN otthonos kényelem';
        $blockTr->content = 'Lorem etterem ipsum English';
        $block->translation()->save($blockTr);
    }
}
