<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sitename
        $setting = Setting::create([
            'key' => 'sitename_hu',
            'value' => 'malom udvar'
        ]);
        $setting = Setting::create([
            'key' => 'sitename_en',
            'value' => 'EN - malom udvar'
        ]);
        $setting = Setting::create([
            'key' => 'sitename_de',
            'value' => 'DE - malom udvar'
        ]);

        // slogan
        $setting = Setting::create([
            'key' => 'slogan_hu',
            'value' => 'ázsiai fúziós étterem'
        ]);
        $setting = Setting::create([
            'key' => 'slogan_en',
            'value' => 'EN - ázsiai fúziós étterem'
        ]);
        $setting = Setting::create([
            'key' => 'slogan_de',
            'value' => 'DE - ázsiai fúziós étterem'
        ]);

        // address
        $setting = Setting::create([
            'key' => 'address_hu',
            'value' => 'TANCSICS ÚT.9 NOGRAD'
        ]);
        $setting = Setting::create([
            'key' => 'address_en',
            'value' => 'EN - TANCSICS ÚT.9 NOGRAD'
        ]);
        $setting = Setting::create([
            'key' => 'address_de',
            'value' => 'DE - TANCSICS ÚT.9 NOGRAD'
        ]);

        // tel1
        $setting = Setting::create([
            'key' => 'tel1_hu',
            'value' => '+36 0 000 0000'
        ]);
        $setting = Setting::create([
            'key' => 'tel1_en',
            'value' => 'EN - +36 0 000 0000'
        ]);
        $setting = Setting::create([
            'key' => 'tel1_de',
            'value' => 'DE - +36 0 000 0000'
        ]);

        // tel2
        $setting = Setting::create([
            'key' => 'tel2_hu',
            'value' => '+36 0 000 0000'
        ]);
        $setting = Setting::create([
            'key' => 'tel2_en',
            'value' => 'EN - +36 0 000 0000'
        ]);
        $setting = Setting::create([
            'key' => 'tel2_de',
            'value' => 'DE - +36 0 000 0000'
        ]);

        // email
        $setting = Setting::create([
            'key' => 'email_hu',
            'value' => 'INFO@MALOMUDVAR.HU'
        ]);
        $setting = Setting::create([
            'key' => 'email_en',
            'value' => 'INFO@MALOMUDVAR.HU'
        ]);
        $setting = Setting::create([
            'key' => 'email_de',
            'value' => 'INFO@MALOMUDVAR.HU'
        ]);

        // company info
        $setting = Setting::create([
            'key' => 'company_info_hu',
            'value' => 'Xy Kft.?
AXA Bank 17000000-14040000-00000000
Adószám: 22000000-0-00
Cégjegyzésszám: 15-00-00000'
        ]);
        $setting = Setting::create([
            'key' => 'company_info_en',
            'value' => 'Xy Kft.?
AXA Bank 17000000-14040000-00000000
Adószám: 22000000-0-00
Cégjegyzésszám: 15-00-00000'
        ]);
        $setting = Setting::create([
            'key' => 'company_info_de',
            'value' => 'Xy Kft.?
AXA Bank 17000000-14040000-00000000
Adószám: 22000000-0-00
Cégjegyzésszám: 15-00-00000'
        ]);

        // contact teaser
        $setting = Setting::create([
            'key' => 'contact_route_hu',
            'value' => 'Ide lehetne írni, hogy jutnak el Önökhöz...'
        ]);
        $setting = Setting::create([
            'key' => 'contact_route_en',
            'value' => 'Ide lehetne írni, hogy jutnak el Önökhöz...'
        ]);
        $setting = Setting::create([
            'key' => 'contact_route_de',
            'value' => 'Ide lehetne írni, hogy jutnak el Önökhöz...'
        ]);
    }
}
