<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlldayToTablereservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tablereservations', function(Blueprint $table) {
            $table->boolean('isAllDay')->after('comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tablereservations', function(Blueprint $table) {
            $table->dropColumn('isAllDay');
        });
    }
}
