<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeblockTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeblock_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('homeblock_id');
            $table->string('language');
            $table->string('name');
            $table->string('description');
            $table->string('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homeblock_translations');
    }
}
