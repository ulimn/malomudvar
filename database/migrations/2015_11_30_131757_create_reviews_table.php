<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('rating')->comment('rating given by the author');
            $table->string('language')->comment('language code of the review');
            $table->string('place')->comment('the place given by the author');
            $table->text('review')->comment('text content of the review');
            $table->string('name')->comment('name given by the author');
            $table->boolean('approved')->comment('only admin sets this');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
