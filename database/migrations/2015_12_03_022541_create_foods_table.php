<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('foodcategory_id');
            $table->integer('price')->default(0);
            $table->boolean('no_egg')->default(0);
            $table->boolean('no_sugar')->default(0);
            $table->boolean('no_milk')->default(0);
            $table->boolean('no_wheat')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('foods');
    }
}
