<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomreservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roomreservations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('apartment_id');
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->datetime('time_from');
            $table->datetime('time_to');
            $table->string('coupon')->nullable();
            $table->integer('adult_count');
            $table->integer('child_count');
            $table->text('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roomreservations');
    }
}
