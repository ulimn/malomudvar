<?php

namespace App\Http\Controllers;

use App\Models\EventTranslation;
use App\Models\Food;
use App\Models\Foodcategory;
use App\Models\FoodTranslation;
use App\Models\Gallery;
use App\Models\GalleryTranslation;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Apartment;
use App\Models\ApartmentTranslation;
use App\Models\Event;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\RoomReservation;
use App\Models\TableReservation;
use App\Models\HomeBlock;
use App\Models\HomeBlockTranslation;
use Mail;

class AdminController extends Controller
{

    private $imagePath = 'assets/img/apartments/';
    private $imagePathStory = 'assets/img/story/';
    private $imagePathRestaurant = 'assets/img/restaurant/';
    private $imagePathServices = 'assets/img/services/';
    private $imagePathProgramroom = 'assets/img/programroom/';
    private $imagePathWellness = 'assets/img/wellness/';
    private $imagePathWatermill= 'assets/img/watermill/';
    private $uploadedImagesPath = 'uploads/images/';
    private $bannerPath = 'uploads/banner/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('admin.dashboard')
            ->with('pageTitle', trans('admin.dashboard'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSettings()
    {
        return view('admin.settings')
            ->with('pageTitle', trans('admin.settings'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeSettings(Request $request)
    {
        $post = $request->input();

        unset($post['_token']); // remove csrf token

        foreach ($post as $key => $row) {
            DB::table('settings')
                ->where('key', $key)
                ->update(['value' => $row]);
        }

        return redirect()->route('admin.settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEvents()
    {
        $events = Event::all();

        return view('admin.events')
            ->with('events', $events)
            ->with('pageTitle', trans('admin.events'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreateEvents()
    {
        return view('admin.events-create')
            ->with('pageTitle', trans('admin.events-new'));
    }

    public function getEventsEdit($id)
    {
        $event = Event::find($id);

        $hasImage = file_exists('uploads/events/' . $event->id . '.jpg');

        return view('admin.events-edit')
            ->with('event', $event)
            ->with('hasImage', $hasImage)
            ->with('pageTitle', trans('admin.events-edit'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeEvent(Request $request)
    {
        $event = Event::create([
            'datetime' => $request->get('datetime'),
            'days' => $request->get('days')
        ]);

        $eventTrHU = new EventTranslation();
        $eventTrHU->language = 'hu';
        $eventTrHU->title = $request->get('title_hu');
        $eventTrHU->text = $request->get('text_hu');
        $event->translation()->save($eventTrHU);

        $eventTrEN = new EventTranslation();
        $eventTrEN->language = 'en';
        $eventTrEN->title = $request->get('title_en');
        $eventTrEN->text = $request->get('text_en');
        $event->translation()->save($eventTrEN);

        $eventTrDE = new EventTranslation();
        $eventTrDE->language = 'de';
        $eventTrDE->title = $request->get('title_de');
        $eventTrDE->text = $request->get('text_de');
        $event->translation()->save($eventTrDE);

        if ($request->hasFile('file')) {
            $image = new Image();

            $fileName = $event->id . '.jpg';

            $ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

            /* if its not an image return false */
            if (!in_array($ext, array('gif', 'jpg', 'png', 'jpeg'))) return false;

            $src = $_FILES['file']['tmp_name'];

            /* read the source image */
            if ($ext == 'gif')
                $resource = imagecreatefromgif($src);
            else if ($ext == 'png')
                $resource = imagecreatefrompng($src);
            else if ($ext == 'jpg' || $ext == 'jpeg')
                $resource = imagecreatefromjpeg($src);

            $width = imagesx($resource);
            $height = imagesy($resource);
            /* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */

            $desired_width = 300;

            $desired_height = floor($height * ($desired_width / $width));

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

            /* copy source image at a resized size */
            imagecopyresized($virtual_image, $resource, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

            /* create the physical thumbnail image to its destination */
            /* Use correct function based on the desired image type from $dest thumbnail source */
            $ext = 'jpg';//strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            /* if dest is not an image type, default to jpg */
            //if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';

            $dest = 'uploads/events/' . $fileName;

            /*if ($ext == 'gif')
                imagegif($virtual_image,$dest);
            else if ($ext == 'png')
                imagepng($virtual_image,$dest,1);
            else if ($ext == 'jpg' || $ext == 'jpeg')*/
            imagejpeg($virtual_image, $dest);
        }


        return redirect()->route('admin.events');
    }


    public function updateEvent(Request $request, $id)
    {
        $event = Event::find($id);

        $event->datetime = $request->get('datetime');
        $event->days = $request->get('days');
        $event->save();

        // hu
        $tr = $event->translation('hu')->update([
            'title' => $request->get('title_hu'),
            'text' => $request->get('text_hu'),
        ]);

        // en
        $tr = $event->translation('en')->update([
            'title' => $request->get('title_en'),
            'text' => $request->get('text_en'),
        ]);

        // de
        $tr = $event->translation('de')->update([
            'title' => $request->get('title_de'),
            'text' => $request->get('text_de'),
        ]);

        if ($request->hasFile('file')) {
            $image = new Image();

            $fileName = $event->id . '.jpg';

            $ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

            /* if its not an image return false */
            if (!in_array($ext, array('gif', 'jpg', 'png', 'jpeg'))) return false;

            $src = $_FILES['file']['tmp_name'];

            /* read the source image */
            if ($ext == 'gif')
                $resource = imagecreatefromgif($src);
            else if ($ext == 'png')
                $resource = imagecreatefrompng($src);
            else if ($ext == 'jpg' || $ext == 'jpeg')
                $resource = imagecreatefromjpeg($src);

            $width = imagesx($resource);
            $height = imagesy($resource);
            /* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */

            $desired_width = 300;

            $desired_height = floor($height * ($desired_width / $width));

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

            /* copy source image at a resized size */
            imagecopyresized($virtual_image, $resource, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

            /* create the physical thumbnail image to its destination */
            /* Use correct function based on the desired image type from $dest thumbnail source */
            $ext = 'jpg';//strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            /* if dest is not an image type, default to jpg */
            //if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';

            $dest = 'uploads/events/' . $fileName;

            /*if ($ext == 'gif')
                imagegif($virtual_image,$dest);
            else if ($ext == 'png')
                imagepng($virtual_image,$dest,1);
            else if ($ext == 'jpg' || $ext == 'jpeg')*/
            imagejpeg($virtual_image, $dest);
        }

        return redirect()->route('admin.events');
    }

    public function deleteEvent($id)
    {
        $event = Event::find($id);
        $event->delete();

        return redirect()->route('admin.events');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHomeblock($id)
    {
        $homeblock = HomeBlock::find($id);

        return view('admin.homeblock-edit')
            ->with('block', $homeblock)
            ->with('pageTitle', trans('admin.homeblock-edit'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateHomeblock($id, Request $request)
    {
        $block = HomeBlock::find($id);

        // hu
        $tr = $block->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'description'=> $request->get('description_hu'),
            'content'=> $request->get('content_hu')
        ]);

        // en
        $tr = $block->translation('en')->update([
            'name' => $request->get('name_en'),
            'description'=> $request->get('description_en'),
            'content'=> $request->get('content_en')
        ]);

        // de
        $tr = $block->translation('de')->update([
            'name' => $request->get('name_de'),
            'description'=> $request->get('description_de'),
            'content'=> $request->get('content_de')
        ]);

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getContact()
    {
        return view('admin.editcontact')
            ->with('pageTitle', trans('admin.contact-data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateContact(Request $request)
    {
        $post = $request->input();

        unset($post['_token']); // remove csrf token

        foreach ($post as $key => $row) {
            DB::table('settings')
                ->where('key', $key)
                ->update(['value' => $row]);
        }

        return redirect()->route('admin.contacts');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRestaurant()
    {
        $data = Page::find(2);

        return view('admin.pages.restaurant')
            ->with('data', $data)
            ->with('pageTitle', trans('admin.restaurant'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateRestaurant(Request $request)
    {

        $page = Page::find(2);

        // hu
        $tr = $page->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
            'text2'=> $request->get('text2_hu'),
            'longtext'=> $request->get('longtext_hu')
        ]);

        // en
        $tr = $page->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
            'text2'=> $request->get('text2_en'),
            'longtext'=> $request->get('longtext_en')
        ]);

        // de
        $tr = $page->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
            'text2'=> $request->get('text2_de'),
            'longtext'=> $request->get('longtext_de')
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePathRestaurant, 'a.jpg');
        }

        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePathRestaurant, 'b.jpg');
        }

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStory()
    {
        $data = Page::find(1);

        return view('admin.pages.story')
            ->with('data', $data)
            ->with('pageTitle', trans('admin.story'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStory(Request $request)
    {

        $page = Page::find(1);

        // hu
        $tr = $page->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
            'text2'=> $request->get('text2_hu')
        ]);

        // en
        $tr = $page->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
            'text2'=> $request->get('text2_en')
        ]);

        // de
        $tr = $page->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
            'text2'=> $request->get('text2_de')
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePathStory, 'a.jpg');
        }

        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePathStory, 'b.jpg');
        }

        return back();
    }

    // TODO updaet


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getServices()
    {
        $data = Page::find(3);

        return view('admin.pages.services')
            ->with('data', $data)
            ->with('pageTitle', trans('admin.services'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateServices(Request $request)
    {

        $page = Page::find(3);

        // hu
        $tr = $page->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
        ]);

        // en
        $tr = $page->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
        ]);

        // de
        $tr = $page->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePathServices, 'a.jpg');
        }
        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePathServices, 'b.jpg');
        }

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWellness()
    {
        $data = Page::find(5);

        return view('admin.pages.wellness')
            ->with('data', $data)
            ->with('pageTitle', trans('admin.wellness'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateWellness(Request $request)
    {

        $page = Page::find(5);

        // hu
        $tr = $page->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
            'text2'=> $request->get('text2_hu'),
        ]);

        // en
        $tr = $page->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
            'text2'=> $request->get('text2_en'),
        ]);

        // de
        $tr = $page->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
            'text2'=> $request->get('text2_de'),
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePathWellness, 'a.jpg');
        }
        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePathWellness, 'b.jpg');
        }

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProgramroom()
    {
        $data = Page::find(6);

        return view('admin.pages.programroom')
            ->with('data', $data)
            ->with('pageTitle', trans('admin.programroom'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProgramroom(Request $request)
    {

        $page = Page::find(6);

        // hu
        $tr = $page->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
            'text2'=> $request->get('text2_hu'),
        ]);

        // en
        $tr = $page->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
            'text2'=> $request->get('text2_en'),
        ]);

        // de
        $tr = $page->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
            'text2'=> $request->get('text2_de'),
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePathProgramroom, 'a.jpg');
        }
        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePathProgramroom, 'b.jpg');
        }

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWatermill()
    {
        $data = Page::find(7);

        return view('admin.pages.watermill')
            ->with('data', $data)
            ->with('pageTitle', trans('admin.watermill'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateWatermill(Request $request)
    {

        $page = Page::find(7);

        // hu
        $tr = $page->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
            'text2'=> $request->get('text2_hu'),
        ]);

        // en
        $tr = $page->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
            'text2'=> $request->get('text2_en'),
        ]);

        // de
        $tr = $page->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
            'text2'=> $request->get('text2_de'),
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePathWatermill, 'a.jpg');
        }
        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePathWatermill, 'b.jpg');
        }

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getApartment($id)
    {
        $apartment = Apartment::find($id);

        return view('admin.apartment')
            ->with('apartment', $apartment)
            ->with('pageTitle', trans('admin.apartment') . ' - ' . $apartment->getTranslation()->name);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateApartment(Request $request, $id)
    {

        $apartment = Apartment::find($id);

        // hu
        $tr = $apartment->translation('hu')->update([
            'name' => $request->get('name_hu'),
            'text1'=> $request->get('text1_hu'),
            'text2'=> $request->get('text2_hu')
        ]);

        // en
        $tr = $apartment->translation('en')->update([
            'name' => $request->get('name_en'),
            'text1'=> $request->get('text1_en'),
            'text2'=> $request->get('text2_en')
        ]);

        // de
        $tr = $apartment->translation('de')->update([
            'name' => $request->get('name_de'),
            'text1'=> $request->get('text1_de'),
            'text2'=> $request->get('text2_de')
        ]);

        if ($request->hasFile('file-a')) {
            $request->file('file-a')->move($this->imagePath . $apartment->id, 'a.jpg');
        }

        if ($request->hasFile('file-b')) {
            $request->file('file-b')->move($this->imagePath . $apartment->id, 'b.jpg');
        }

        return back();
    }

    public function getRoomReservationIndex()
    {
        $reservations = RoomReservation::all();

        return view('admin.reservation.room.index')
            ->with('reservations', $reservations);
    }

    public function getRoomReservationCreate(Request $request, $id = 1)
    {
        $apartments = Apartment::all();
        $disabledDates = [];

        $reservations = RoomReservation::where('apartment_id', $id)->get();
        foreach ($reservations as $r) {
            $start_date = date('Y-m-d H:00:00', strtotime($r->time_from));
            $start_date = new \DateTime($start_date);
            $end_date = date('Y-m-d H:00:00', strtotime($r->time_to));
            $end_date = new \DateTime($end_date);
            $end_date->add(new \DateInterval('P1D'));

            $period = new \DatePeriod(
            	$start_date, // 1st PARAM: start date
            	new \DateInterval('P1D'), // 2nd PARAM: interval (1 day interval in this case)
            	$end_date // 3rd PARAM: end date
            	 // 4th PARAM (optional): self-explanatory
            );

            foreach($period as $date) {
            	$disabledDates[] = $date->format('Y-m-d H:i'); // Display the dates in yyyy-mm-dd format
            }
        }

        return view('admin.reservation.room.create')
            ->with('apartments', $apartments)
            ->with('currentApartment', $request->id)
            ->with('disabledDates', $disabledDates)
            ->with('pageTitle', trans('admin.new-reservation'));
    }

    // Room res. store
    public function storeRoomReservation(Request $request)
    {
        $res = RoomReservation::create([
            'apartment_id' => $request->get('apartment'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'time_from' => $request->get('time_from'),
            'time_to' => $request->get('time_to'),
            'coupon' => $request->get('coupon'),
            'adult_count' => $request->get('adults'),
            'child_count' => $request->get('children'),
            'comment' => $request->get('comment'),
        ]);

        // send confirmation email to person making reservation
        Mail::send('emails.res_room', [], function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to($res->email, $res->name)->subject(trans('strings.email.confirmation'));
        });

        // send mail to admin
        Mail::send('emails.res_room', [], function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to('contact@malomudvar.com', 'Admin')->subject(trans('strings.email.confirmation'));
        });

        return redirect()->route('admin.reservations.room.index');
    }

    public function getRoomReservationEdit($id)
    {
        $apartments = Apartment::all();
        $res = RoomReservation::find($id);

        $reservations = RoomReservation::where('id','<>',$res->id)->get();
        $disabledDates = [];

        foreach ($reservations as $r) {
            $start_date = date('Y-m-d H:00:00', strtotime($r->time_from));
            $start_date = new \DateTime($start_date);
            $end_date = date('Y-m-d H:00:00', strtotime($r->time_to));
            $end_date = new \DateTime($end_date);
            $end_date->add(new \DateInterval('P1D'));

            $period = new \DatePeriod(
            	$start_date, // 1st PARAM: start date
            	new \DateInterval('P1D'), // 2nd PARAM: interval (1 day interval in this case)
            	$end_date // 3rd PARAM: end date
            	 // 4th PARAM (optional): self-explanatory
            );

            foreach($period as $date) {
            	$disabledDates[] = $date->format('Y-m-d H:i'); // Display the dates in yyyy-mm-dd format
            }
        }

        return view('admin.reservation.room.edit')
            ->with('disabledDates', $disabledDates)
            ->with('apartments', $apartments)
            ->with('reservation', $res)
            ->with('pageTitle', trans('admin.reservation-edit'));
    }

    // Room res. store
    public function updateRoomReservation(Request $request, $id)
    {
        $from  = Carbon::createFromFormat('Y-m-d', $request->get('time_from'));
        $res = RoomReservation::find($id);

        $res->apartment_id = $request->get('apartment');
        $res->name = $request->get('name');
        $res->address = $request->get('address');
        $res->phone = $request->get('phone');
        $res->email = $request->get('email');
        $res->time_from = $request->get('time_from');
        $res->time_to = $request->get('time_to');
        $res->coupon = $request->get('coupon');
        $res->adult_count = $request->get('adults');
        $res->child_count = $request->get('children');
        $res->comment = $request->get('comment');

        $res->save();

        return redirect()->route('admin.reservations.room.index');
    }

    // Room res. store
    public function deleteRoomReservation($id)
    {
        $res = RoomReservation::find($id);
        $res->delete();

        return redirect()->route('admin.reservations.room.index');
    }

    // table reservations
    public function getTableReservationIndex()
    {
        $times = ['10:00', '12:00', '14:00', '16:00', '18:00', '20:00']; // TODO dynamic
        $startDay = Carbon::today()->format('Y-m-d');
        $endDay = Carbon::today()->addDays(27)->format('Y-m-d');
        $reservations = [];
        foreach ($times as $time) {
            $rawReservations = \DB::select("SELECT sum(guest_count) AS 'count', `time` FROM tablereservations where `time` >= '{$startDay}' AND `time` < '{$endDay}' AND `time` LIKE '%{$time}%' GROUP BY `time`");
            foreach($rawReservations as $res) { // reformat the array to make it more js friendly
                $reservations[$time][$res->time] = $res->count;
            }
            if (!$rawReservations) // handle empty times
                $reservations[$time] = null;
        }

        $days = [];
        for ($i = 0; $i<28; $i++) {
            $days[] = Carbon::today()->addDays($i)->format('Y-m-d');
        }

        $limit = 44; // TODO dynamic

        $disabledTableDays = \DB::select("SELECT GROUP_CONCAT(CONCAT('\"', date(`time`), '\"') SEPARATOR ', ') AS 'days' from tablereservations where isAllDay = 1")[0]->days;

        return view('admin.reservation.table.index')
            ->with('days', $days)
            ->with('limit', $limit)
            ->with('times', $times)
            ->with('disabledTableDays', $disabledTableDays)
            ->with('reservations', $reservations);
    }

    // table reservations
    public function getAjaxTableReservations(Request $request)
    {
        if (! $request->ajax()) die("Request not supported."); // only ajax allowed

        $datetime = $request->get('datetime');
        if (! $datetime) die("Bad request.");

        $reservations = [];
        $reservations = TableReservation::where('time', '=', $datetime)->get();

        echo json_encode($reservations);
    }

    public function getTableReservationSettings() {
        $unavailables = DB::table('unavailabledays')->get();

        foreach ($unavailables as $key => $row) {
            $unavailables[$key] = $row->unavailable;
        }

        return view('admin.reservation.table.settings')->with('unavailables', $unavailables);
    }

    public function postAjaxSetUnavailableday(Request $request) {
        if (! $request->ajax()) die("Request not supported."); // only ajax allowed

        $dayOfWeek = $request->get('dayOfWeek');
        $state = $request->get('value') === 'true' ? false : true; // false value means the day is unavailable

        DB::table('unavailabledays')->where('daynum', $dayOfWeek)->update(['unavailable' => $state]);
        dd($state);
    }

    public function postAjaxSetForcedday(Request $request) {
        if (! $request->ajax()) die("Request not supported."); // only ajax allowed

        try {
            DB::table('forceddays')->insert(['date' => Carbon::parse($request->get('date'))->toDateString()]);
        } catch (\Illuminate\Database\QueryException $e) {
            // todo?
        }

        echo json_encode(["forceddays" => $this->loadForceddays()]);
    }

    public function postAjaxRemoveForcedday(Request $request) {
        if (! $request->ajax()) die("Request not supported."); // only ajax allowed

        try {
            DB::table('forceddays')->where('date', $request->get('date'))->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            // todo?
        }

        echo json_encode(["forceddays" => $this->loadForceddays()]);
    }

    public function getAjaxGetForcedday(Request $request) {
        if (! $request->ajax()) die("Request not supported."); // only ajax allowed

        echo json_encode(["forceddays" => $this->loadForceddays()]);
    }

    private function loadForceddays() {
        return DB::table('forceddays')->lists('date');
    }

    public function getTableReservationCreate()
    {
        $limit = 44; // max reservations for one time limiter

        $times = ['10:00', '12:00', '14:00', '16:00', '18:00', '20:00']; // TODO dynamic

        $startDay = Carbon::today()->format('Y-m-d');
        // $endDay = Carbon::today()->addDays(27)->format('Y-m-d'); disabled for admin

        $disabledTimes = [];
        $disabledTimes = \DB::select(
            "SELECT sum(guest_count) AS 'count', `time`
            FROM tablereservations
            where `time` >= '{$startDay}'
            GROUP BY `time`
            HAVING sum(guest_count) >= {$limit}"
        );

        return view('admin.reservation.table.create')
            ->with('limit', $limit)
            ->with('times', $times)
            ->with('disabledTimes', $disabledTimes)
            ->with('pageTitle', trans('admin.new-reservation'));
    }

    // Table res. store
    public function storeTableReservation(Request $request)
    {
        $datetime = $request->get('date') . ' ' . $request->get('time');

        // TODO count validation
        $res = TableReservation::create([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'time' => $datetime,
            'guest_count' => $request->get('guests'),
            'comment' => $request->get('comment'),
        ]);

        if ($request->get('allday')) {
            $res->isAllDay = 1;
            $res->save();
        }

        // send confirmation email to person making reservation
        Mail::send('emails.res_table', [], function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to($res->email, $res->name)->subject(trans('strings.email.confirmation'));
        });

        // send mail to admin
        Mail::send('emails.res_table', [], function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to('contact@malomudvar.com', 'Admin')->subject(trans('strings.email.confirmation'));
        });

        return redirect()->route('admin.reservations.table.index');
    }

    public function getTableReservationEdit($id)
    {
        $res = TableReservation::find($id);

        $limit = 3; // max reservations for one time limiter TODO change test-number to 44

        $times = ['10:00', '12:00', '14:00', '16:00', '18:00', '20:00']; // TODO dynamic

        $startDay = Carbon::today()->format('Y-m-d');
        // $endDay = Carbon::today()->addDays(27)->format('Y-m-d'); disabled for admin

        $disabledTimes = [];
        $disabledTimes = \DB::select(
            "SELECT sum(guest_count) AS 'count', `time`
            FROM tablereservations
            where `time` >= '{$startDay}'
            GROUP BY `time`
            HAVING sum(guest_count) >= {$limit}"
        );


        return view('admin.reservation.table.edit')
            ->with('reservation', $res)
            ->with('reservation_date', explode(' ',$res->time)[0])
            ->with('reservation_time', explode(' ',$res->time)[1])
            ->with('limit', $limit)
            ->with('times', $times)
            ->with('disabledTimes', $disabledTimes)
            ->with('pageTitle', trans('admin.reservation-edit'));
    }

    public function updateTableReservation(Request $request, $id)
    {
        $res = TableReservation::find($id);

        $datetime = $request->get('date') . ' ' . $request->get('time');

        $res->name = $request->get('name');
        $res->address = $request->get('address');
        $res->phone = $request->get('phone');
        $res->email = $request->get('email');
        $res->time = $datetime;
        $res->guest_count = $request->get('guests');
        $res->comment = $request->get('comment');

        $res->save();

        return redirect()->route('admin.reservations.table.index');
    }

    public function deleteTableReservation($id)
    {
        $res = TableReservation::find($id);
        $res->delete();

        return redirect()->route('admin.reservations.table.index');
    }

    public function getGalleries()
    {
        $galleries = Gallery::all();

        return view('admin.galleries.index')
            ->with('galleries', $galleries)
            ->with('pageTitle', trans('admin.galleries'));
    }

    public function getGalleriesCreate()
    {
        return view('admin.galleries.create')
            ->with('pageTitle', trans('admin.gallery-new'));
    }

    public function getGallery($id)
    {
        $gallery = Gallery::find($id);

        return view('admin.galleries.show')
            ->with('gallery', $gallery)
            ->with('pageTitle', trans('admin.gallery-admin'));
    }

    public function getGalleriesEdit($id)
    {
        $gallery = Gallery::find($id);

        return view('admin.galleries.edit')
            ->with('gallery', $gallery)
            ->with('pageTitle', trans('admin.gallery-edit'));
    }

    public function storeGallery(Request $request)
    {
        $gallery = Gallery::create();

        $galleryTrHU = new GalleryTranslation();
        $galleryTrHU->language = 'hu';
        $galleryTrHU->name = $request->get('name_hu');
        $gallery->translation()->save($galleryTrHU);

        $galleryTrEN = new GalleryTranslation();
        $galleryTrEN->language = 'en';
        $galleryTrEN->name = $request->get('name_en');
        $gallery->translation()->save($galleryTrEN);

        $galleryTrDE = new GalleryTranslation();
        $galleryTrDE->language = 'de';
        $galleryTrDE->name = $request->get('name_de');
        $gallery->translation()->save($galleryTrDE);

        return redirect()->route('admin.galleries.show', [$gallery->id]);
    }

    public function updateGallery(Request $request, $id)
    {
        $gallery = Gallery::find($id);

        // hu
        $tr = $gallery->translation('hu')->update([
            'name' => $request->get('name_hu'),
        ]);

        // en
        $tr = $gallery->translation('en')->update([
            'name' => $request->get('name_en'),
        ]);

        // de
        $tr = $gallery->translation('de')->update([
            'name' => $request->get('name_de'),
        ]);

        return redirect()->route('admin.galleries');
    }

    public function storeImage(Request $request)
    {
        $ds = DIRECTORY_SEPARATOR;

        $gallery = Gallery::find($request->get('gallery_id'));

        $hadNotValidFile = false;

        foreach ($request->file('file') as $key => $file) {
            if ($file->isValid()) {
                $image = new Image();

                $newFileName = date('YmdHi') . ' - ' . $file->getClientOriginalName();

                $file->move($this->uploadedImagesPath, $newFileName);

                $newFile = $this->uploadedImagesPath . $newFileName;

                if (is_readable($newFile)) { // new file created
                    $thumbFile = $this->uploadedImagesPath . 'thumbnails' . $ds . $newFileName;
                    $thumbnail = $this->create_thumb($newFile, $thumbFile, 600);
                    if (is_readable($thumbnail['dest'])) { // thumbnail file created
                        $image->gallery_id = $gallery->id;
                        $image->filename = $newFileName;
                        $image->save();
                    } else { // delete the new file if thumbnail creation failed
                        unlink($newFile);
                        return redirect('admin/galleries/'.$gallery->id.'/edit')
                            ->withErrors(array('upload' => trans('forms.errors.thumbnail'))) // TODO LANG
                            ->withInput();
                    }
                } else {
                    return redirect('admin/galleries/'.$gallery->id.'/edit')
                        ->withErrors(array('upload' => trans('forms.errors.upload'))) // TODO LANG
                        ->withInput();
                }
            } else {
                $hadNotValidFile = true;
            }
        }

        if ($hadNotValidFile)
            return redirect('admin/galleries/'.$gallery->id)
                    ->withErrors(array('upload' =>  trans('forms.errors.upload'))) // TODO LANG
                    ->withInput();

        return back();
    }

    /**
     * Create thumbnail for an image.
     *
     * @param $src string The source file.
     * @param $dest string The destination file.
     * @param null $desired_width integer The width of the thumbnail.
     * @param null $desired_height integer The height of the thumbnail.
     * @return array|bool
     */
    function create_thumb($src, $dest, $desired_width = null, $desired_height = null)
    {
        /*If no dimension for thumbnail given, return false */
        if (!$desired_height && !$desired_width) return false;
        $ext = strtolower(pathinfo($src, PATHINFO_EXTENSION));
        /* if its not an image return false */
        if (!in_array($ext,array('gif','jpg','png','jpeg'))) return false;

        /* read the source image */
        if ($ext == 'gif')
            $resource = imagecreatefromgif($src);
        else if ($ext == 'png')
            $resource = imagecreatefrompng($src);
        else if ($ext == 'jpg' || $ext == 'jpeg')
            $resource = imagecreatefromjpeg($src);

        $width  = imagesx($resource);
        $height = imagesy($resource);
        /* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */
        if(!$desired_height) $desired_height = floor($height*($desired_width/$width));
        if(!$desired_width)  $desired_width  = floor($width*($desired_height/$height));

        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width,$desired_height);

        /* copy source image at a resized size */
        imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);

        /* create the physical thumbnail image to its destination */
        /* Use correct function based on the desired image type from $dest thumbnail source */
        $ext = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
        /* if dest is not an image type, default to jpg */
        if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';

        if ($ext == 'gif')
            imagegif($virtual_image,$dest);
        else if ($ext == 'png')
            imagepng($virtual_image,$dest,1);
        else if ($ext == 'jpg' || $ext == 'jpeg')
            imagejpeg($virtual_image,$dest,100);

        return array(
            'width'     => $width,
            'height'    => $height,
            'new_width' => $desired_width,
            'new_height'=> $desired_height,
            'dest'      => $dest
        );
    }

    /**
     * Add new images to the specified gallery
     *
     * @param $galleryId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setAlbumImage($galleryId, Request $request)
    {
        $gallery = Gallery::find($galleryId);
        $gallery->featured_image_id = $request->input('imageId');
        $gallery->save();

        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteGallery($id)
    {
        $gallery = Gallery::findOrFail($id);
        $images = $gallery->images()->get();

        foreach ($images as $image) {
            unlink($this->uploadedImagesPath . $image->filename);
            unlink($this->uploadedImagesPath . 'thumbnails/' . $image->filename);

            $image->delete();
        }

        if ($gallery->imageCount() == 0) $gallery->delete();

        return redirect()->route('admin.galleries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyImage($id)
    {
        $image = Image::find($id);
        unlink($this->uploadedImagesPath . $image->filename);
        unlink($this->uploadedImagesPath . 'thumbnails/' . $image->filename);


        $gallery = $image->gallery()->first();
        if ($gallery->featured_image_id === $image->id)
            $gallery->featured_image_id = null;


        $image->delete();
        $gallery->save();

        return back();
    }



    public function getFoodsCreate()
    {
        $foodcategories = Foodcategory::all();

        return view('admin.foods.create')
            ->with('pageTitle', trans('admin.foods-new'))
            ->with('foodcategories', $foodcategories);
    }

    public function storeFood(Request $request)
    {
        $food = new Food();

        $food->foodcategory_id = $request->get('foodcategory');
        $food->price = $request->get('price');
        $food->no_egg = $request->get('no_egg', 0);
        $food->no_sugar = $request->get('no_sugar', 0);
        $food->no_milk = $request->get('no_milk', 0);
        $food->no_wheat = $request->get('no_wheat', 0);

        $food->save();

        $foodTrHU = new FoodTranslation();
        $foodTrHU->language = 'hu';
        $foodTrHU->name = $request->get('name_hu');
        $food->translation()->save($foodTrHU);

        $foodTrEN = new FoodTranslation();
        $foodTrEN->language = 'en';
        $foodTrEN->name = $request->get('name_en');
        $food->translation()->save($foodTrEN);

        $foodTrDE = new FoodTranslation();
        $foodTrDE->language = 'de';
        $foodTrDE->name = $request->get('name_de');
        $food->translation()->save($foodTrDE);

        return redirect()->route('admin.food');
    }

    public function deleteFood($id)
    {
        $food = Food::find($id);
        $food->delete();

        return redirect()->route('admin.food');
    }

    public function updateFood(Request $request, $id)
    {
        $food = Food::find($id);

        $food->foodcategory_id = $request->get('foodcategory');
        $food->price = $request->get('price');
        $food->no_egg = $request->get('no_egg', 0);
        $food->no_sugar = $request->get('no_sugar', 0);
        $food->no_milk = $request->get('no_milk', 0);
        $food->no_wheat = $request->get('no_wheat', 0);

        $food->save();

        // hu
        $tr = $food->translation('hu')->update([
            'name' => $request->get('name_hu'),
        ]);

        // en
        $tr = $food->translation('en')->update([
            'name' => $request->get('name_en'),
        ]);

        // de
        $tr = $food->translation('de')->update([
            'name' => $request->get('name_de'),
        ]);

        return redirect()->route('admin.food');
    }

    public function getFoods()
    {
        $foods = Food::all();

        return view('admin.foods.index')
            ->with('foods', $foods)
            ->with('pageTitle', trans('admin.foods'));
    }

    public function getFood($id)
    {
        $food = Food::find($id);
        $foodcategories = Foodcategory::all();

        return view('admin.foods.edit')
            ->with('food', $food)
            ->with('foodcategories', $foodcategories)
            ->with('pageTitle', trans('admin.foods'));
    }

    public function getCategoryImages()
    {
        $foodcategories = FoodCategory::all();

        return view('admin.foods.catimages')
            ->with('foodcategories', $foodcategories)
            ->with('pageTitle', trans('admin.food-category-images'));
    }

    public function setCategoryImages(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $image = new Image();

            $fileName = $request->get('side').'.jpg';

            $ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

            /* if its not an image return false */
            if (!in_array($ext,array('gif','jpg','png','jpeg'))) return false;

            $src = $_FILES['file']['tmp_name'];

            /* read the source image */
            if ($ext == 'gif')
                $resource = imagecreatefromgif($src);
            else if ($ext == 'png')
                $resource = imagecreatefrompng($src);
            else if ($ext == 'jpg' || $ext == 'jpeg')
                $resource = imagecreatefromjpeg($src);

            $width  = imagesx($resource);
            $height = imagesy($resource);
            /* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */

            $desired_width = 300;

            $desired_height = floor($height*($desired_width/$width));

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width,$desired_height);

            /* copy source image at a resized size */
            imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);

            /* create the physical thumbnail image to its destination */
            /* Use correct function based on the desired image type from $dest thumbnail source */
            $ext = 'jpg';//strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            /* if dest is not an image type, default to jpg */
            //if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';

            $dest = 'uploads/foodcategories/' . $request->get('category') . '/' . $request->get('side') . '.jpg';

            /*if ($ext == 'gif')
                imagegif($virtual_image,$dest);
            else if ($ext == 'png')
                imagepng($virtual_image,$dest,1);
            else if ($ext == 'jpg' || $ext == 'jpeg')*/
                var_dump(imagejpeg($virtual_image,$dest,100));

        } else {
            return back()
                ->withErrors(array('upload' =>  trans('forms.errors.upload')))
                ->withInput();
        }

        return back();
    }

    public function getBanner()
    {
        return view('admin.banner')
            ->with('pageTitle', trans('admin.banner'));
    }

    public function setBanner(Request $request)
    {
        //$currentFile = DB::table('settings')->select('value')->where('key', '=', 'banner_file')->first();

        //if ($currentFile && ! is_writable($this->bannerPath . $currentFile))
        if (is_file($this->bannerPath . 'banner.jpg') && ! is_writable($this->bannerPath . 'banner.jpg'))
            die('Error! Current banner file is not writable!');

        if ($request->hasFile('file')) {
            $image = new Image();

            $fileName ='banner.jpg';

            $ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

            /* if its not an image return false */
            if (!in_array($ext,array('gif','jpg','png','jpeg'))) die("Bad file."); // TODO do it better if you want.

            $src = $_FILES['file']['tmp_name'];

            /* read the source image */
            if ($ext == 'gif')
                $resource = imagecreatefromgif($src);
            else if ($ext == 'png')
                $resource = imagecreatefrompng($src);
            else if ($ext == 'jpg' || $ext == 'jpeg')
                $resource = imagecreatefromjpeg($src);

            $width  = imagesx($resource);
            $height = imagesy($resource);
            /* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */

            $desired_width = 1200; // TODO this is the max, bla bla...

            if ($width <= $desired_width) {
                $desired_width = $width;
                $desired_height = $height;
            } else {
                $desired_height = floor($height*($desired_width/$width));
            }

            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width,$desired_height);

            /* copy source image at a resized size */
            imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);

            /* create the physical thumbnail image to its destination */
            /* Use correct function based on the desired image type from $dest thumbnail source */
            //$ext = 'jpg';//strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            /* if dest is not an image type, default to jpg */
            //if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';

            $dest = $this->bannerPath . 'banner.jpg';

            /*if ($ext == 'gif')
                imagegif($virtual_image,$dest);
            else if ($ext == 'png')
                imagepng($virtual_image,$dest,1);
            else if ($ext == 'jpg' || $ext == 'jpeg')*/
            imagejpeg($virtual_image,$dest,100);

            $currentFile = DB::table('settings')->select('value')->where('key', '=', 'banner_file')->first();

            return redirect()->back();
        } else {
            if (is_file($this->bannerPath . 'banner.jpg') && is_writable($this->bannerPath . 'banner.jpg'))
                unlink($this->bannerPath . 'banner.jpg');

            return redirect()->back();
        }
    }

    public function getVideos()
    {
        $videos = DB::table('videos')->get();

        return view('admin.videos')
            ->with('videos', $videos)
            ->with('pageTitle', trans('admin.videos'));
    }

    public function storeVideos(Request $request)
    {
        DB::table('videos')->insert(
            ['embedcode' => $request->get('embedcode'), 'title' => $request->get('title')]
        );

        return redirect()->back();
    }

    public function deleteVideos($id)
    {
        DB::table('videos')->where('id', $id)->delete();

        return redirect()->back();
    }
}
