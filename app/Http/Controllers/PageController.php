<?php

namespace App\Http\Controllers;

use App\Models\Foodcategory;
use App\Models\Gallery;
use App\Models\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\HomeBlock;
use App\Models\Page;
use App\Models\Apartment;
use App\Models\RoomReservation;
use App\Models\TableReservation;
use App\Models\Event;
use Illuminate\Support\Facades\DB;
use \LaravelLocalization;

class PageController extends Controller
{

    // Homepage
    public function getHomePage()
    {
        $blocks = HomeBlock::all();
        $reviews = Review::where('approved', '=', 1)
            ->get();
        $apartments = Apartment::all();

        return view('homepage')
            ->with('apartments', $apartments)
            ->with('blocks', $blocks)
            ->with('reviews', $reviews);
    }

    // Contact
    public function getContact()
    {
        return view('contact');
    }

    // Events
    public function getEvents()
    {
        $todayDatetime = Carbon::today()->startOfDay();
        $events = Event::where('datetime', '>', $todayDatetime)->orderBy('datetime')->get();

        $pastEvents = Event::where('datetime', '<', $todayDatetime)->orderBy('datetime', 'desc')->get();

        return view('events')
            ->with('pastEvents', $pastEvents)
            ->with('events', $events);
    }

    // Events
    public function getStory()
    {
        $data = Page::find(1);

        return view('pages.story')
            ->with('data', $data);
    }

    // Restaurant
    public function getRestaurant()
    {
        $data = Page::find(2);

        return view('pages.restaurant')
            ->with('data', $data);
    }

    // Menu
    public function getMenu()
    {
        $data = Page::find(2);
        $foodcategories = Foodcategory::all();

        return view('pages.menu')
            ->with('foodcategories', $foodcategories)
            ->with('data', $data);
    }

    // Services
    public function getServices()
    {
        $data = Page::find(3);

        return view('pages.services')
            ->with('data', $data);
    }

    // Wellness
    public function getWellness()
    {
        $data = Page::find(5);

        return view('pages.wellness')
            ->with('data', $data);
    }

    // Wellness
    public function getProgramroom()
    {
        $data = Page::find(6);

        return view('pages.programroom')
            ->with('data', $data);
    }

    // Wellness
    public function getWatermill()
    {
        $data = Page::find(7);

        return view('pages.watermill')
            ->with('data', $data);
    }

    // Gallery
    public function getGallery()
    {
        $galleries = Gallery::all();
        $videos = DB::table('videos')->get();

        return view('gallery')
            ->with('videos', $videos)
            ->with('galleries', $galleries);
    }

    // Show one image gallery
    public function showGallery($id)
    {
        $gallery = Gallery::findOrFail($id);

        return view('gallery-show')
            ->with('gallery', $gallery);
    }

    // Events
    public function getTableReservation()
    {
        $limit = 44; // max reservations for one time limiter

        $times = ['10:00', '12:00', '14:00', '16:00', '18:00', '20:00']; // TODO dynamic

        $startDay = Carbon::today()->format('Y-m-d');
        $endDay = Carbon::today()->addDays(27);

        // get the previously set unavailable list (key: day of week, value: is unavailable?
        $forcedDays = DB::table('forceddays')->whereBetween('date', [Carbon::today()->toDateString(), $endDay->toDateString()])->lists('date');
        $unavailables = DB::table('unavailabledays')->get();
        foreach ($unavailables as $key => $row) {
            $unavailables[$key] = $row->unavailable;
        };

        $unavailableDays = []; // container for unavailable days passed to view

        // walk through today and the end day and check if the day should be added to disabled days
        $aDay = Carbon::today();
        while ($aDay->lte($endDay)) {
            if ($unavailables[$aDay->dayOfWeek] == 1 && !in_array($aDay->toDateString(), $forcedDays)) {
                $unavailableDays[] = $aDay->toDateString();
            }
            $aDay->addDay();
        }

        $disabledTimes = \DB::select(
            "SELECT sum(guest_count) AS 'count', `time`
            FROM tablereservations
            where `time` >= '{$startDay}'
            AND `time` < '{$endDay->format('Y-m-d')}'
            GROUP BY `time`
            HAVING sum(guest_count) >= {$limit}"
        );

        return view('reservation.table')
            ->with('limit', $limit)
            ->with('times', $times)
            ->with('disabledTimes', $disabledTimes)
            ->with('unavailableDays', $unavailableDays);
    }

    // table reservations
    public function getAjaxTableReservationCount(Request $request)
    {
        if (! $request->ajax()) die("Request not supported."); // only ajax allowed

        $limit = 44; // TODO dynamic

        $datetime = $request->get('datetime');
        if (! $datetime) die("Bad request.");

        $count = DB::table('tablereservations')
            ->select(DB::raw('SUM(guest_count) as val'))
            ->groupBy('time')
            ->where('time', '=', $datetime)
            ->first();

        echo $count ? $limit-$count->val : $limit;
    }


    // Table res. store
    public function storeTableReservation(Request $request)
    {
        $time = $request->get('date') . ' ' . $request->get('time');

        // TODO count validation
        $res = TableReservation::create([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'time' => $time,
            'guest_count' => $request->get('guests'),
            'comment' => $request->get('comment'),
        ]);

        $emailData = [
            "name" => $request->get('name'),
            "email" => $request->get('email'),
            "phone" => $request->get('phone'),
            "reservationDate" => $time,
            "personCount" => $request->get('guests'),
        ];
        // send confirmation email to person making reservation
        Mail::send('emails.res_table', $emailData, function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to($res->email, $res->name)->subject(trans('strings.email.confirmation'));
        });

        // send mail to admin
        Mail::send('emails.res_table_admin', $emailData, function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to('contact@malomudvar.com', 'Admin')->subject(trans('strings.email.confirmation'));
        });

        $request->session()->flash('toast', trans('strings.messages.successful-reservation'));

        return redirect()->route('homepage')
            ->withCookie(cookie('MALOMUDVAR_USER_NAME', $res->name))
            ->withCookie(cookie('MALOMUDVAR_USER_ADDRESS', $res->address))
            ->withCookie(cookie('MALOMUDVAR_USER_PHONE', $res->phone))
            ->withCookie(cookie('MALOMUDVAR_USER_EMAIL', $res->email));
    }

    // Events
    public function getRoomReservation($id = null)
    {
        $apartments = Apartment::all();
        $reservations = RoomReservation::all();

        // Mandatory to set the default timezone to work with DateTime functions
        date_default_timezone_set('Europe/Budapest');

        // get disabled dates grouped by apartment id as array key
        $disabledDates = [];
        foreach ($reservations as $res) {
            $start_date = date('Y-m-d H:00:00', strtotime($res->time_from));
            $start_date = new \DateTime($start_date);
            $end_date = date('Y-m-d H:00:00', strtotime($res->time_to));
            $end_date = new \DateTime($end_date);
            $end_date->add(new \DateInterval('P1D'));

            $period = new \DatePeriod(
            	$start_date, // 1st PARAM: start date
            	new \DateInterval('P1D'), // 2nd PARAM: interval (1 day interval in this case)
            	$end_date // 3rd PARAM: end date
            	 // 4th PARAM (optional): self-explanatory
            );

            foreach($period as $date) {
            	$disabledDates[$res->apartment_id][] = $date->format('Y-m-d H:i'); // Display the dates in yyyy-mm-dd format
            }
        }

        return view('reservation.room')
            ->with('selectedApartment', $id)
            ->with('disabledDates', $disabledDates)
            ->with('apartments', $apartments);
    }

    // Room res. store
    public function storeRoomReservation(Request $request)
    {
        $res = RoomReservation::create([
            'apartment_id' => $request->get('apartment'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'time_from' => $request->get('time_from'),
            'time_to' => $request->get('time_to'),
            'coupon' => $request->get('coupon'),
            'adult_count' => $request->get('adults'),
            'child_count' => $request->get('children'),
            'comment' => $request->get('comment'),
        ]);

        $emailData = [
            "name" => $request->get('name'),
            "email" => $request->get('email'),
            "phone" => $request->get('phone'),
            'time_from' => $request->get('time_from'),
            'time_to' => $request->get('time_to'),
            "adultCount" => $request->get('adults'),
            "childCount" => $request->get('children'),
        ];
        // send confirmation email to person making reservation
        Mail::send('emails.res_room', $emailData, function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to($res->email, $res->name)->subject(trans('strings.email.confirmation'));
        });

        // send mail to admin
        Mail::send('emails.res_room_admin', $emailData, function ($m) use ($res) {
            $m->from(env('MAIL_DEFAULT_SENDER_ADDRESS' ,'no-reply@malomudvar.com'), config('project.site-title'));
            $m->to('contact@malomudvar.com', 'Admin')->subject(trans('strings.email.confirmation'));
        });

        $request->session()->flash('toast', trans('strings.messages.successful-reservation'));

        return redirect()->route('homepage')
            ->withCookie(cookie('MALOMUDVAR_USER_NAME', $res->name))
            ->withCookie(cookie('MALOMUDVAR_USER_ADDRESS', $res->address))
            ->withCookie(cookie('MALOMUDVAR_USER_PHONE', $res->phone))
            ->withCookie(cookie('MALOMUDVAR_USER_EMAIL', $res->email));
    }

    // Events
    public function getApartment($id)
    {
        $apartment = Apartment::find($id);
        $reservations = RoomReservation::where('apartment_id', '=', $id)->get();

        // Mandatory to set the default timezone to work with DateTime functions
        date_default_timezone_set('Europe/Budapest');

        $disabledDates = [];

        foreach ($reservations as $res) {
            $start_date = date('Y-m-d H:00:00', strtotime($res->time_from));
            $start_date = new \DateTime($start_date);
            $end_date = date('Y-m-d H:00:00', strtotime($res->time_to));
            $end_date = new \DateTime($end_date);
            $end_date->add(new \DateInterval('P1D'));

            $period = new \DatePeriod(
                $start_date, // 1st PARAM: start date
                new \DateInterval('P1D'), // 2nd PARAM: interval (1 day interval in this case)
                $end_date // 3rd PARAM: end date
                // 4th PARAM (optional): self-explanatory
            );

            foreach($period as $date) {
                $disabledDates[] = $date->format('Y-m-d H:i'); // Display the dates in yyyy-mm-dd format
            }
        }

        return view('apartment')
            ->with('disabledDatesForApartment', $disabledDates)
            ->with('apartment', $apartment);
    }

    // Contact
    public function postContact(Request $request)
    {
        // Create the message
        $message = \Swift_Message::newInstance()
            ->setSubject('Malom-udvar Contact Message')
            ->setFrom(array($request->get('email') => $request->get('name')))
            ->setTo(array('contact@malomudvar.com' => 'Malom Udvar'))
            ->setBody($request->get('message'));

        $transport = \Swift_SmtpTransport::newInstance('mail.3gteam.hu', 465, 'ssl')
            ->setUsername('contact@malomudvar.com')
            ->setPassword('QPv6xSMCdEa6B')
        ;

        $mailer = \Swift_Mailer::newInstance($transport);

        $result = $mailer->send($message);

        return back(); // TODO flash message maybe?
    }

    public function getBanner()
    {
        $bannerFile = public_path() . '/uploads/banner/banner.jpg'; // TODO if you want

        if (is_file($bannerFile) && is_readable($bannerFile)
            && (! isset($_COOKIE['MALOMUDVAR_AD_HASH']) || $_COOKIE['MALOMUDVAR_AD_HASH'] != md5_file($bannerFile) )) {
            setcookie('MALOMUDVAR_AD_HASH', md5_file($bannerFile), time()+60*60*24*30, '/');
            return view('banner');
        } else {
            header('HTTP/1.1 500 Internal Server Error');
            die;
        }
    }
}
