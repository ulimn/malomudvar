<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // TODO ADMIN AUTH
        $reviews = Review::all();
        return view('review.admin')
            ->with('reviews', $reviews)
            ->with('pageTitle', trans('admin.reviews.title-plural'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('review.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'rating' => 'required|numeric|min:1|max:5',
            'name' => 'required|min:5|max:255',
            'place' => 'required|min:2|max:255',
            'review' => 'required|min:5|max:65535'
        ]);

        $review = Review::create([
            'name' => $request->get('name'),
            'rating' => $request->get('rating'),
            'place' => $request->get('place'),
            'review' => $request->get('review'),
            'language' => LaravelLocalization::getCurrentLocale()
        ]);
        return redirect()->route('homepage');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);
        return view('review.show')
            ->with('review', $review)
            ->with('pageTitle', trans('admin.reviews.title-single'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function approve($id)
    {
        $review = Review::find($id);
        $review->approved = 1;
        $review->save();

        return redirect()->route('admin.reviews.show', [$id]);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function disapprove($id)
    {
        $review = Review::find($id);
        $review->approved = 0;
        $review->save();

        return redirect()->route('admin.reviews.show', [$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::find($id);
        $review->delete();

        return redirect()->route('admin.reviews');
    }
}
