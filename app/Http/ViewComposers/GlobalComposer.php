<?php

namespace App\Http\ViewComposers;

use App\Models\Setting;
use Illuminate\Contracts\View\View;

class GlobalComposer
{
    protected $settings;

    /**
     * Create a new onepage composer.
     *
     * @return void
     */
    public function __construct()
    {
        $settings = Setting::all();
        foreach($settings as $key => $row) {
            $newKey = $row['key'];
            $settings[$newKey] = $row['value'];
            unset($settings[$key]);
        }
        $this->settings = $settings;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('pageSettings', $this->settings);
    }
}