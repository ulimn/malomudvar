<?php

namespace App\Http\ViewComposers;

use App\Models\RoomReservation;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;

class ReservationComposer
{
    protected $days = [];
    protected $times = [];
    protected $limit;
    protected $reservations = [];
    protected $disabledTableDays = [];
    protected $unavailableTableDays = [];

    /**
     * Create a new onepage composer.
     *
     * @return void
     */
    public function __construct()
    {
        $times = ['10:00', '12:00', '14:00', '16:00', '18:00', '20:00']; // TODO dynamic

        $startDay = Carbon::today()->format('Y-m-d');
        $endDay = Carbon::today()->addDays(27)->format('Y-m-d');

        $reservations = [];
        foreach ($times as $time) {
            $rawReservations = \DB::select("SELECT sum(guest_count) AS 'count', `time` FROM tablereservations where `time` >= '{$startDay}' AND `time` < '{$endDay}' AND `time` LIKE '%{$time}%' GROUP BY `time`");
            foreach($rawReservations as $res) { // reformat the array to make it more js friendly
                $reservations[$time][$res->time] = $res->count;
            }
            if (!$rawReservations) // handle empty times
                $reservations[$time] = null;
        }

        $days = [];
        for ($i = 0; $i<28; $i++) {
            $days[] = Carbon::today()->addDays($i)->format('Y-m-d');
        }

        $limit = 44; // TODO dynamic

        $this->times = $times;
        $this->days = $days;
        $this->limit = $limit;
        $this->reservations = $reservations;
        $this->disabledTableDays = \DB::select("SELECT GROUP_CONCAT(CONCAT('\"', date(`time`), '\"') SEPARATOR ', ') AS 'days' from tablereservations where isAllDay = 1")[0]->days;

        $startDay = Carbon::today()->format('Y-m-d');
        $endDay = Carbon::today()->addDays(27);

        // get the previously set unavailable list (key: day of week, value: is unavailable?
        $forcedDays = \DB::table('forceddays')->whereBetween('date', [Carbon::today()->toDateString(), $endDay->toDateString()])->lists('date');
        $unavailables = \DB::table('unavailabledays')->get();
        foreach ($unavailables as $key => $row) {
            $unavailables[$key] = $row->unavailable;
        };

        // walk through today and the end day and check if the day should be added to disabled days
        $aDay = Carbon::today();
        while ($aDay->lte($endDay)) {
            if ($unavailables[$aDay->dayOfWeek] == 1 && !in_array($aDay->toDateString(), $forcedDays)) {
                $this->unavailableTableDays[] = '"'.$aDay->toDateString().'"';
            }
            $aDay->addDay();
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('table_picker_days', $this->days)
            ->with('table_picker_limit', $this->limit)
            ->with('table_picker_times', $this->times)
            ->with('table_picker_reservations', $this->reservations)
            ->with('table_picker_disabled_days', $this->disabledTableDays)
            ->with('table_picker_unavailable_days', implode(',',$this->unavailableTableDays));
    }
}
