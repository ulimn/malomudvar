<?php

namespace App\Http\ViewComposers;

use App\Models\HomeBlock;
use Illuminate\Contracts\View\View;

class OnepageComposer
{
    protected $homeblocks;

    /**
     * Create a new onepage composer.
     *
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->homeblocks = HomeBlock::all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('blocks', $this->homeblocks);
    }
}