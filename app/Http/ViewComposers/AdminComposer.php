<?php

namespace App\Http\ViewComposers;

use App\Models\Apartment;
use App\Models\HomeBlock;
use App\Models\Page;
use Illuminate\Contracts\View\View;

class AdminComposer
{
    protected $apartments;
    protected $homeblocks;
    protected $pages;

    /**
     * Create a new admin composer.
     *
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->apartments = Apartment::all();
        $this->homeblocks = HomeBlock::all();
        $this->pages = Page::all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('apartments', $this->apartments)
            ->with('homeblocks', $this->homeblocks)
            ->with('pages', $this->pages);
    }
}
