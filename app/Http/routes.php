<?php
/*

|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
],
function()
{
    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', [
        'as' => 'postLogin',
        'uses' => 'Auth\AuthController@postLogin'
    ]);
    Route::get('auth/logout', 'Auth\AuthController@getLogout');


    // Homepage
    Route::get('/', [
        'as' => 'homepage',
        'uses' => 'PageController@getHomePage'
    ]);

    // Banner load
    Route::get('load-banner', [
        'as' => 'load-banner',
        'uses' => 'PageController@getBanner'
    ]);

    // Contact page
    Route::get('contact', [
        'as' => 'contact',
        'uses' => 'PageController@getContact'
    ]);
    // POST contact message
    Route::post('contact', [
        'as' => 'contact',
        'uses' => 'PageController@postContact'
    ]);

    // Events page
    Route::get('events', [
        'as' => 'events',
        'uses' => 'PageController@getEvents'
    ]);

    // Our Story page
    Route::get('story', [
        'as' => 'story',
        'uses' => 'PageController@getStory'
    ]);

    // Restaurant page
    Route::get('restaurant', [
        'as' => 'restaurant',
        'uses' => 'PageController@getRestaurant'
    ]);

    // Menu page
    Route::get('menu', [
        'as' => 'menu',
        'uses' => 'PageController@getMenu'
    ]);

    // services page
    Route::get('services', [
        'as' => 'services',
        'uses' => 'PageController@getServices'
    ]);

    // Events page
    Route::get('gallery', [
        'as' => 'gallery',
        'uses' => 'PageController@getGallery'
    ]);

    // Events page
    Route::get('gallery/{id}', [
        'as' => 'gallery.show',
        'uses' => 'PageController@showGallery'
    ]);

    // wellness
    Route::get('wellness', [
        'as' => 'wellness',
        'uses' => 'PageController@getWellness'
    ]);

    // rendezvényterem
    Route::get('programroom', [
        'as' => 'programroom',
        'uses' => 'PageController@getProgramroom'
    ]);

    // Vízimalom
    Route::get('watermill', [
        'as' => 'watermill',
        'uses' => 'PageController@getWatermill'
    ]);

    // Table reservation page
    Route::get('reservation/table', [
        'as' => 'reservation.table',
        'uses' => 'PageController@getTableReservation'
    ]);
    Route::post('reservations/table/ajax-get-count', [
        'as' => 'reservation.table.ajax-get-count',
        'uses' => 'PageController@getAjaxTableReservationCount'
    ]);
    // Store table reservation
    Route::post('reservation/table', [
        'as' => 'reservation.table.store',
        'uses' => 'PageController@storeTableReservation'
    ]);

    // Room reservation page
    Route::get('reservation/room/{id?}', [
        'as' => 'reservation.room',
        'uses' => 'PageController@getRoomReservation'
    ]);
    // Store table reservation
    Route::post('reservation/room', [
        'as' => 'reservation.room.store',
        'uses' => 'PageController@storeRoomReservation'
    ]);

    // Apartment page
    Route::get('apartments/{id}', [
        'as' => 'apartments',
        'uses' => 'PageController@getApartment'
    ]);

    // reviews
    Route::get('review/create', [
        'as' => 'review.create',
        'uses' => 'ReviewController@create']
    );
    Route::get('admin/reviews', [
        'as' => 'admin.reviews',
        'uses' => 'ReviewController@index'
    ]);
    Route::get('admin/review/{id}', [
        'as' => 'admin.reviews.show',
        'uses' => 'ReviewController@show'
    ]);
    Route::get('admin/review/{id}/approve', [
        'as' => 'admin.review.approve',
        'uses' => 'ReviewController@approve'
    ]);
    Route::get('admin/review/{id}/disapprove', [
        'as' => 'admin.review.disapprove',
        'uses' => 'ReviewController@disapprove'
    ]);
    Route::delete('admin/review/{id}', [
        'as' => 'admin.review.delete',
        'uses' => 'ReviewController@destroy'
    ]);
    Route::post('review', [
        'as' => 'review.store',
        'uses' => 'ReviewController@store',
    ]);

    /*
     * ADMIN AREA
     */
    Route::group([
        'prefix' => 'admin',
        'middleware' => 'auth'
    ], function()
    {
        Route::get('/', [
            'as' => 'admin.dashboard',
            'uses' => 'AdminController@getIndex'
        ]);

        Route::get('settings', [
            'as' => 'admin.settings',
            'uses' => 'AdminController@getSettings'
        ]);
        Route::post('settings', [
            'as' => 'admin.settings.store',
            'uses' => 'AdminController@storeSettings'
        ]);

        Route::get('homeblocks/{id}', [
            'as' => 'admin.homeblocks.edit',
            'uses' => 'AdminController@getHomeblock'
        ]);
        Route::post('homeblocks/{id}', [
            'as' => 'admin.homeblocks.update',
            'uses' => 'AdminController@updateHomeblock'
        ]);

        Route::get('contacts', [
            'as' => 'admin.contacts',
            'uses' => 'AdminController@getContact'
        ]);
        Route::post('contacts', [
            'as' => 'admin.contacts.update',
            'uses' => 'AdminController@updateContact'
        ]);

        Route::get('events', [
            'as' => 'admin.events',
            'uses' => 'AdminController@getEvents'
        ]);
        Route::get('events/create', [
            'as' => 'admin.events.create',
            'uses' => 'AdminController@getCreateEvents'
        ]);
        Route::get('events/{id}/edit', [
            'as' => 'admin.events.edit',
            'uses' => 'AdminController@getEventsEdit'
        ]);
        Route::post('events/{id}/update', [
            'as' => 'admin.events.update',
            'uses' => 'AdminController@updateEvent'
        ]);
        Route::post('events', [
            'as' => 'admin.events.store',
            'uses' => 'AdminController@storeEvent'
        ]);
        Route::get('events/{id}/delete', [
            'as' => 'admin.events.delete',
            'uses' => 'AdminController@deleteEvent'
        ]);

        // apartments
        Route::get('apartments/{id}', [
            'as' => 'admin.apartments.edit',
            'uses' => 'AdminController@getApartment'
        ]);
        Route::post('apartments/{id}', [
            'as' => 'admin.apartments.update',
            'uses' => 'AdminController@updateApartment'
        ]);

        // Room reservation
        Route::get('reservations/room', [
            'as' => 'admin.reservations.room.index',
            'uses' => 'AdminController@getRoomReservationIndex'
        ]);
        Route::get('reservations/room/create/{id?}', [
            'as' => 'admin.reservations.room.create',
            'uses' => 'AdminController@getRoomReservationCreate'
        ]);
        Route::post('reservations/room/store', [
            'as' => 'admin.reservations.room.store',
            'uses' => 'AdminController@storeRoomReservation'
        ]);
        Route::get('reservations/room/{id}/edit', [
            'as' => 'admin.reservations.room.edit',
            'uses' => 'AdminController@getRoomReservationEdit'
        ]);
        Route::post('reservations/room/{id}/update', [
            'as' => 'admin.reservations.room.update',
            'uses' => 'AdminController@updateRoomReservation'
        ]);
        Route::get('reservations/room/{id}/delete', [
            'as' => 'admin.reservations.room.delete',
            'uses' => 'AdminController@deleteRoomReservation'
        ]);
        // table reservation
        Route::get('reservations/table', [
            'as' => 'admin.reservations.table.index',
            'uses' => 'AdminController@getTableReservationIndex'
        ]);
        Route::post('reservations/table/ajax-get', [
            'as' => 'admin.reservations.table.ajax-get',
            'uses' => 'AdminController@getAjaxTableReservations'
        ]);
        Route::get('reservations/table/settings', [
            'as' => 'admin.reservations.table.settings',
            'uses' => 'AdminController@getTableReservationSettings'
        ]);
        Route::post('reservations/table/ajax-set-unavailableday', [
            'as' => 'admin.reservations.table.ajax-set-unavailableday',
            'uses' => 'AdminController@postAjaxSetUnavailableday'
        ]);
        Route::get('reservations/table/ajax-get-forcedday', [
            'as' => 'admin.reservations.table.ajax-get-forcedday',
            'uses' => 'AdminController@getAjaxGetForcedday'
        ]);
        Route::post('reservations/table/ajax-set-forcedday', [
            'as' => 'admin.reservations.table.ajax-set-forcedday',
            'uses' => 'AdminController@postAjaxSetForcedday'
        ]);
        Route::post('reservations/table/ajax-remove-forcedday', [
            'as' => 'admin.reservations.table.ajax-remove-forcedday',
            'uses' => 'AdminController@postAjaxRemoveForcedday'
        ]);
        Route::get('reservations/table/create', [
            'as' => 'admin.reservations.table.create',
            'uses' => 'AdminController@getTableReservationCreate'
        ]);
        Route::post('reservations/table/store', [
            'as' => 'admin.reservations.table.store',
            'uses' => 'AdminController@storeTableReservation'
        ]);
        Route::get('reservations/table/{id}/edit', [
            'as' => 'admin.reservations.table.edit',
            'uses' => 'AdminController@getTableReservationEdit'
        ]);
        Route::post('reservations/table/{id}/update', [
            'as' => 'admin.reservations.table.update',
            'uses' => 'AdminController@updateTableReservation'
        ]);
        Route::get('reservations/table/{id}/delete', [
            'as' => 'admin.reservations.table.delete',
            'uses' => 'AdminController@deleteTableReservation'
        ]);

        // restaurant
        Route::get('pages/restaurant', [
            'as' => 'admin.restaurant.edit',
            'uses' => 'AdminController@getRestaurant'
        ]);
        Route::post('pages/restaurant', [
            'as' => 'admin.restaurant.update',
            'uses' => 'AdminController@updateRestaurant'
        ]);

        // story
        Route::get('pages/story', [
            'as' => 'admin.story.edit',
            'uses' => 'AdminController@getStory'
        ]);
        Route::post('pages/story', [
            'as' => 'admin.story.update',
            'uses' => 'AdminController@updateStory'
        ]);

        Route::get('pages/services', [
            'as' => 'admin.services.edit',
            'uses' => 'AdminController@getServices'
        ]);
        Route::post('pages/services', [
            'as' => 'admin.services.update',
            'uses' => 'AdminController@updateServices'
        ]);

        Route::get('pages/wellness', [
            'as' => 'admin.wellness.edit',
            'uses' => 'AdminController@getWellness'
        ]);
        Route::post('pages/wellness', [
            'as' => 'admin.wellness.update',
            'uses' => 'AdminController@updateWellness'
        ]);

        Route::get('pages/programroom', [
            'as' => 'admin.programroom.edit',
            'uses' => 'AdminController@getProgramroom'
        ]);
        Route::post('pages/programroom', [
            'as' => 'admin.programroom.update',
            'uses' => 'AdminController@updateProgramroom'
        ]);

        Route::get('pages/watermill', [
            'as' => 'admin.watermill.edit',
            'uses' => 'AdminController@getWatermill'
        ]);
        Route::post('pages/watermill', [
            'as' => 'admin.watermill.update',
            'uses' => 'AdminController@updateWatermill'
        ]);

        // gallery
        Route::get('galleries', [
            'as' => 'admin.galleries',
            'uses' => 'AdminController@getGalleries'
        ]);
        Route::get('galleries/create', [
            'as' => 'admin.galleries.create',
            'uses' => 'AdminController@getGalleriesCreate'
        ]);
        Route::get('galleries/{id}', [
            'as' => 'admin.galleries.show',
            'uses' => 'AdminController@getGallery'
        ]);
        Route::get('galleries/{id}/edit', [
            'as' => 'admin.galleries.edit',
            'uses' => 'AdminController@getGalleriesEdit'
        ]);
        Route::post('galleries/{id}/update', [
            'as' => 'admin.galleries.update',
            'uses' => 'AdminController@updateGallery'
        ]);
        Route::get('galleries/{id}/delete', [
            'as' => 'admin.galleries.delete',
            'uses' => 'AdminController@deleteGallery'
        ]);
        Route::post('galleries/store', [
            'as' => 'admin.galleries.store',
            'uses' => 'AdminController@storeGallery'
        ]);
        Route::post('images/store', [
            'as' => 'admin.image.store',
            'uses' => 'AdminController@storeImage'
        ]);
        Route::post('galleries/{id}/albumimage', [
            'as' => 'admin.galleries.albumimage',
            'uses' => 'AdminController@setAlbumImage'
        ]);
        Route::delete('galleries/{id}/destroyImage', [
            'as' => 'admin.image.destroy',
            'uses' => 'AdminController@destroyImage'
        ]);


        // food
        Route::get('food', [
            'as' => 'admin.food',
            'uses' => 'AdminController@getFoods'
        ]);
        Route::get('food/create', [
            'as' => 'admin.food.cccreate',
            'uses' => 'AdminController@getFoodsCreate'
        ]);
        Route::get('food/{id}/edit', [
            'as' => 'admin.food.edit',
            'uses' => 'AdminController@getFood'
        ]);
        Route::get('food/{id}/delete', [
            'as' => 'admin.food.delete',
            'uses' => 'AdminController@deleteFood'
        ]);
        Route::post('food/{id}/update', [
            'as' => 'admin.food.update',
            'uses' => 'AdminController@updateFood'
        ]);
        Route::post('food/{id}/store', [
            'as' => 'admin.food.store',
            'uses' => 'AdminController@storeFood'
        ]);
        Route::get('food/category-images', [
            'as' => 'admin.food.categoryimages',
            'uses' => 'AdminController@getCategoryImages'
        ]);
        Route::post('food/category-images/{id}', [
            'as' => 'admin.food.categoryimages.update',
            'uses' => 'AdminController@setCategoryImages'
        ]);
        Route::get('banner', [
            'as' => 'admin.banner',
            'uses' => 'AdminController@getBanner'
        ]);
        Route::post('banner', [
            'as' => 'admin.banner.save',
            'uses' => 'AdminController@setBanner'
        ]);
        Route::get('videos', [
            'as' => 'admin.videos',
            'uses' => 'AdminController@getVideos'
        ]);
        Route::post('videos', [
            'as' => 'admin.videos.store',
            'uses' => 'AdminController@storeVideos'
        ]);
        Route::get('videos/{id}/delete', [
            'as' => 'admin.videos.delete',
            'uses' => 'AdminController@deleteVideos'
        ]);
    });
});
