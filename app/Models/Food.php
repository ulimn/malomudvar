<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{

    protected $table = 'foods';

    protected $fillable = ['foodcategory_id', 'price', 'no_egg', 'no_sugar', 'no_milk', 'no_wheat'];

    /**
     * Get the gallery that owns the image.
     */
    public function foodcategory()
    {
        return $this->belongsTo('App\Models\Foodcategory', 'foodcategory_id', 'id');
    }


    /**
     * Get the translations for the gallery.
     *
     * @param string
     */
    public function translation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\FoodTranslation', 'food_id')->where('language', '=', $language);
    }

    /**
     * Get the translations for the Model.
     *
     * @param string
     */
    public function getTranslation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\FoodTranslation', 'food_id')->where('language', '=', $language)->first();
    }
}
