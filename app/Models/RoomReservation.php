<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RoomReservation extends Model
{

    protected $table = "roomreservations";

    protected $fillable = ['apartment_id', 'name', 'address', 'phone',
        'email', 'time_from', 'time_to', 'coupon', 'adult_count',
        'child_count', 'comment'];


    public function setFromTime($datetime)
    {
        $this->attributes['time_from'] = Carbon::createFromFormat('Y-m-d H:i', $datetime);
    }

    public function getTimeFrom()
    {
        $dt = Carbon::parse($this->time_from);

        return $dt->toDateString();
    }

    public function getTimeTo()
    {
        $dt = Carbon::parse($this->time_to);

        return $dt->toDateString();
    }
}
