<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * Get the images for the gallery.
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image', 'gallery_id');
    }

    /**
     * Get the translations for the gallery.
     *
     * @param string
     */
    public function translation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\GalleryTranslation', 'gallery_id')->where('language', '=', $language);
    }

    /**
     * Get the translations for the Model.
     *
     * @param string
     */
    public function getTranslation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\GalleryTranslation', 'gallery_id')->where('language', '=', $language)->first();
    }

    /**
     * Rerutns the count of images related to this gallery.
     *
     * @return mixed
     */
    public function imageCount()
    {
        return $this->images()->get()->count();
    }

    public function getAlbumImageUrl($thumb = false)
    {
        $thumbnail = '';
        if ($thumb) {
            $thumbnail = 'thumbnails/';
        }

        if ($this->featured_image_id != 0) {
            return '/uploads/images/' . $thumbnail . $this->images()->find($this->featured_image_id)->filename;
        } else {
            return asset('assets/img/noimage.png');
        }
    }
}
