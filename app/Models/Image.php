<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    protected $table = 'images';

    protected $fillable = ['gallery_id', 'filename'];

    /**
     * Get the gallery that owns the image.
     */
    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery', 'gallery_id', 'id');
    }

    public function getUrl($thumb = false)
    {
        $thumbnail = '';
        if ($thumb) {
            $thumbnail = 'thumbnails/';
        }

        return '/uploads/images/' . $thumbnail . $this->filename;
    }
}
