<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryTranslation extends Model
{

    protected $table = 'gallery_translations';

    protected $fillable = ['language', 'name', 'gallery_id'];

    /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function homeBlock()
   {
       return $this->belongsTo('App\Models\Gallery', 'gallery_id', 'id');
   }

}
