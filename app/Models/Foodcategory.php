<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foodcategory extends Model
{
    /**
     * Get the images for the gallery.
     */
    public function foods()
    {
        return $this->hasMany('App\Models\Food', 'foodcategory_id');
    }

    /**
     * Get the translations for the gallery.
     *
     * @param string
     */
    public function translation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\FoodcategoryTranslation', 'foodcategory_id')->where('language', '=', $language);
    }

    /**
     * Get the translations for the Model.
     *
     * @param string
     */
    public function getTranslation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\FoodcategoryTranslation', 'foodcategory_id')->where('language', '=', $language)->first();
    }
}
