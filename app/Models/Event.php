<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = ['days', 'datetime'];

    /**
     * Get the translations for the Model.
     *
     * @param string
     */
    public function translation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\EventTranslation', 'event_id')->where('language', '=', $language);
    }

    /**
     * Get the translations for the Model.
     *
     * @param string
     */
    public function getTranslation($language = null)
    {
        if ($language == null) {
            $language = \LaravelLocalization::getCurrentLocale();
        }
        return $this->hasMany('App\Models\EventTranslation', 'event_id')->where('language', '=', $language)->first();
    }

}
