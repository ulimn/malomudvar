<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeBlockTranslation extends Model
{

    protected $table = 'homeblock_translations';

    protected $fillable = ['language', 'name', 'content'];

    /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function homeBlock()
   {
       return $this->belongsTo('App\Models\HomeBlock', 'homeblock_id', 'id');
   }

}
