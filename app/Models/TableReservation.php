<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TableReservation extends Model
{

    protected $table = "tablereservations";

    protected $fillable = ['apartment_id', 'name', 'address', 'phone', 'email', 'time', 'guest_count', 'comment'];

}
