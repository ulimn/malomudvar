<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodTranslation extends Model
{

    protected $table = 'food_translations';

    protected $fillable = ['language', 'name'];

    /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function food()
   {
       return $this->belongsTo('App\Models\Food', 'food_id', 'id');
   }

}
