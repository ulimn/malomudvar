<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartmentTranslation extends Model
{

    protected $table = 'apartment_translations';

    protected $fillable = ['name', 'rating', 'place', 'review', 'language'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function apartment()
    {
        return $this->belongsTo('App\Models\Apartment', 'apartment_id', 'id');
    }

}
