<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodcategoryTranslation extends Model
{

    protected $table = 'foodcategory_translations';

    protected $fillable = ['language', 'name'];

    /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function homeBlock()
   {
       return $this->belongsTo('App\Models\Gallery', 'gallery_id', 'id');
   }


}
