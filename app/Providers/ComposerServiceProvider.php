<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        view()->composer(
            'admin.layouts.main', 'App\Http\ViewComposers\AdminComposer'
        );
        view()->composer(
            'layouts.onepage', 'App\Http\ViewComposers\OnepageComposer'
        );
        view()->composer(
            'layouts.normal', 'App\Http\ViewComposers\OnepageComposer'
        );
        view()->composer(
            'reservation', 'App\Http\ViewComposers\ReservationComposer'
        );
        view()->composer(
            '*', 'App\Http\ViewComposers\GlobalComposer'
        );

        // Using Closure based composers...
        /*view()->composer('dashboard', function ($view) {

        });*/
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
