#!/usr/bin/env bash

RED='\033[0;31m'
CYAN='\033[0;36m'
GREEN='\033[1;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No color

create_env() {
	printf "\n${GREEN}Checking .env file and key...${NC}\n"
	if [ ! -f .env ]; then
	    printf "\n.env file not found, copying example...\n"
	    cp .env.example .env
	    printf "${ORANGE}Edit your environment's settings in the .env file and re-run the script!${NC}\n"
	    exit 0
    else
    	printf "\n${GREEN}.env exists, continuing...${NC}\n"
    fi
}

deploy() {
	printf "\n${GREEN}Running Composer...${NC}\n"
	composer install
	composer update
	composer dump-autoload

	printf "\n${GREEN}Setting file permissions...${NC}\n"
	chmod -R 664 storage/
	chmod -R +X storage/
	chmod -R 664 bootstrap/cache/
	chmod -R +X bootstrap/cache/

	printf "\n${GREEN}Clearing Laravel cache etc...${NC}\n"
	php artisan cache:clear
	php artisan clear-compiled

    printf "\n${GREEN}Clearing config cache and generating new key...${NC}\n"
    php artisan config:clear
    php artisan key:generate

	printf "\n${GREEN}Running migrations and seeds...${NC}\n"
	php artisan migrate:refresh --seed

	printf "\n${GREEN}Running bower...${NC}\n"
	bower install

	printf "\n${GREEN}Deploy Script End.${NC}\n"
}

printf "\n${CYAN}Laravel Deploy Script for lazy people :D${NC}\n"

create_env

printf "\n${ORANGE}To set everything correctly, you must be the owner of the files in this folder!${NC}\n"

read -r -p "${1:-Continue? [y/N]} " response
case $response in
    [yY][eE][sS]|[yY])
        deploy
        ;;
    *)
        printf "\n\n${RED}Stopping script.${NC}\n"
        exit 0
        ;;
esac
